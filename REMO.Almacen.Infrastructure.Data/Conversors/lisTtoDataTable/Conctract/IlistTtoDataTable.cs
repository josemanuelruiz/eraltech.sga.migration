﻿using System.Collections.Generic;
using System.Data;

namespace REMO.Almacen.Infrastructure.Data.Conversors.lisTtoDataTable.Conctract
{
    public interface IlistTtoDataTable<T> where T : class
    {
        string ExportToExcel(string filepath, List<T> objectlist);
        DataTable ToDataTable(List<T> objectlist);    

    }
}
