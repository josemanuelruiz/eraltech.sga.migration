﻿using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Infrastructure.Data.Repository;
using REMO.Almacen.Infrastructure.Data.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;


namespace REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract
{
    public interface IUnitOfWork : IDisposable
    {
       IGenericRepository<M_Almacenes> M_AlmacenesRepository { get; }
       IGenericRepository<M_Equipos> M_EquiposRepository { get; }
       IGenericRepository<M_Especialidades> M_EspecialidadesRepository { get; }
       IGenericRepository<M_Familias> M_FamiliasRepository { get; }
       IGenericRepository<M_Fases> M_FasesRepository { get; }
       IGenericRepository<M_Layout> M_LayoutRepository { get; }
       IGenericRepository<M_Lineas> M_LineasRepository { get; }
       IGenericRepository<M_Marcas> M_MarcasRepository { get; }
       IGenericRepository<M_Operaciones> M_OperacionesRepository { get; }
       IGenericRepository<M_Planta> M_PlantaRepository { get; }
       IGenericRepository<M_TablaConfiguracion> M_TablaConfiguracionRepository { get; }
       IGenericRepository<T_ErroresalguardarAutocorrecci> T_ErroresalguardarAutocorrecciRepository { get; }
       IGenericRepository<T_Erroresdepegado> T_ErroresdepegadoRepository { get; }
       IGenericRepository<T_Movimientos> T_MovimientosRepository { get; }
       IGenericRepository<T_RecambiosAlmacen> T_RecambiosAlmacenRepository { get; }
       IGenericRepository<T_RelacionEquipo_Recambios> T_RelacionEquipo_RecambiosRepository { get; }
       IGenericRepository<T_ReferenciasAsociadas> T_ReferenciasAsociadasRepository { get; }
       IGenericRepository<T_ZZZMovimientos> T_ZZZMovimientosRepository { get; }

        //tablas con FK y borrado lógico
        IGenericRepository<zSYS_M_Almacenes> zSYS_M_AlmacenesRepository  { get; }
        IGenericRepository<zSYS_M_Equipos> zSYS_M_EquiposRepository  { get; }
        IGenericRepository<zSYS_M_Especialidades> zSYS_M_EspecialidadesRepository  { get; }
        IGenericRepository<zSYS_M_Familias> zSYS_M_FamiliasRepository  { get; }
        IGenericRepository<zSYS_M_Fases> zSYS_M_FasesRepository  { get; }
        IGenericRepository<zSYS_M_Layout> zSYS_M_LayoutRepository  { get; }
        IGenericRepository<zSYS_M_Lineas> zSYS_M_LineasRepository  { get; }
        IGenericRepository<zSYS_M_Marcas> zSYS_M_MarcasRepository  { get; }
        IGenericRepository<zSYS_M_MovementTypes> zSYS_M_MovementTypesRepository  { get; }
        IGenericRepository<zSYS_M_Operaciones> zSYS_M_OperacionesRepository  { get; }
        IGenericRepository<zSYS_M_Planta> zSYS_M_PlantaRepository  { get; }
        IGenericRepository<zSYS_M_TablaConfiguracion> zSYS_M_TablaConfiguracionRepository  { get; }
        IGenericRepository<zSYS_M_Ubicaciones> zSYS_M_UbicacionesRepository  { get; }
        IGenericRepository<zSYS_T_Movimientos> zSYS_T_MovimientosRepository  { get; }
        IGenericRepository<zSYS_T_RecambiosAlmacen> zSYS_T_RecambiosAlmacenRepository  { get; }
        IGenericRepository<zSYS_T_ReferenciasAsociadas> zSYS_T_ReferenciasAsociadasRepository  { get; }
        IGenericRepository<zSYS_T_RelacionEquipo_Recambios> zSYS_T_RelacionEquipo_RecambiosRepository  { get; }
        IGenericRepository<zSYS_T_ZZZMovimientos> zSYS_T_ZZZMovimientosRepository  { get; }


        IEnumerable<T> ExecuteWithStoreProcedureNoTransaction<T>(string query, params object[] parameters);
        IEnumerable<T> ExecuteWithStoreProcedure<T>(string query, params object[] parameters);
        void ExecuteWithStoreProcedureNoTransaction(string query, params object[] parameters);
        void ExecuteWithStoreProcedure(string query, params object[] parameters);
        int ExecWithStoreProcedure(string query, params object[] parameters);
        void Save();
        Task SaveAsync();
    }
}

