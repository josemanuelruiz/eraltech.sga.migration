﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Infrastructure.Data.Repository;
using REMO.Almacen.Infrastructure.Data.Repository.Contract;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;

namespace REMO.Almacenes.Infrastructure.Data.UnitOfWork.Implementation
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly REMODataContext _context;

        private IGenericRepository<M_Almacenes> _m_AlmacenesRepository;
        private IGenericRepository<M_Equipos> _m_EquiposRepository;
        private IGenericRepository<M_Especialidades> _m_EspecialidadesRepository;
        private IGenericRepository<M_Familias> _m_FamiliasRepository;
        private IGenericRepository<M_Fases> _m_FasesRepository;
        private IGenericRepository<M_Layout> _m_LayoutRepository;
        private IGenericRepository<M_Lineas> _m_LineasRepository;
        private IGenericRepository<M_Marcas> _m_MarcasRepository;
        private IGenericRepository<M_Operaciones> _m_OperacionesRepository;
        private IGenericRepository<M_Planta> _m_PlantaRepository;
        private IGenericRepository<M_TablaConfiguracion> _m_TablaConfiguracionRepository;
        private IGenericRepository<T_ErroresalguardarAutocorrecci> _t_ErroresalguardarAutocorrecciRepository;
        private IGenericRepository<T_Erroresdepegado> _t_ErroresdepegadoRepository;
        private IGenericRepository<T_Movimientos> _t_MovimientosRepository;
        private IGenericRepository<T_ReferenciasAsociadas> _t_ReferenciasAsociadasRepository;
        private IGenericRepository<T_RecambiosAlmacen> _t_RecambiosAlmacenRepository;
        private IGenericRepository<T_RelacionEquipo_Recambios> _t_RelacionEquipo_RecambiosRepository;
        private IGenericRepository<T_ZZZMovimientos> _t_ZZZMovimientosRepository;

        //tablas con FK y borrado lógico
        private IGenericRepository<zSYS_M_Almacenes> _zSYS_m_AlmacenesRepository;
        private IGenericRepository<zSYS_M_Equipos> _zSYS_m_EquiposRepository;
        private IGenericRepository<zSYS_M_Especialidades> _zSYS_m_EspecialidadesRepository;
        private IGenericRepository<zSYS_M_Familias> _zSYS_m_FamiliasRepository;
        private IGenericRepository<zSYS_M_Fases> _zSYS_m_FasesRepository;
        private IGenericRepository<zSYS_M_Layout> _zSYS_m_LayoutRepository;
        private IGenericRepository<zSYS_M_Lineas> _zSYS_m_LineasRepository;
        private IGenericRepository<zSYS_M_Marcas> _zSYS_m_MarcasRepository;
        private IGenericRepository<zSYS_M_MovementTypes> _zSYS_m_MovementTypesRepository;
        private IGenericRepository<zSYS_M_Operaciones> _zSYS_m_OperacionesRepository;
        private IGenericRepository<zSYS_M_Planta> _zSYS_m_PlantaRepository;
        private IGenericRepository<zSYS_M_TablaConfiguracion> _zSYS_m_TablaConfiguracionRepository;
        private IGenericRepository<zSYS_M_Ubicaciones> _zSYS_m_UbicacionesRepository;
        private IGenericRepository<zSYS_T_Movimientos> _zSYS_t_MovimientosRepository;
        private IGenericRepository<zSYS_T_RecambiosAlmacen> _zSYS_t_RecambiosAlmacenRepository;
        private IGenericRepository<zSYS_T_ReferenciasAsociadas> _zSYS_t_ReferenciasAsociadasRepository;
        private IGenericRepository<zSYS_T_RelacionEquipo_Recambios> _zSYS_t_RelacionEquipo_RecambiosRepository;
        private IGenericRepository<zSYS_T_ZZZMovimientos> _zSYS_t_ZZZMovimientosRepository;


        private bool _disposed;

        public UnitOfWork()
        {
            _context = new REMODataContext();
        }

        public UnitOfWork(string connectionString)
        {
            _context = new REMODataContext(connectionString);
        }

        //Repositories
        public IGenericRepository<M_Almacenes> M_AlmacenesRepository => _m_AlmacenesRepository ??
          (_m_AlmacenesRepository = new GenericRepository<M_Almacenes>(_context));
        public IGenericRepository<M_Equipos> M_EquiposRepository => _m_EquiposRepository ??
          (_m_EquiposRepository = new GenericRepository<M_Equipos>(_context));
        public IGenericRepository<M_Especialidades> M_EspecialidadesRepository => _m_EspecialidadesRepository ??
          (_m_EspecialidadesRepository = new GenericRepository<M_Especialidades>(_context));
        public IGenericRepository<M_Familias> M_FamiliasRepository => _m_FamiliasRepository ??
          (_m_FamiliasRepository = new GenericRepository<M_Familias>(_context));
        public IGenericRepository<M_Fases> M_FasesRepository => _m_FasesRepository ??
          (_m_FasesRepository = new GenericRepository<M_Fases>(_context));
        public IGenericRepository<M_Layout> M_LayoutRepository => _m_LayoutRepository ??
          (_m_LayoutRepository = new GenericRepository<M_Layout>(_context));
        public IGenericRepository<M_Lineas> M_LineasRepository => _m_LineasRepository ??
          (_m_LineasRepository = new GenericRepository<M_Lineas>(_context));
        public IGenericRepository<M_Marcas> M_MarcasRepository => _m_MarcasRepository ??
          (_m_MarcasRepository = new GenericRepository<M_Marcas>(_context));
        public IGenericRepository<M_Operaciones> M_OperacionesRepository => _m_OperacionesRepository ??
          (_m_OperacionesRepository = new GenericRepository<M_Operaciones>(_context));
        public IGenericRepository<M_Planta> M_PlantaRepository => _m_PlantaRepository ??
          (_m_PlantaRepository = new GenericRepository<M_Planta>(_context));
        public IGenericRepository<M_TablaConfiguracion> M_TablaConfiguracionRepository => _m_TablaConfiguracionRepository ??
          (_m_TablaConfiguracionRepository = new GenericRepository<M_TablaConfiguracion>(_context));

        public IGenericRepository<T_ErroresalguardarAutocorrecci> T_ErroresalguardarAutocorrecciRepository => _t_ErroresalguardarAutocorrecciRepository ??
          (_t_ErroresalguardarAutocorrecciRepository = new GenericRepository<T_ErroresalguardarAutocorrecci>(_context));
        public IGenericRepository<T_Erroresdepegado> T_ErroresdepegadoRepository => _t_ErroresdepegadoRepository ??
          (_t_ErroresdepegadoRepository = new GenericRepository<T_Erroresdepegado>(_context));
        public IGenericRepository<T_Movimientos> T_MovimientosRepository => _t_MovimientosRepository ??
          (_t_MovimientosRepository = new GenericRepository<T_Movimientos>(_context));
        public IGenericRepository<T_ReferenciasAsociadas> T_ReferenciasAsociadasRepository => _t_ReferenciasAsociadasRepository ??
          (_t_ReferenciasAsociadasRepository = new GenericRepository<T_ReferenciasAsociadas>(_context));
        public IGenericRepository<T_RecambiosAlmacen> T_RecambiosAlmacenRepository => _t_RecambiosAlmacenRepository ??
          (_t_RecambiosAlmacenRepository = new GenericRepository<T_RecambiosAlmacen>(_context));
        public IGenericRepository<T_RelacionEquipo_Recambios> T_RelacionEquipo_RecambiosRepository => _t_RelacionEquipo_RecambiosRepository ??
         (_t_RelacionEquipo_RecambiosRepository = new GenericRepository<T_RelacionEquipo_Recambios>(_context));
        public IGenericRepository<T_ZZZMovimientos> T_ZZZMovimientosRepository => _t_ZZZMovimientosRepository ??
          (_t_ZZZMovimientosRepository = new GenericRepository<T_ZZZMovimientos>(_context));

        //Repositories
        public IGenericRepository<zSYS_M_Almacenes> zSYS_M_AlmacenesRepository => _zSYS_m_AlmacenesRepository ??
          (_zSYS_m_AlmacenesRepository = new GenericRepository<zSYS_M_Almacenes>(_context));
        public IGenericRepository<zSYS_M_Equipos> zSYS_M_EquiposRepository => _zSYS_m_EquiposRepository ??
          (_zSYS_m_EquiposRepository = new GenericRepository<zSYS_M_Equipos>(_context));
        public IGenericRepository<zSYS_M_Especialidades> zSYS_M_EspecialidadesRepository => _zSYS_m_EspecialidadesRepository ??
          (_zSYS_m_EspecialidadesRepository = new GenericRepository<zSYS_M_Especialidades>(_context));
        public IGenericRepository<zSYS_M_Familias> zSYS_M_FamiliasRepository => _zSYS_m_FamiliasRepository ??
          (_zSYS_m_FamiliasRepository = new GenericRepository<zSYS_M_Familias>(_context));
        public IGenericRepository<zSYS_M_Fases> zSYS_M_FasesRepository => _zSYS_m_FasesRepository ??
          (_zSYS_m_FasesRepository = new GenericRepository<zSYS_M_Fases>(_context));
        public IGenericRepository<zSYS_M_Layout> zSYS_M_LayoutRepository => _zSYS_m_LayoutRepository ??
          (_zSYS_m_LayoutRepository = new GenericRepository<zSYS_M_Layout>(_context));
        public IGenericRepository<zSYS_M_Lineas> zSYS_M_LineasRepository => _zSYS_m_LineasRepository ??
          (_zSYS_m_LineasRepository = new GenericRepository<zSYS_M_Lineas>(_context));
        public IGenericRepository<zSYS_M_Marcas> zSYS_M_MarcasRepository => _zSYS_m_MarcasRepository ??
          (_zSYS_m_MarcasRepository = new GenericRepository<zSYS_M_Marcas>(_context));
        public IGenericRepository<zSYS_M_MovementTypes> zSYS_M_MovementTypesRepository => _zSYS_m_MovementTypesRepository ??
       (_zSYS_m_MovementTypesRepository = new GenericRepository<zSYS_M_MovementTypes>(_context));
        public IGenericRepository<zSYS_M_Ubicaciones> zSYS_M_UbicacionesRepository => _zSYS_m_UbicacionesRepository ??
       (_zSYS_m_UbicacionesRepository = new GenericRepository<zSYS_M_Ubicaciones>(_context));
        public IGenericRepository<zSYS_M_Operaciones> zSYS_M_OperacionesRepository => _zSYS_m_OperacionesRepository ??
          (_zSYS_m_OperacionesRepository = new GenericRepository<zSYS_M_Operaciones>(_context));
        public IGenericRepository<zSYS_M_Planta> zSYS_M_PlantaRepository => _zSYS_m_PlantaRepository ??
          (_zSYS_m_PlantaRepository = new GenericRepository<zSYS_M_Planta>(_context));
        public IGenericRepository<zSYS_M_TablaConfiguracion> zSYS_M_TablaConfiguracionRepository => _zSYS_m_TablaConfiguracionRepository ??
          (_zSYS_m_TablaConfiguracionRepository = new GenericRepository<zSYS_M_TablaConfiguracion>(_context));

        //public IGenericRepository<zSYS_T_ErroresalguardarAutocorrecci> zSYS_T_ErroresalguardarAutocorrecciRepository => _zSYS_t_ErroresalguardarAutocorrecciRepository ??
        //  (_zSYS_t_ErroresalguardarAutocorrecciRepository = new GenericRepository<zSYS_T_ErroresalguardarAutocorrecci>(_context));
        //public IGenericRepository<zSYS_T_Erroresdepegado> zSYS_T_ErroresdepegadoRepository => _zSYS_t_ErroresdepegadoRepository ??
        //  (_zSYS_t_ErroresdepegadoRepository = new GenericRepository<zSYS_T_Erroresdepegado>(_context));
        public IGenericRepository<zSYS_T_Movimientos> zSYS_T_MovimientosRepository => _zSYS_t_MovimientosRepository ??
          (_zSYS_t_MovimientosRepository = new GenericRepository<zSYS_T_Movimientos>(_context));
        public IGenericRepository<zSYS_T_ReferenciasAsociadas> zSYS_T_ReferenciasAsociadasRepository => _zSYS_t_ReferenciasAsociadasRepository ??
          (_zSYS_t_ReferenciasAsociadasRepository = new GenericRepository<zSYS_T_ReferenciasAsociadas>(_context));
        public IGenericRepository<zSYS_T_RecambiosAlmacen> zSYS_T_RecambiosAlmacenRepository => _zSYS_t_RecambiosAlmacenRepository ??
          (_zSYS_t_RecambiosAlmacenRepository = new GenericRepository<zSYS_T_RecambiosAlmacen>(_context));
        public IGenericRepository<zSYS_T_RelacionEquipo_Recambios> zSYS_T_RelacionEquipo_RecambiosRepository => _zSYS_t_RelacionEquipo_RecambiosRepository ??
         (_zSYS_t_RelacionEquipo_RecambiosRepository = new GenericRepository<zSYS_T_RelacionEquipo_Recambios>(_context));
        public IGenericRepository<zSYS_T_ZZZMovimientos> zSYS_T_ZZZMovimientosRepository => _zSYS_t_ZZZMovimientosRepository ??
          (_zSYS_t_ZZZMovimientosRepository = new GenericRepository<zSYS_T_ZZZMovimientos>(_context));


        #region ExecuteStore
        public IEnumerable<T> ExecuteWithStoreProcedureNoTransaction<T>(string query, params object[] parameters)
        {
            return _context.Database.SqlQuery<T>(query, parameters);
        }
        public int ExecWithStoreProcedure(string query, params object[] parameters)
        {
            return _context.Database.ExecuteSqlCommand("EXEC " + query, parameters);
        }
        public IEnumerable<T> ExecuteWithStoreProcedure<T>(string query, params object[] parameters)
        {
            using (var trans = _context.Database.BeginTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {
                    var result = _context.Database.SqlQuery<T>(query, parameters);
                    trans.Commit();
                    return result;
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
                finally
                {
                    trans?.Dispose();
                }
            }

        }
        public void ExecuteWithStoreProcedureNoTransaction(string query, params object[] parameters)
        {
            _context.Database.ExecuteSqlCommand(query, parameters);
        }
        public void ExecuteWithStoreProcedure(string query, params object[] parameters)
        {

            using (var trans = _context.Database.BeginTransaction(IsolationLevel.RepeatableRead))
            {
                try
                {
                    _context.Database.ExecuteSqlCommand(query, parameters);
                    trans.Commit();
                }
                catch (Exception)
                {
                    trans.Rollback();
                    throw;
                }
                finally
                {
                    trans?.Dispose();
                }
            }

        }

        #endregion



        public void Save()
        {
            _context.SaveChanges();
        }

        public async Task SaveAsync()
        {
            await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed)
                if (disposing) _context.Dispose();
            _disposed = true;
        }
    }
}
