﻿using Eraltech.SGA.Shared.Models.Dispositivo;
using Eraltech.SGA.Shared.Models.Producto;

namespace Eraltech.SGA.Services.Common
{
    public interface IBarcodeGenerator
    {
        string BarcodeToImage(IProductoModel productoFormModel);

        string BarcodeToImage(DispositivoFormModel dispositivoFormModel);
    }
}
