﻿using Eraltech.SGA.Shared.Models.Dispositivo;
using Eraltech.SGA.Shared.Models.Producto;
using Microsoft.Extensions.Configuration;
using Syncfusion.Pdf.Barcode;
using Syncfusion.Pdf.Graphics;
using System;
using System.Drawing;
using System.IO;
using System.Reflection;

namespace Eraltech.SGA.Services.Common
{
    public class BarcodeGenerator : IBarcodeGenerator
    {
        protected readonly IConfiguration Configuration;
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        public BarcodeGenerator(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public string BarcodeToImage(IProductoModel productoFormModel)
        {
            string barcodeImageFileOutputPath = "";

            try
            {
                //Initialize a new PdfCode39ExtendedBarcode instance
                PdfCode39ExtendedBarcode barcode = new PdfCode39ExtendedBarcode();

                //Set the height and text for barcode
                barcode.BarHeight = 20;
                barcode.TextDisplayLocation = TextLocation.None;
                //barcode.BackColor = new PdfColor(Syncfusion.Drawing.Color.Transparent);

                barcode.Text = productoFormModel.CodigoProducto;

                //layout labels
                var imageFilePathin = $"{Configuration.GetValue<string>("BarcodeTemplatePath")}\\ProductLabelTemplate.png";
                var whiteFileBand = $"{Configuration.GetValue<string>("BarcodeTemplatePath")}\\WhiteBand.PNG";

                var codAgWithWhites = ConvertToAGCode(productoFormModel.CodigoProducto);

                //Convert the barcode to image
                Image barcodeImage = barcode.ToImage(new SizeF(210, 50));
                Bitmap whiteFileImage = (Bitmap)Image.FromFile(whiteFileBand);
                float dpiX;
                float dpiY;
                using (var bitmap = (Bitmap)Image.FromFile(imageFilePathin)) //load the layout label file
                {
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        dpiX = graphics.DpiX;
                        dpiY = graphics.DpiY;

                        TextAlignment.Center(graphics, productoFormModel.FamiliaDescripcion, "times new roman", 15, FontStyle.Bold, Brushes.Black, 0, 4, 260, 40);
                        TextAlignment.Center(graphics, productoFormModel.Nombre, "times new roman", 10, FontStyle.Bold, Brushes.Black, 0, 42, 260, 35);
                        TextAlignment.Center(graphics, productoFormModel.Referencia, "times new roman", 13, FontStyle.Bold, Brushes.Black, 0, 73, 260, 35);
                        TextAlignment.Center(graphics, productoFormModel.MarcaDescripcion, "times new roman", 10, FontStyle.Bold, Brushes.DarkBlue, 0, 106, 135, 25);
                        //TextAlignment.Center(graphics, productoFormModel.Stock.UbicacionDescripcion, "times new roman", 10, FontStyle.Bold, Brushes.DarkBlue, 135, 106, 135, 25);

                        graphics.DrawImage(barcodeImage, new PointF(20f, 130f));
                        graphics.DrawImage(whiteFileImage, new PointF(5f, 163f));
                        graphics.DrawString(codAgWithWhites, new Font("DECTERM", (float)(11.5), FontStyle.Bold), Brushes.Black, new PointF(22f, 163f));
                    }

                    using (MemoryStream stream = new MemoryStream())
                    {
                        //Save image to stream.
                        bitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

                        stream.Position = 0;
                        System.Random random = new System.Random();
                        int i = random.Next();
                        barcodeImageFileOutputPath = $"{Configuration.GetValue<string>("FilesOutputPath")}\\{productoFormModel.CodigoProducto}_{i}.png";

                        File.WriteAllBytes(barcodeImageFileOutputPath, stream.ToArray());
                    }

                    bitmap.Dispose();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("{0}", ex.Message));
            }

            return barcodeImageFileOutputPath;
        }

        public string BarcodeToImage(DispositivoFormModel dispositivoFormModel)    //string value, string ProductName, string DeviceName)
        {
            ////Initialize a new PdfQRBarcode instance 
            //PdfQRBarcode barcode = new PdfQRBarcode();

            ////Set the XDimension and text for barcode 
            //barcode.XDimension = 3;

            string barcodeImageFileOutputPath = "";

            try
            {
                //layout measures
                float labelWidth = 68;
                float labelHeight = 50;

                //Initialize a new PdfCode39ExtendedBarcode instance
                PdfCode39ExtendedBarcode barcode = new PdfCode39ExtendedBarcode();

                //Set the height and text for barcode
                barcode.BarHeight = 45;

                barcode.Text = dispositivoFormModel.SerialNumber;

                //layout labels
                var imageFilePathin = $"{Configuration.GetValue<string>("BarcodeTemplatePath")}\\DeviceLabelTemplate.png";

                //Convert the barcode to image
                Image barcodeImage = barcode.ToImage(new SizeF(400, 140));
                Bitmap newBitmap;
                float dpiX;
                float dpiY;
                using (var bitmap = (Bitmap)Image.FromFile(imageFilePathin)) //load the layout label file
                {
                    using (Graphics graphics = Graphics.FromImage(bitmap))
                    {
                        dpiX = graphics.DpiX;
                        dpiY = graphics.DpiY;

                        TextAlignment.Center(graphics, dispositivoFormModel.NombreProducto, "times new roman", 30, FontStyle.Bold, Brushes.Black, 10, 15, 535, 80);
                        TextAlignment.Center(graphics, dispositivoFormModel.Descripcion, "times new roman", 18, FontStyle.Bold, Brushes.Black, 10, 95, 535, 70);

                        graphics.DrawImage(barcodeImage, new PointF(65f, 180f));
                    }
                    using (MemoryStream stream = new MemoryStream())
                    {
                        newBitmap = new Bitmap(bitmap, mmToPixel(labelWidth, dpiX), mmToPixel(labelHeight, dpiY));

                        //Save image to stream.
                        newBitmap.Save(stream, System.Drawing.Imaging.ImageFormat.Png);

                        stream.Position = 0;
                        System.Random random = new System.Random();
                        // TODO - Quitar el random en PRE y PRO
                        int i = random.Next();
                        barcodeImageFileOutputPath = $"{Configuration.GetValue<string>("FilesOutputPath")}\\{dispositivoFormModel.SerialNumber}_{i}.png";
                        // fin

                        File.WriteAllBytes(barcodeImageFileOutputPath, stream.ToArray());
                    }
                    bitmap.Dispose();
                    newBitmap.Dispose();
                }
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("{0}", ex.Message));
            }

            return barcodeImageFileOutputPath;
        }

        private string ConvertToAGCode(string codeAg)
        {
            try
            {
                char[] c = codeAg.ToCharArray();

                string codAgWithWhites = "*  " + c[0] + "  ";

                for (int i = 1; i < c.Length; i++)
                {
                    codAgWithWhites = codAgWithWhites + c[i] + "  ";
                }
                codAgWithWhites = codAgWithWhites + "*";
                return codAgWithWhites;
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("{0}", ex.Message));
            }
        }

        private int mmToPixel(float mm, float dpi)
        {
            return (int)Math.Round((mm / 25.4) * dpi);
        }
    }
}
