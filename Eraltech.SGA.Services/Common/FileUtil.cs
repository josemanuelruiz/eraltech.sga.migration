﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Eraltech.SGA.Services.Common
{
    public class FileUtil : IFileUtil
    {
        protected readonly IConfiguration Configuration;

        public FileUtil(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void DeleteTemporalFiles()
        {
            string fileOutputPath = $"{Configuration.GetValue<string>("FilesOutputPath")}";
            string[] files = Directory.GetFiles(fileOutputPath);
            DateTime creationDate;         
            foreach(string file in files)
            {
                creationDate = File.GetCreationTime(file).AddDays(1);
                if (creationDate < DateTime.Now)
                    File.Delete(file);
            }
        }
    }
}
