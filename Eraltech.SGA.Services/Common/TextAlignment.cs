﻿using System.Drawing;

namespace Eraltech.SGA.Services.Common
{
    public static class TextAlignment
    {
        public static void Center(Graphics graphics, string text, string fontName, int fontSize, FontStyle fontStyle, Brush fontColor, int x, int y, int width, int height)
        {
            //string text1 = "Use StringFormat and Rectangle objects to"
            //    + " center text in a rectangle.";
            using (Font font = new Font(fontName, fontSize, fontStyle, GraphicsUnit.Point))
            {
                Rectangle rect = new Rectangle(x, y, width, height);

                // Create a StringFormat object with the each line of text, and the block
                // of text centered on the page.
                StringFormat stringFormat = new StringFormat();
                stringFormat.Alignment = StringAlignment.Center;
                stringFormat.LineAlignment = StringAlignment.Center;

                // Draw the text and the surrounding rectangle.
                
                //graphics.DrawString(text, font, Brushes.Blue, new PointF(70f, 160f), stringFormat);
                graphics.DrawString(text, font, fontColor, rect, stringFormat);
                graphics.DrawRectangle(Pens.Transparent, rect);
            }
        }
    }
}
