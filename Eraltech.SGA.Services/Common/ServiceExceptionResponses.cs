﻿using Eraltech.SGA.Shared;
using System;
using Eraltech.SGA.Shared.Models;

namespace Eraltech.SGA.Services.Common
{
    public class ServiceExceptionResponses
    {
        internal static ResponseModel GetResponseFromException(Exception exception)
        {
            ResponseModel response = new ResponseModel();
            response.Ok = false;
            if (exception.InnerException != null)
                response.Message = exception.InnerException.Message;
            else
                response.Message = exception.Message;

            return response;
        }

        public static ResponseDataModel<T> GetResponseDataFromException<T>(T data, Exception exception)
        {
            ResponseDataModel<T> response = new ResponseDataModel<T>();
            response.Ok = false;
            response.Object = data;
            if (exception.InnerException != null)
                response.Message = exception.InnerException.Message;
            else
                response.Message = exception.Message;

            return response;
        }
    }
}
