﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eraltech.Common.Logs;
using Eraltech.Login;
using Eraltech.Login.Models;
using Eraltech.SGA.Core;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Rol;

namespace Eraltech.SGA.Services
{
    public class RolService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserManagement _userManagement;

        public RolService(IUnitOfWork unitOfWork, IUserManagement userManagement)
        {
            this._unitOfWork = unitOfWork;
            this._userManagement = userManagement;
        }

        public IEnumerable<RolGridModel> GetAllRoles()
        {            
            var rolDataCollection = _userManagement.CargarRoles();
            var rolModelCollection = rolDataCollection.Select(u => RolMapper.MapToGridModel(u));
            return rolModelCollection.ToList();
        }

        public RolFormModel GetRollById(int id)
        {
            var rolData = _userManagement.CargarRol(id);
            var rolModel = RolMapper.MapToModel(rolData);
            return rolModel;
        }

        public RolGridModel GetRolGridModelById(int id)
        {
            var rolData = _userManagement.CargarRol(id);
            var rolModel = RolMapper.MapToGridModel(rolData);
            return rolModel;
        }

        public ResponseDataModel<RolGridModel> CreateRol(RolFormModel newRolModel)
        {
            RolGridModel newRolGridModel = null;
            try
            {
                var newRolData = RolMapper.MapToEntity(newRolModel);
                newRolGridModel = RolMapper.MapToGridModel(newRolData);

                _userManagement.GuardarRol(newRolData);

                newRolGridModel = RolMapper.MapToGridModel(newRolData);
                return new ResponseDataModel<RolGridModel>()
                {
                    Ok = true,
                    Object = newRolGridModel
                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<RolGridModel>(newRolGridModel, ex);
            }
        }

        public ResponseModel UpdateRol(RolFormModel rolModel)
        {
            try
            {
                var rolDataToBeUpdated = RolMapper.MapToEntity(rolModel);
                _userManagement.GuardarRol(rolDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteRol(RolGridModel rolModel)
        {
            try
            {
                var rolDataToBeDeleted = RolMapper.MapFromGridModelToEntity(rolModel);
                _userManagement.EliminarRol(rolModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboRol()
        {
            try
            {
                var result = _userManagement.CargarRoles().Select(p => new ComboBoxModel<string>(p.Id.ToString(), p.Nombre));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

    }
}
