﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Almacen;
using Eraltech.SGA.Shared.Models.Ubicacion;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class AlmacenMapper
    {
        public static AlmacenFormModel MapToModel(this Almacenes a)
        {
            return new AlmacenFormModel
            {
                Id = a.Id,
                CodigoAlmacen = a.CodigoAlmacen,
                Descripcion = a.Descripcion,
                IdPlanta = a.IdPlanta.ToString(),
                UbicacionEstandar = a.UbicacionEstandar,
                CodigoProductoManual = a.CodigoProductoManual
            };
        }

        public static Almacenes MapToEntity(this AlmacenFormModel a)
        {
            Almacenes entity = new Almacenes();
            entity.Id = a.Id;
            entity.CodigoAlmacen = a.CodigoAlmacen;
            entity.Descripcion = a.Descripcion;
            int planta = 0;
            if (int.TryParse(a.IdPlanta, out planta))
                entity.IdPlanta = planta;
            entity.UbicacionEstandar = a.UbicacionEstandar;
            entity.CodigoProductoManual = a.CodigoProductoManual;
            return entity;
        }

        public static AlmacenGridModel MapToGridModel(this Almacenes a)
        {
            List<UbicacionGridModel> ubicacionGridModelList = new List<UbicacionGridModel>();
            foreach (var ubicacion in a.Ubicaciones)
                ubicacionGridModelList.Add(ubicacion.MapToGridModel());

            return new AlmacenGridModel
            {
                Id = a.Id,
                CodigoAlmacen = a.CodigoAlmacen,
                Descripcion = a.Descripcion,
                IdPlanta = a.IdPlanta,
                PlantaDescripcion = a.IdPlantaNavigation?.Descripcion,
                UbicacionEstandar = a.UbicacionEstandar,
                CodigoProductoManual = a.CodigoProductoManual,
                Ubicaciones = ubicacionGridModelList
            };
        }

        public static Almacenes MapFromGridModelToEntity(this AlmacenGridModel a)
        {
            Almacenes entity = new Almacenes();
            entity.Id = a.Id;
            entity.CodigoAlmacen = a.CodigoAlmacen;
            entity.Descripcion = a.Descripcion;
            entity.IdPlanta = a.IdPlanta;
            entity.UbicacionEstandar = a.UbicacionEstandar;
            entity.CodigoProductoManual = a.CodigoProductoManual;
            return entity;
        }

    }
}
