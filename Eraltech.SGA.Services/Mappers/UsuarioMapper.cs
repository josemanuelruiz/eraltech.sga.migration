﻿using Eraltech.Login.Models;
using Eraltech.SGA.Shared.Models.AlmacenUsuario;
using Eraltech.SGA.Shared.Models.Usuario;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class UsuarioMapper
    {
        public static UsuarioFormModel MapToModel(this Usuarios u)
        {
            List<AlmacenUsuarioGridModel> almacenGridModelList = new List<AlmacenUsuarioGridModel>();
            foreach (var almacen in u.AlmacenesUsuarios)
                almacenGridModelList.Add(almacen.MapLoginEntityToGridModel());

            return new UsuarioFormModel
            {
                Id = u.Id,
                Login = u.Login,
                Password = u.Password,
                Descripcion = u.Descripcion,
                Estado = u.Estado,
                IdRol = u.IdRol.ToString(),
                Creacion = u.Creacion,
                IntentosLogin = u.IntentosLogin.HasValue ? u.IntentosLogin.Value : 0,
                Email = u.Email,
                Almacenes = almacenGridModelList
            };
        }

        public static Usuarios MapToEntity(this UsuarioFormModel u)
        {
            List<AlmacenesUsuarios> almacenesList = new List<AlmacenesUsuarios>();
            foreach (var almacen in u.Almacenes)
                almacenesList.Add(almacen.MapFromGridModelToLoginEntity());

            Usuarios entity = new Usuarios();
            entity.Id = u.Id;
            entity.Login = u.Login;
            entity.Password = u.Password;
            entity.Descripcion = u.Descripcion;
            entity.Estado = u.Estado;
            int rol = 0;
            if (int.TryParse(u.IdRol, out rol))
                entity.IdRol = rol;
            entity.Creacion = u.Creacion;
            entity.IntentosLogin = u.IntentosLogin;
            entity.Email = u.Email;
            entity.AlmacenesUsuarios = almacenesList;
            return entity;
        }

        public static UsuarioGridModel MapToGridModel(this Usuarios u)
        {
            List<AlmacenUsuarioGridModel> almacenGridModelList = new List<AlmacenUsuarioGridModel>();
            foreach (var almacen in u.AlmacenesUsuarios)
                almacenGridModelList.Add(almacen.MapLoginEntityToGridModel());

            return new UsuarioGridModel
            {
                Id = u.Id,
                Login = u.Login,
                Password = u.Password,
                Descripcion = u.Descripcion,
                IdEstado = u.Estado,
                RolDescripcion = u.IdRolNavigation?.Nombre,
                EstadoDescripcion = u.Estado,
                Creacion = u.Creacion,
                IntentosLogin = u.IntentosLogin.HasValue ? u.IntentosLogin.Value : 0,
                Email = u.Email,
                Almacenes = almacenGridModelList
            };
        }

        public static Usuarios MapFromGridModelToEntity(this UsuarioGridModel u)
        {
            Usuarios entity = new Usuarios();
            entity.Id = u.Id;
            entity.Login = u.Login;
            entity.Password = u.Password;
            entity.Descripcion = u.Descripcion;
            entity.Estado = u.IdEstado;
            entity.Creacion = u.Creacion;
            entity.IntentosLogin = u.IntentosLogin;
            entity.Email = u.Email;
            return entity;
        }
    }
}
