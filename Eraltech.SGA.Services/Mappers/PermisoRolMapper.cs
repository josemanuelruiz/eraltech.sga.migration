﻿using Eraltech.Login.Models;
using Eraltech.SGA.Shared.Models.PermisoRol;
using Eraltech.SGA.Shared.Models.Producto;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class PermisoRolMapper
    {
        public static PermisoRolGridModel MapToGridModel(this PermisosRoles u)
        {
            return new PermisoRolGridModel
            {
                IdRol = u.IdRol,
                IdPermiso = u.IdPermiso,
                CodigoPermiso = u.IdPermisoNavigation?.Codigo,
                DescripcionPermiso = u.IdPermisoNavigation?.Descripcion
            };
        }

        public static PermisosRoles MapFromGridModelToEntity(this PermisoRolGridModel u)
        {
            PermisosRoles entity = new PermisosRoles();
            entity.IdPermiso = u.IdPermiso;
            entity.IdRol = u.IdRol;
            return entity;
        }
    }
}
