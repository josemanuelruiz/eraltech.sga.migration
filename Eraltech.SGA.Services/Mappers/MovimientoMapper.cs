﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Movimiento;

namespace Eraltech.SGA.Services.Mappers
{
    public static class MovimientoMapper
    {
        public static MovimientoFormModel MapToModel(this Movimientos m)
        {
            int codigoUbicacion = 0;

            if (m.IdUbicacionNavigation != null)
                codigoUbicacion = int.Parse(m.IdUbicacionNavigation?.CodigoUbicacion);

            return new MovimientoFormModel
            {
                Id = m.Id,
                Created = m.Created,
                CreatedUser = m.CreatedUser,
                DetalleUbicacion = m.DetalleUbicacion,
                IdAlmacen = m.IdAlmacen.ToString(),
                IdDispositivo = m.IdDispositivo.ToString(),
                IdProducto = m.IdProducto.ToString(),
                IdTipoMovimiento = m.IdTipoMovimiento.ToString(),
                IdUbicacion = m.IdUbicacion.ToString(),
                IdZona = m.IdZona.ToString(),
                Modified = m.Modified,
                ModifiedUser = m.ModifiedUser,
                Stock = m.Stock,
                Automatico = m.Automatico,
                NombreProducto = m.IdProductoNavigation?.Nombre,
                CodigoProducto = m.IdProductoNavigation?.CodigoProducto,
                ImageUrl = m.IdProductoNavigation?.ImageUrl,
                AlmacenDescripcion = m.IdAlmacenNavigation?.Descripcion,
                DispositivoSerialNumber = m.IdDispositivoNavigation?.SerialNumber,
                DispositivoDescripcion = m.IdDispositivoNavigation?.Descripcion,
                TipoMovimientoDescripcion = m.IdTipoMovimientoNavigation?.Descripcion,
                ZonaDescripcion = m.IdZonaNavigation?.Descripcion,
                UbicacionDescripcion = string.Format("{0:00-00-00-0}", codigoUbicacion)
            };
        }

        public static Movimientos MapToEntity(this MovimientoFormModel m)
        {
            Movimientos entity = new Movimientos();
            entity.Id = m.Id;
            entity.Created = m.Created;
            entity.CreatedUser = m.CreatedUser;
            entity.DetalleUbicacion = m.DetalleUbicacion;
            entity.IdAlmacen = int.Parse(m.IdAlmacen);
            if (!string.IsNullOrEmpty(m.IdDispositivo))
                entity.IdDispositivo = int.Parse(m.IdDispositivo);
            if (!string.IsNullOrEmpty(m.IdProducto))
                entity.IdProducto = int.Parse(m.IdProducto);
            entity.IdTipoMovimiento = int.Parse(m.IdTipoMovimiento);
            if (!string.IsNullOrEmpty(m.IdZona))
                entity.IdZona = int.Parse(m.IdZona);
            if (!string.IsNullOrEmpty(m.IdUbicacion))
                entity.IdUbicacion = int.Parse(m.IdUbicacion);
            entity.Modified = m.Modified;
            entity.ModifiedUser = m.ModifiedUser;
            entity.Stock = m.Stock;
            return entity;
        }

        public static MovimientoGridModel MapToGridModel(this Movimientos m)
        {
            int codigoUbicacion = 0;

            if (m.IdUbicacionNavigation != null)
                codigoUbicacion = int.Parse(m.IdUbicacionNavigation?.CodigoUbicacion);

            return new MovimientoGridModel
            {
                Id = m.Id,
                Created = m.Created,
                CreatedUser = m.CreatedUser,
                DetalleUbicacion = m.DetalleUbicacion,
                IdAlmacen = m.IdAlmacen,
                IdDispositivo = m.IdDispositivo,
                IdProducto = m.IdProducto,
                IdTipoMovimiento = m.IdTipoMovimiento,
                IdZona = m.IdZona,
                Modified = m.Modified,
                ModifiedUser = m.ModifiedUser,
                Stock = m.Stock,
                IdUbicacion = m.IdUbicacion,
                Automatico = m.Automatico,
                NombreProducto = m.IdProductoNavigation?.Nombre,
                ImageUrl = m.IdProductoNavigation?.ImageUrl,
                AlmacenDescripcion = m.IdAlmacenNavigation?.Descripcion,
                DispositivoSerialNumber = m.IdDispositivoNavigation?.SerialNumber,
                DispositivoDescripcion = m.IdDispositivoNavigation?.Descripcion,
                TipoMovimientoDescripcion = m.IdTipoMovimientoNavigation?.Descripcion,
                ZonaDescripcion = m.IdZonaNavigation?.Descripcion,
                UbicacionDescripcion = string.Format("{0:00-00-00-0}", codigoUbicacion)
            };
        }

        public static Movimientos MapFromGridModelToEntity(this MovimientoGridModel m)
        {
            Movimientos entity = new Movimientos();
            entity.Id = m.Id;
            entity.Created = m.Created;
            entity.CreatedUser = m.CreatedUser;
            entity.DetalleUbicacion = m.DetalleUbicacion;
            entity.IdAlmacen = m.IdAlmacen;
            entity.IdDispositivo = m.IdDispositivo;
            entity.IdProducto = m.IdProducto;
            entity.IdTipoMovimiento = m.IdTipoMovimiento;
            entity.IdUbicacion = m.IdUbicacion;
            entity.IdZona = m.IdZona;
            entity.Modified = m.Modified;
            entity.ModifiedUser = m.ModifiedUser;
            entity.Stock = m.Stock;
            return entity;
        }

    }
}
