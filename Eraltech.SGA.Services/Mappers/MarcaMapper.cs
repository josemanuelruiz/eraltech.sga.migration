﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Marca;
using Eraltech.SGA.Shared.Models.Producto;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class MarcaMapper
    {
        public static MarcaFormModel MapToModel(this Marcas u)
        {
            return new MarcaFormModel
            {
                Id = u.Id,
                Descripcion = u.Descripcion
            };
        }

        public static Marcas MapToEntity(this MarcaFormModel u)
        {
            Marcas entity = new Marcas();
            entity.Id = u.Id;
            entity.Descripcion = u.Descripcion;
            return entity;
        }

        public static MarcaGridModel MapToGridModel(this Marcas u)
        {
            List<ProductoGridModel> productoGridModelList = new List<ProductoGridModel>();
            foreach (var producto in u.Productos)
                productoGridModelList.Add(producto.MapToGridModel());

            return new MarcaGridModel
            {
                Id = u.Id,
                Descripcion = u.Descripcion,
                Productos = productoGridModelList
            };
        }

        public static Marcas MapFromGridModelToEntity(this MarcaGridModel u)
        {
            Marcas entity = new Marcas();
            entity.Id = u.Id;
            entity.Descripcion = u.Descripcion;
            return entity;
        }
    }
}
