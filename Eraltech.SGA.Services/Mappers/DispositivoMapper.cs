﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Dispositivo;
using Eraltech.SGA.Shared.Models.Movimiento;
using Eraltech.SGA.Shared.Models.Stock;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class DispositivoMapper
    {
        public static DispositivoFormModel MapToModel(this Dispositivos d)
        {
            bool serialNumberRequired = false;
            if (d.IdProductoNavigation != null)
                serialNumberRequired = d.IdProductoNavigation.SerialNumberRequired;

            return new DispositivoFormModel
            {
                Id = d.Id,
                IdProducto = d.IdProducto.ToString(),
                SerialNumber = d.SerialNumber,
                Descripcion = d.Descripcion,
                FechaEntrega = d.IdProductoNavigation?.FechaEntrega,
                SerialNumberRequired = serialNumberRequired,
                NombreProducto = d.IdProductoNavigation?.Nombre,
                ImageUrl = d.IdProductoNavigation?.ImageUrl,
                FechaAlta = d.FechaAlta,
                FechaBaja = d.FechaBaja,
            };
        }

        public static Dispositivos MapToEntity(this DispositivoFormModel d)
        {
            Dispositivos entity = new Dispositivos();
            entity.Id = d.Id;
            entity.IdProducto = int.Parse(d.IdProducto);
            entity.SerialNumber = d.SerialNumber;
            entity.Descripcion = d.Descripcion;
            entity.FechaAlta = d.FechaAlta;
            entity.FechaBaja = d.FechaBaja;
            return entity;
        }

        public static DispositivoGridModel MapToGridModel(this Dispositivos d)
        {
            List<StockGridModel> stockGridModelList = new List<StockGridModel>();
            foreach (var stock in d.Stocks)
                stockGridModelList.Add(stock.MapToGridModel());

            List<MovimientoGridModel> movimientoGridModelList = new List<MovimientoGridModel>();
            foreach (var movimiento in d.Movimientos)
                movimientoGridModelList.Add(movimiento.MapToGridModel());

            bool serialNumberRequired = false;
            if (d.IdProductoNavigation != null)
                serialNumberRequired = d.IdProductoNavigation.SerialNumberRequired;

            return new DispositivoGridModel
            {
                Id = d.Id,
                IdProducto = d.IdProducto,
                SerialNumber = d.SerialNumber,
                Descripcion = d.Descripcion,
                SerialNumberRequired = serialNumberRequired,
                NombreProducto = d.IdProductoNavigation?.Nombre,
                ImageUrl = d.IdProductoNavigation ?.ImageUrl,
                FechaAlta = d.FechaAlta,
                FechaBaja = d.FechaBaja,

                Stocks = stockGridModelList,
                Movimientos = movimientoGridModelList
            };
        }

        public static Dispositivos MapFromGridModelToEntity(this DispositivoGridModel d)
        {
            Dispositivos entity = new Dispositivos();
            entity.Id = d.Id;
            entity.IdProducto = d.IdProducto;
            entity.SerialNumber = d.SerialNumber;
            entity.Descripcion = d.Descripcion;
            entity.FechaAlta = d.FechaAlta;
            entity.FechaBaja = d.FechaBaja;
            return entity;
        }

    }
}
