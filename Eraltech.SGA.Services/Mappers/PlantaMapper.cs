﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Almacen;
using Eraltech.SGA.Shared.Models.Planta;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class PlantaMapper
    {
        public static PlantaFormModel MapToModel(this Plantas p)
        {
            return new PlantaFormModel
            {
                Id = p.Id,
                CodigoPlanta = p.CodigoPlanta,
                Descripcion = p.Descripcion,
                Direccion = p.Direccion,
                CodigoPostal = p.CodigoPostal,
                IdFabrica = p.IdFabrica.ToString()
            };
        }

        public static Plantas MapToEntity(this PlantaFormModel p)
        {
            Plantas entity = new Plantas();
            entity.Id = p.Id;
            entity.CodigoPlanta = p.CodigoPlanta;
            entity.Descripcion = p.Descripcion;
            entity.Direccion = p.Direccion;
            entity.CodigoPostal = p.CodigoPostal;
            int fabrica = 0;
            if (int.TryParse(p.IdFabrica, out fabrica))
                entity.IdFabrica = fabrica;
            return entity;
        }

        public static PlantaGridModel MapToGridModel(this Plantas p)
        {
            List<AlmacenGridModel> almacenesGridModelList = new List<AlmacenGridModel>();
            foreach (var almacen in p.Almacenes)
                almacenesGridModelList.Add(almacen.MapToGridModel());

            return new PlantaGridModel
            {
                Id = p.Id,
                CodigoPlanta = p.CodigoPlanta,
                Descripcion = p.Descripcion,
                Direccion = p.Direccion,
                CodigoPostal = p.CodigoPostal,
                IdFabrica = p.IdFabrica,
                FabricaDescripcion = p.IdFabricaNavigation?.Descripcion,
                Almacenes = almacenesGridModelList
            };
        }

        public static Plantas MapFromGridModelToEntity(this PlantaGridModel p)
        {
            Plantas entity = new Plantas();
            entity.Id = p.Id;
            entity.CodigoPlanta = p.CodigoPlanta;
            entity.Descripcion = p.Descripcion;
            entity.Direccion = p.Direccion;
            entity.CodigoPostal = p.CodigoPostal;
            entity.IdFabrica = p.IdFabrica;
            return entity;
        }
    }
}
