﻿using Eraltech.Login.Models;
using Eraltech.SGA.Shared.Models.Permiso;
using Eraltech.SGA.Shared.Models.Usuario;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class PermisoMapper
    {
        //public static PermisoFormModel MapToModel(this Permisos u)
        //{
        //    return new PermisoFormModel
        //    {
        //        Id = u.Id,
        //        Nombre = u.Nombre,
        //        GrupoDominio = u.GrupoDominio
        //    };
        //}

        //public static Permisos MapToEntity(this PermisoFormModel u)
        //{
        //    Permisos entity = new Permisos();
        //    entity.Id = u.Id;
        //    entity.Nombre = u.Nombre;
        //    entity.GrupoDominio = u.GrupoDominio;
        //    return entity;
        //}

        public static PermisoGridModel MapToGridModel(this Permisos u)
        {
            //List<PermisoGridModel> permisoGridModelList = new List<PermisoGridModel>();
            //foreach (var permiso in u.PermisosRoles)
            //    permisoGridModelList.Add(permiso.MapToGridModel());

            //List<UsuarioGridModel> usuarioGridModelList = new List<UsuarioGridModel>();
            //foreach (var usuario in u.Usuarios)
            //    usuarioGridModelList.Add(usuario.MapToGridModel());

            return new PermisoGridModel
            {
                Id = u.Id,
                Codigo = u.Codigo,
                Descripcion = u.Descripcion,
                //Checked = Checked
            };
        }

        public static Permisos MapFromGridModelToEntity(this PermisoGridModel u)
        {
            Permisos entity = new Permisos();
            entity.Id = u.Id;
            entity.Codigo = u.Codigo;
            entity.Descripcion = u.Descripcion;
            return entity;
        }
    }
}
