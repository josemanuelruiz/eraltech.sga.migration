﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Stock;

namespace Eraltech.SGA.Services.Mappers
{
    public static class StockMapper
    {
        public static StockFormModel MapToModel(this Stocks s)
        {
            return new StockFormModel
            {
                Id = s.Id,
                IdAlmacen = s.IdAlmacen.ToString(),
                IdUbicacion = s.IdUbicacion.ToString(),
                IdProducto = s.IdProducto.ToString(),
                IdDispositivo = s.IdDispositivo.ToString(),
                CurrentStock = s.Stock,
                StockMinimo = s.StockMinimo,
                StockMaximo = s.StockMaximo,
                BuenEstado = s.BuenEstado,
                MalEstado = s.MalEstado,
                PendienteClasificar = s.PendienteClasificar,
                PendienteRecibir = s.PendienteRecibir,
                DetalleUbicacion = s.DetalleUbicacion,
                AlmacenDescripcion = s.IdAlmacenNavigation?.Descripcion,
                UbicacionDescripcion = s.IdUbicacionNavigation?.Descripcion
            };
        }

        public static Stocks MapToEntity(this StockFormModel s)
        {
            Stocks entity = new Stocks();
            entity.Id = s.Id;
            entity.IdAlmacen = int.Parse(s.IdAlmacen);
            entity.IdUbicacion = int.Parse(s.IdUbicacion);
            entity.IdProducto = int.Parse(s.IdProducto);
            if (!string.IsNullOrEmpty(s.IdDispositivo))
                entity.IdDispositivo = int.Parse(s.IdDispositivo);
            entity.Stock = s.CurrentStock;
            entity.StockMinimo = s.StockMinimo;
            entity.StockMaximo = s.StockMaximo;
            entity.BuenEstado = s.BuenEstado;
            entity.MalEstado = s.MalEstado;
            entity.PendienteClasificar = s.PendienteClasificar;
            entity.PendienteRecibir = s.PendienteRecibir;
            entity.DetalleUbicacion = s.DetalleUbicacion;
            return entity;
        }

        public static StockGridModel MapToGridModel(this Stocks s)
        {
            string codigoUbicacion = "";
            int codigo;

            if (s.IdUbicacionNavigation != null)
            {
                if (int.TryParse(s.IdUbicacionNavigation?.CodigoUbicacion, out codigo))
                    codigoUbicacion = string.Format("{0:00-00-00-0}", codigo);
                else
                    codigoUbicacion = s.IdUbicacionNavigation?.CodigoUbicacion;

            }

            return new StockGridModel
            {
                Id = s.Id,
                IdAlmacen = s.IdAlmacen,
                IdProducto = s.IdProducto,
                IdDispositivo = s.IdDispositivo,
                IdUbicacion = s.IdUbicacion,
                CurrentStock = s.Stock,
                StockMinimo = s.StockMinimo,
                StockMaximo = s.StockMaximo,
                BuenEstado = s.BuenEstado,
                MalEstado = s.MalEstado,
                PendienteClasificar = s.PendienteClasificar,
                PendienteRecibir = s.PendienteRecibir,
                DetalleUbicacion = s.DetalleUbicacion,
                AlmacenDescripcion = s.IdAlmacenNavigation?.Descripcion,
                UbicacionDescripcion = codigoUbicacion,
                ProductoDescripcion = s.IdProductoNavigation?.Nombre,
                DispositivoSerialNumber = s.IdDispositivoNavigation?.SerialNumber,
                DispositivoDescripcion = s.IdDispositivoNavigation?.Descripcion,
            };
        }

        public static Stocks MapFromGridModelToEntity(this StockGridModel s)
        {
            Stocks entity = new Stocks();
            entity.Id = s.Id;
            entity.IdAlmacen = s.IdAlmacen;
            entity.IdUbicacion = s.IdUbicacion;
            entity.IdProducto = s.IdProducto;
            entity.IdDispositivo = s.IdDispositivo;
            entity.Stock = s.CurrentStock;
            entity.StockMinimo = s.StockMinimo;
            entity.StockMaximo = s.StockMaximo;
            entity.BuenEstado = s.BuenEstado;
            entity.MalEstado = s.MalEstado;
            entity.PendienteClasificar = s.PendienteClasificar;
            entity.PendienteRecibir = s.PendienteRecibir;
            entity.DetalleUbicacion = s.DetalleUbicacion;
            return entity;
        }
    }
}
