﻿using Eraltech.Login.Models;
using Eraltech.SGA.Shared.Models.Permiso;
using Eraltech.SGA.Shared.Models.PermisoRol;
using Eraltech.SGA.Shared.Models.Rol;
using Eraltech.SGA.Shared.Models.Usuario;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class RolMapper
    {
        public static RolFormModel MapToModel(this Roles u)
        {
            List<PermisoRolGridModel> permisoGridModelList = new List<PermisoRolGridModel>();
            foreach (var permiso in u.PermisosRoles)
                permisoGridModelList.Add(permiso.MapToGridModel());

            return new RolFormModel
            {
                Id = u.Id,
                Nombre = u.Nombre,
                GrupoDominio = u.GrupoDominio,
                PermisosRoles = permisoGridModelList,
            };
        }

        public static Roles MapToEntity(this RolFormModel u)
        {
            List<PermisosRoles> permisosList = new List<PermisosRoles>();
            foreach (var permiso in u.PermisosRoles)
                permisosList.Add(permiso.MapFromGridModelToEntity());

            Roles entity = new Roles();
            entity.Id = u.Id;
            entity.Nombre = u.Nombre;
            entity.GrupoDominio = u.GrupoDominio;
            entity.PermisosRoles = permisosList;
            return entity;
        }

        public static RolGridModel MapToGridModel(this Roles u)
        {
            List<PermisoRolGridModel> permisoGridModelList = new List<PermisoRolGridModel>();
            foreach (var permiso in u.PermisosRoles)
                permisoGridModelList.Add(permiso.MapToGridModel());

            List<UsuarioGridModel> usuarioGridModelList = new List<UsuarioGridModel>();
            foreach (var usuario in u.Usuarios)
                usuarioGridModelList.Add(usuario.MapToGridModel());

            return new RolGridModel
            {
                Id = u.Id,
                Nombre = u.Nombre,
                GrupoDominio = u.GrupoDominio,
                PermisosRoles = permisoGridModelList,
                Usuarios = usuarioGridModelList
            };
        }

        public static Roles MapFromGridModelToEntity(this RolGridModel u)
        {
            Roles entity = new Roles();
            entity.Id = u.Id;
            entity.Nombre = u.Nombre;
            entity.GrupoDominio = u.GrupoDominio;
            return entity;
        }
    }
}
