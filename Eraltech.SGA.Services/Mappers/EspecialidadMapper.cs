﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Especialidad;
using Eraltech.SGA.Shared.Models.Producto;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class EspecialidadMapper
    {
        public static EspecialidadFormModel MapToModel(this Especialidades u)
        {
            return new EspecialidadFormModel
            {
                Id = u.Id,
                Descripcion = u.Descripcion
            };
        }

        public static Especialidades MapToEntity(this EspecialidadFormModel u)
        {
            Especialidades entity = new Especialidades();
            entity.Id = u.Id;
            entity.Descripcion = u.Descripcion;
            return entity;
        }

        public static EspecialidadGridModel MapToGridModel(this Especialidades u)
        {
            List<ProductoGridModel> productoGridModelList = new List<ProductoGridModel>();
            foreach (var producto in u.Productos)
                productoGridModelList.Add(producto.MapToGridModel());

            return new EspecialidadGridModel
            {
                Id = u.Id,
                Descripcion = u.Descripcion,
                Productos = productoGridModelList
            };
        }

        public static Especialidades MapFromGridModelToEntity(this EspecialidadGridModel u)
        {
            Especialidades entity = new Especialidades();
            entity.Id = u.Id;
            entity.Descripcion = u.Descripcion;
            return entity;
        }
    }
}
