﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Dispositivo;
using Eraltech.SGA.Shared.Models.Movimiento;
using Eraltech.SGA.Shared.Models.Producto;
using Eraltech.SGA.Shared.Models.Stock;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class ProductoMapper
    {
        public static ProductoFormModel MapToModel(this Productos p)
        {
            return new ProductoFormModel
            {
                Id = p.Id,
                CodigoProducto = p.CodigoProducto,
                Nombre = p.Nombre,
                Referencia = p.Referencia,
                IdEspecialidad = p.IdEspecialidad.ToString(),
                IdFamilia = p.IdFamilia.ToString(),
                IdMarca = p.IdMarca.ToString(),
                Observaciones = p.Observaciones,
                Instalacion = p.Instalacion,
                Precio = p.Precio,
                FechaEntrega = p.FechaEntrega,
                Pedido = p.Pedido,
                PerteneceKit = p.PerteneceKit,
                Seleccionado = p.Seleccionado,
                SerialNumberRequired = p.SerialNumberRequired,
                ImageUrl = p.ImageUrl,
                FamiliaDescripcion = p.IdFamiliaNavigation?.Descripcion,
                MarcaDescripcion = p.IdMarcaNavigation?.Descripcion,
                FechaAlta = p.FechaAlta,
                FechaBaja = p.FechaBaja,
            };
        }

        public static Productos MapToEntity(this ProductoFormModel p)
        {
            Productos entity = new Productos();
            entity.Id = p.Id;
            entity.CodigoProducto = p.CodigoProducto;
            entity.Nombre = p.Nombre;
            entity.Referencia = p.Referencia;
            entity.IdEspecialidad = int.Parse(p.IdEspecialidad);
            entity.IdFamilia = int.Parse(p.IdFamilia);
            entity.IdMarca = int.Parse(p.IdMarca);
            entity.Observaciones = p.Observaciones;
            entity.Instalacion = p.Instalacion;
            entity.Precio = p.Precio;
            entity.FechaEntrega = p.FechaEntrega;
            entity.Pedido = p.Pedido;
            entity.PerteneceKit = p.PerteneceKit;
            entity.Seleccionado = p.Seleccionado;
            entity.SerialNumberRequired = p.SerialNumberRequired;
            entity.ImageUrl = p.ImageUrl;
            entity.FechaAlta = p.FechaAlta;
            entity.FechaBaja = p.FechaBaja;
            return entity;
        }

        public static ProductoGridModel MapToGridModel(this Productos p)
        {
            List<StockGridModel> stockGridModelList = new List<StockGridModel>();
            foreach (var stock in p.Stocks)
                stockGridModelList.Add(stock.MapToGridModel());

            List<MovimientoGridModel> movimientoGridModelList = new List<MovimientoGridModel>();
            foreach (var movimiento in p.Movimientos)
                movimientoGridModelList.Add(movimiento.MapToGridModel());

            List<DispositivoGridModel> dispositivoGridModelList = new List<DispositivoGridModel>();
            foreach (var dispositivo in p.Dispositivos)
                dispositivoGridModelList.Add(dispositivo.MapToGridModel());

            //TODO - Descomentar cuando se implemente la gestión de pedidos a proveedores - LineasPedidoGridModel
            //List<LineasPedidoGridModel> lineasPedidoGridModelList = new List<LineasPedidoGridModel>();
            //foreach (var lineasPedido in p.LineasPedidos)
            //    lineasPedidoGridModelList.Add(lineasPedido.MapToGridModel());

            return new ProductoGridModel
            {
                Id = p.Id,
                CodigoProducto = p.CodigoProducto,
                Nombre = p.Nombre,
                Referencia = p.Referencia,
                IdEspecialidad = p.IdEspecialidad,
                IdFamilia = p.IdFamilia,
                IdMarca = p.IdMarca,
                Observaciones = p.Observaciones,
                Instalacion = p.Instalacion,
                Precio = p.Precio,
                FechaEntrega = p.FechaEntrega,
                Pedido = p.Pedido,
                PerteneceKit = p.PerteneceKit,
                Seleccionado = p.Seleccionado,
                SerialNumberRequired = p.SerialNumberRequired,
                ImageUrl = p.ImageUrl,
                EspecialidadDescripcion = p.IdEspecialidadNavigation?.Descripcion,
                FamiliaDescripcion = p.IdFamiliaNavigation?.Descripcion,
                MarcaDescripcion = p.IdMarcaNavigation?.Descripcion,
                ImageUrlLoaded = false,
                FechaAlta = p.FechaAlta,
                FechaBaja = p.FechaBaja,

                Stocks = stockGridModelList,
                Movimientos = movimientoGridModelList,
                Dispositivos = dispositivoGridModelList,
                //LineasPedidos = lineasPedidoGridModelList
            };
        }

        public static Productos MapFromGridModelToEntity(this ProductoGridModel p)
        {
            Productos entity = new Productos();
            entity.Id = p.Id;
            entity.CodigoProducto = p.CodigoProducto;
            entity.Nombre = p.Nombre;
            entity.Referencia = p.Referencia;
            entity.IdEspecialidad = p.IdEspecialidad;
            entity.IdFamilia = p.IdFamilia;
            entity.IdMarca = p.IdMarca;
            entity.Observaciones = p.Observaciones;
            entity.Instalacion = p.Instalacion;
            entity.Precio = p.Precio;
            entity.FechaEntrega = p.FechaEntrega;
            entity.Pedido = p.Pedido;
            entity.PerteneceKit = p.PerteneceKit;
            entity.Seleccionado = p.Seleccionado;
            entity.SerialNumberRequired = p.SerialNumberRequired;
            entity.ImageUrl = p.ImageUrl;
            entity.FechaAlta = p.FechaAlta;
            entity.FechaBaja = p.FechaBaja;
            return entity;
        }
    }
}
