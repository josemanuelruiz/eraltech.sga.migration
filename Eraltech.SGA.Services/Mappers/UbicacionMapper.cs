﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Ubicacion;
using Eraltech.SGA.Shared.Models.Planta;
using System.Collections.Generic;
using Eraltech.SGA.Shared.Models.Stock;

namespace Eraltech.SGA.Services.Mappers
{
    public static class UbicacionMapper
    {
        public static UbicacionFormModel MapToModel(this Ubicaciones u)
        {
            return new UbicacionFormModel
            {
                Id = u.Id,
                CodigoUbicacion = u.CodigoUbicacion,
                Descripcion = u.Descripcion,
                IdAlmacen = u.IdAlmacen.ToString(),
                PosX = u.PosX,
                PosY = u.PosY,
                UbicacionStandar = u.IdAlmacenNavigation != null ? u.IdAlmacenNavigation.UbicacionEstandar : false
            };
        }

        public static Ubicaciones MapToEntity(this UbicacionFormModel u)
        {
            Ubicaciones entity = new Ubicaciones();
            entity.Id = u.Id;
            entity.CodigoUbicacion = u.CodigoUbicacion;
            entity.Descripcion = u.Descripcion;
            int almacen = 0;
            if (int.TryParse(u.IdAlmacen, out almacen))
                entity.IdAlmacen = almacen;
            entity.PosX = u.PosX;
            entity.PosY = u.PosY;
            return entity;
        }

        public static UbicacionGridModel MapToGridModel(this Ubicaciones u)
        {
            List<StockGridModel> stockGridModelList = new List<StockGridModel>();
            foreach (var stock in u.Stocks)
                stockGridModelList.Add(stock.MapToGridModel());

            string codigoUbicacion;
            int codigo;
            if (int.TryParse(u.CodigoUbicacion, out codigo))
                codigoUbicacion = string.Format("{0:00-00-00-0}", codigo);
            else
                codigoUbicacion = u.CodigoUbicacion;

            bool ubicacionStandar = false;
            if (u.IdAlmacenNavigation != null)
                ubicacionStandar = u.IdAlmacenNavigation.UbicacionEstandar;
            return new UbicacionGridModel
            {
                Id = u.Id,
                CodigoUbicacion = codigoUbicacion,
                Descripcion = u.Descripcion,
                IdAlmacen = u.IdAlmacen,
                AlmacenDescripcion = u.IdAlmacenNavigation?.Descripcion,
                PosX = u.PosX,
                PosY = u.PosY,
                UbicacionStandar = ubicacionStandar,
                Stocks = stockGridModelList
            };
        }

        public static Ubicaciones MapFromGridModelToEntity(this UbicacionGridModel u)
        {
            Ubicaciones entity = new Ubicaciones();
            entity.Id = u.Id;
            entity.CodigoUbicacion = u.CodigoUbicacion;
            entity.Descripcion = u.Descripcion;
            entity.IdAlmacen = u.IdAlmacen;
            entity.PosX = u.PosX;
            entity.PosY = u.PosY;
            return entity;
        }
    }
}
