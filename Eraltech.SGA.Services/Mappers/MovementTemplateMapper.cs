﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.MovementTemplate;
using System;
using System.Collections.Generic;
using System.Text;

namespace Eraltech.SGA.Services.Mappers
{
    public static class MovementTemplateMapper
    {
        public static MovementTemplateFormModel MapToModel(this MovementTemplates m)
        {
            //int codigoUbicacion = 0;

            //if (m.IdUbicacionNavigation != null)
            //    codigoUbicacion = int.Parse(m.IdUbicacionNavigation?.CodigoUbicacion);

            return new MovementTemplateFormModel
            {
                Id = m.Id,
                Descripcion = m.Descripcion,
                IdTipoMovimiento = m.IdTipoMovimiento.ToString(),
                IdAlmacenOrigen = m.IdAlmacenOrigen.ToString(),
                IdUbicacionOrigen = m.IdUbicacionOrigen.ToString(),
                IdZonaOrigen = m.IdZonaOrigen.ToString(),
                IdAlmacenDestino = m.IdAlmacenDestino.ToString(),
                IdUbicacionDestino = m.IdUbicacionDestino.ToString(),
                IdZonaDestino = m.IdZonaDestino.ToString(),
                IdProducto = m.IdProducto.ToString(),
                Created = m.Created,
                CreatedUser = m.CreatedUser,
                Modified = m.Modified,
                ModifiedUser = m.ModifiedUser,
                Public = m.Public,
                Automatico = m.Automatico,
                //NombreProducto = m.IdProductoNavigation?.Nombre,
                //ImageUrl = m.IdProductoNavigation?.ImageUrl,
                //AlmacenOrigenDescripcion = m.IdAlmacenNavigation?.Descripcion,
                //TipoMovimientoDescripcion = m.IdTipoMovimientoNavigation?.Descripcion,
                //ZonaDescripcion = m.IdZonaNavigation?.Descripcion,
                //UbicacionDescripcion = string.Format("{0:00-00-00-0}", codigoUbicacion)
            };
        }

        public static MovementTemplates MapToEntity(this MovementTemplateFormModel m)
        {
            MovementTemplates entity = new MovementTemplates();
            entity.Id = m.Id;
            entity.Descripcion = m.Descripcion;
            entity.IdTipoMovimiento = int.Parse(m.IdTipoMovimiento);
            if (!string.IsNullOrEmpty(m.IdAlmacenOrigen))
                entity.IdAlmacenOrigen = int.Parse(m.IdAlmacenOrigen);
            if (!string.IsNullOrEmpty(m.IdZonaOrigen))
                entity.IdZonaOrigen = int.Parse(m.IdZonaOrigen);
            if (!string.IsNullOrEmpty(m.IdUbicacionOrigen))
                entity.IdUbicacionOrigen = int.Parse(m.IdUbicacionOrigen);
            if (!string.IsNullOrEmpty(m.IdAlmacenOrigen))
                entity.IdAlmacenOrigen = int.Parse(m.IdAlmacenOrigen);
            if (!string.IsNullOrEmpty(m.IdZonaOrigen))
                entity.IdZonaOrigen = int.Parse(m.IdZonaOrigen);
            if (!string.IsNullOrEmpty(m.IdUbicacionOrigen))
                entity.IdUbicacionOrigen = int.Parse(m.IdUbicacionOrigen);
            if (!string.IsNullOrEmpty(m.IdProducto))
                entity.IdProducto = int.Parse(m.IdProducto);
            entity.Created = m.Created;
            entity.CreatedUser = m.CreatedUser;
            entity.Modified = m.Modified;
            entity.ModifiedUser = m.ModifiedUser;
            entity.Public = m.Public;
            entity.Automatico = m.Automatico;
            return entity;
        }

        public static MovementTemplateGridModel MapToGridModel(this MovementTemplates m)
        {
            int codigoUbicacionOrigen = 0;
            int codigoUbicacionDestino = 0;

            if (m.IdUbicacionOrigenNavigation != null)
                codigoUbicacionOrigen = int.Parse(m.IdUbicacionOrigenNavigation?.CodigoUbicacion);
            if (m.IdUbicacionDestinoNavigation != null)
                codigoUbicacionDestino = int.Parse(m.IdUbicacionDestinoNavigation?.CodigoUbicacion);

            return new MovementTemplateGridModel
            {
                Id = m.Id,
                Descripcion = m.Descripcion,
                IdTipoMovimiento = m.IdTipoMovimiento,
                IdAlmacenOrigen = m.IdAlmacenOrigen,
                IdUbicacionOrigen = m.IdUbicacionOrigen,
                IdZonaOrigen = m.IdZonaOrigen,
                IdAlmacenDestino = m.IdAlmacenDestino,
                IdUbicacionDestino = m.IdUbicacionDestino,
                IdZonaDestino = m.IdZonaDestino,
                IdProducto = m.IdProducto,
                Created = m.Created,
                CreatedUser = m.CreatedUser,
                Modified = m.Modified,
                ModifiedUser = m.ModifiedUser,
                Public = m.Public,
                Automatico = m.Automatico,
                TipoMovimientoDescripcion = m.IdTipoMovimientoNavigation?.Descripcion,
                NombreProducto = m.IdProductoNavigation?.Nombre,
                ImageUrl = m.IdProductoNavigation?.ImageUrl,
                AlmacenOrigenDescripcion = m.IdAlmacenOrigenNavigation?.Descripcion,
                ZonaOrigenDescripcion = m.IdZonaOrigenNavigation?.Descripcion,
                UbicacionOrigenDescripcion = string.Format("{0:00-00-00-0}", codigoUbicacionOrigen),
                AlmacenDestinoDescripcion = m.IdAlmacenDestinoNavigation?.Descripcion,
                ZonaDestinoDescripcion = m.IdZonaDestinoNavigation?.Descripcion,
                UbicacionDestinoDescripcion = string.Format("{0:00-00-00-0}", codigoUbicacionDestino)
            };
        }

        public static MovementTemplates MapFromGridModelToEntity(this MovementTemplateGridModel m)
        {
            MovementTemplates entity = new MovementTemplates();
            entity.Id = m.Id;
            entity.Descripcion = m.Descripcion;
            entity.IdTipoMovimiento = m.IdTipoMovimiento;
            entity.IdAlmacenOrigen = m.IdAlmacenOrigen;
            entity.IdUbicacionOrigen = m.IdUbicacionOrigen;
            entity.IdZonaOrigen = m.IdZonaOrigen;
            entity.IdAlmacenDestino = m.IdAlmacenDestino;
            entity.IdUbicacionDestino = m.IdUbicacionDestino;
            entity.IdZonaDestino = m.IdZonaDestino;
            entity.IdProducto = m.IdProducto;
            entity.Created = m.Created;
            entity.CreatedUser = m.CreatedUser;
            entity.Modified = m.Modified;
            entity.ModifiedUser = m.ModifiedUser;
            entity.Public = m.Public;
            entity.Automatico = m.Automatico;
            return entity;
        }
    }
}
