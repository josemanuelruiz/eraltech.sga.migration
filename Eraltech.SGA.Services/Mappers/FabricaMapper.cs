﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Fabrica;
using Eraltech.SGA.Shared.Models.Planta;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class FabricaMapper
    {
        public static FabricaFormModel MapToModel(this Fabricas f)
        {
            return new FabricaFormModel
            {
                Id = f.Id,
                CodigoFabrica = f.CodigoFabrica,
                Descripcion = f.Descripcion,
                Direccion = f.Direccion,
                CodigoPostal = f.CodigoPostal
            };
        }

        public static Fabricas MapToEntity(this FabricaFormModel f)
        {
            Fabricas entity = new Fabricas();
            entity.Id = f.Id;
            entity.CodigoFabrica = f.CodigoFabrica;
            entity.Descripcion = f.Descripcion;
            entity.Direccion = f.Direccion;
            entity.CodigoPostal = f.CodigoPostal;
            return entity;
        }

        public static FabricaGridModel MapToGridModel(this Fabricas f)
        {
            List<PlantaGridModel> plantaGridModelList = new List<PlantaGridModel>();
            foreach (var planta in f.Plantas)
                plantaGridModelList.Add(planta.MapToGridModel());

            return new FabricaGridModel
            {
                Id = f.Id,
                CodigoFabrica = f.CodigoFabrica,
                Descripcion = f.Descripcion,
                Direccion = f.Direccion,
                CodigoPostal = f.CodigoPostal,
                Plantas = plantaGridModelList
            };
        }

        public static Fabricas MapFromGridModelToEntity(this FabricaGridModel f)
        {
            Fabricas entity = new Fabricas();
            entity.Id = f.Id;
            entity.CodigoFabrica = f.CodigoFabrica;
            entity.Descripcion = f.Descripcion;
            entity.Direccion = f.Direccion;
            entity.CodigoPostal = f.CodigoPostal;
            return entity;
        }

    }
}
