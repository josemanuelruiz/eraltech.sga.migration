﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.AlmacenUsuario;

namespace Eraltech.SGA.Services.Mappers
{
    public static class AlmacenUsuarioMapper
    {
        public static AlmacenUsuarioGridModel MapLoginEntityToGridModel(this Login.Models.AlmacenesUsuarios u)
        {
            return new AlmacenUsuarioGridModel
            {
                IdUsuario = u.IdUsuario,
                IdAlmacen = u.IdAlmacen,
                Default = u.Default,
                CodigoAlmacen = u.IdAlmacenNavigation?.CodigoAlmacen,
                DescripcionAlmacen = u.IdAlmacenNavigation?.Descripcion
            };
        }

        public static Login.Models.AlmacenesUsuarios MapFromGridModelToLoginEntity(this AlmacenUsuarioGridModel u)
        {
            Login.Models.AlmacenesUsuarios entity = new Login.Models.AlmacenesUsuarios();
            entity.IdUsuario = u.IdUsuario;
            entity.IdAlmacen = u.IdAlmacen;
            entity.Default = u.Default;
            return entity;
        }

        public static AlmacenUsuarioGridModel MapToGridModel(this AlmacenesUsuarios u)
        {
            return new AlmacenUsuarioGridModel
            {
                IdUsuario = u.IdUsuario,
                IdAlmacen = u.IdAlmacen,
                Default = u.Default,
                CodigoAlmacen = u.IdAlmacenNavigation?.CodigoAlmacen,
                DescripcionAlmacen = u.IdAlmacenNavigation?.Descripcion
            };
        }

        public static AlmacenesUsuarios MapFromGridModelToEntity(this AlmacenUsuarioGridModel u)
        {
            AlmacenesUsuarios entity = new AlmacenesUsuarios();
            entity.IdUsuario = u.IdUsuario;
            entity.IdAlmacen = u.IdAlmacen;
            entity.Default = u.Default;
            return entity;
        }
    }
}
