﻿using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared.Models.Familia;
using Eraltech.SGA.Shared.Models.Producto;
using System.Collections.Generic;

namespace Eraltech.SGA.Services.Mappers
{
    public static class FamiliaMapper
    {
        public static FamiliaFormModel MapToModel(this Familias u)
        {
            return new FamiliaFormModel
            {
                Id = u.Id,
                Descripcion = u.Descripcion
            };
        }

        public static Familias MapToEntity(this FamiliaFormModel u)
        {
            Familias entity = new Familias();
            entity.Id = u.Id;
            entity.Descripcion = u.Descripcion;
            return entity;
        }

        public static FamiliaGridModel MapToGridModel(this Familias u)
        {
            List<ProductoGridModel> productoGridModelList = new List<ProductoGridModel>();
            foreach (var producto in u.Productos)
                productoGridModelList.Add(producto.MapToGridModel());

            return new FamiliaGridModel
            {
                Id = u.Id,
                Descripcion = u.Descripcion,
                Productos = productoGridModelList
            };
        }

        public static Familias MapFromGridModelToEntity(this FamiliaGridModel u)
        {
            Familias entity = new Familias();
            entity.Id = u.Id;
            entity.Descripcion = u.Descripcion;
            return entity;
        }
    }
}
