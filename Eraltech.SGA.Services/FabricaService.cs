﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Fabrica;

namespace Eraltech.SGA.Services
{
    public class FabricaService : IFabricaService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FabricaService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<FabricaGridModel>> GetAllFabricas()
        {
            var fabricaDataCollection = await _unitOfWork.Fabricas.GetAllFabricasAsync();
            var fabricaModelCollection = fabricaDataCollection.Select(f => FabricaMapper.MapToGridModel(f));
            return fabricaModelCollection.ToList();
        }

        public FabricaFormModel GetFabricalById(int id)
        {
            var fabricaData = _unitOfWork.Fabricas.GetFabrica(id);
            var fabricaModel = FabricaMapper.MapToModel(fabricaData);
            return fabricaModel;
        }

        public FabricaGridModel GetFabricaGridModelById(int id)
        {
            var fabricaData = _unitOfWork.Fabricas.GetFabrica(id);
            var fabricaModel = FabricaMapper.MapToGridModel(fabricaData);
            return fabricaModel;
        }

        public ResponseDataModel<FabricaGridModel> CreateFabrica(FabricaFormModel newFabricaModel)
        {
            FabricaGridModel newFabricaGridModel = null;
            try
            { 
                var newFabricaData = FabricaMapper.MapToEntity(newFabricaModel);
                newFabricaGridModel = FabricaMapper.MapToGridModel(newFabricaData);

                _unitOfWork.Fabricas.AddFabrica(newFabricaData);

                newFabricaGridModel = FabricaMapper.MapToGridModel(newFabricaData);
                return new ResponseDataModel<FabricaGridModel>()
                {
                    Ok = true,
                    Object = newFabricaGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<FabricaGridModel>(newFabricaGridModel, ex);
            }
        }

        public ResponseModel UpdateFabrica(FabricaFormModel fabricaModel)
        {
            try
            {
                var fabricaDataToBeUpdated = FabricaMapper.MapToEntity(fabricaModel);
                _unitOfWork.Fabricas.UpdateFabrica(fabricaDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteFabrica(FabricaGridModel fabricaModel)
        {
            try
            {
                var fabricaDataToBeDeleted = FabricaMapper.MapFromGridModelToEntity(fabricaModel);
                _unitOfWork.Fabricas.DeleteFabrica(fabricaModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }
    }
}
