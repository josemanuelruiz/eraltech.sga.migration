﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Movimiento;
using REMO.Almacen.Domain.Factory.DTOs;
using static Eraltech.SGA.Shared.Enums;

namespace Eraltech.SGA.Services
{
    public class MovimientoService : IMovimientoService
    {
        private readonly IUnitOfWork _unitOfWork;
        public Dictionary<string, string> Errors { get; }

        public MovimientoService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
            Errors = new Dictionary<string, string>();
        }

        public async Task<IEnumerable<MovimientoGridModel>> GetAllMovimientos()
        {
            var movimientoDataCollection = await _unitOfWork.Movimientos.GetAllMovimientosAsync();
            var movimientoModelCollection = movimientoDataCollection.Select(m => MovimientoMapper.MapToGridModel(m));
            return movimientoModelCollection.ToList();
        }

        public MovimientoFormModel GetMovimientoFormModelById(int id, SGADbContext dbContext = null)
        {
            Movimientos movimientoData = null;

            if (dbContext != null)
                movimientoData = _unitOfWork.Movimientos.GetMovimientoInTransaction(id, dbContext);
            else
                movimientoData = _unitOfWork.Movimientos.GetMovimiento(id);

            var movimientoModel = MovimientoMapper.MapToModel(movimientoData);
            return movimientoModel;
        }

        public MovimientoGridModel GetMovimientoGridModelById(int id, SGADbContext dbContext = null)
        {
            Movimientos movimientoData = null;

            if (dbContext != null)
                movimientoData = _unitOfWork.Movimientos.GetMovimientoInTransaction(id, dbContext);
            else
                movimientoData = _unitOfWork.Movimientos.GetMovimiento(id);

            MovimientoGridModel movimientoModel = null;
            if (movimientoData != null)
                movimientoModel = MovimientoMapper.MapToGridModel(movimientoData);
            return movimientoModel;
        }

        public ResponseDataModel<MovimientoGridModel> CreateMovimiento(MovimientoFormModel newMovimientoModel, string user)
        {
            MovimientoGridModel newMovimientoGridModel = null;
            try
            { 
                var newMovimientoData = MovimientoMapper.MapToEntity(newMovimientoModel);
                newMovimientoGridModel = MovimientoMapper.MapToGridModel(newMovimientoData);

                _unitOfWork.Movimientos.AddMovimiento(newMovimientoData, user);

                newMovimientoGridModel = MovimientoMapper.MapToGridModel(newMovimientoData);
                return new ResponseDataModel<MovimientoGridModel>()
                {
                    Ok = true,
                    Object = newMovimientoGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<MovimientoGridModel>(newMovimientoGridModel, ex);
            }
        }

        public ResponseDataModel<MovimientosDTO> Create__Migracion_Movimientos_RMO_SGA(MovimientosDTO Movimiento, int idSGA)
        {
            try
            {
                _unitOfWork.Movimientos.Create__Migracion_Movimientos_RMO_SGA(Movimiento, idSGA);
                return new ResponseDataModel<MovimientosDTO>()
                {
                    Ok = true,
                    Object = Movimiento

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<MovimientosDTO>(Movimiento, ex);
            }
        }

        #region Crea un movimiento dentro de una misma transacción

        public List<ResponseDataModel<MovimientoGridModel>> CreateMovimientoOrigenDestino(MovimientoFormModel newMovimientoModelOrigen, MovimientoFormModel newMovimientoModelDestino, string user)
        {
            List<ResponseDataModel<MovimientoGridModel>> responseList = new List<ResponseDataModel<MovimientoGridModel>>();
            bool ok = false;

            using (var context = new SGADbContext())
            {
                using (var transaction = context.Database.BeginTransaction())
                {
                    if (newMovimientoModelOrigen != null)
                    {
                        var responseData = CreateMovimientoInTransaction(newMovimientoModelOrigen, user, context, false);
                        ok = responseData.Ok;
                        responseList.Add(responseData);
                    }

                    if (newMovimientoModelDestino != null)
                    {
                        var responseData = CreateMovimientoInTransaction(newMovimientoModelDestino, user, context, true);
                        ok = ok && responseData.Ok;
                        responseList.Add(responseData);
                    }

                    if (ok)
                        transaction.Commit();
                    else
                        transaction.Rollback();
                }
            }

            return responseList;
        }

        public ResponseDataModel<MovimientoGridModel> CreateMovimientoInTransaction(MovimientoFormModel newMovimientoModel, string user, SGADbContext dbContext, bool crearStock = false)
        {
            MovimientoGridModel newMovimientoGridModel = null;
            try
            {
                SGADbContext context = dbContext;

                var newMovimientoData = MovimientoMapper.MapToEntity(newMovimientoModel);

                if (!crearStock && !ExistStock(newMovimientoData, context))
                    return ServiceExceptionResponses.GetResponseDataFromException<MovimientoGridModel>(newMovimientoGridModel, new Exception(this.Errors[GlobalConstants.NoTieneStock]));

                newMovimientoGridModel = MovimientoMapper.MapToGridModel(newMovimientoData);

                _unitOfWork.Movimientos.AddMovimientoInTransaction(newMovimientoData, user, context);
                UpdateStockWithTransaction(newMovimientoData, context, crearStock);

                newMovimientoGridModel = MovimientoMapper.MapToGridModel(newMovimientoData);                      
                return new ResponseDataModel<MovimientoGridModel>()
                {
                    Ok = true,
                    Object = newMovimientoGridModel
                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<MovimientoGridModel>(newMovimientoGridModel, ex);
            }
        }

        public bool ExistStock(Movimientos movimiento, SGADbContext context)
        {
            if (movimiento.IdDispositivo == null)
                return context.Stocks.Any(s => s.IdAlmacen == movimiento.IdAlmacen
                                            && s.IdUbicacion == movimiento.IdUbicacion
                                            && s.IdProducto == movimiento.IdProducto);
            else
                return context.Stocks.Any(s => s.IdAlmacen == movimiento.IdAlmacen
                                            && s.IdUbicacion == movimiento.IdUbicacion
                                            && s.IdProducto == movimiento.IdProducto
                                            && s.IdDispositivo == movimiento.IdDispositivo);
        }

        private void UpdateStockWithTransaction(Movimientos movimiento, SGADbContext context, bool crearStock = false)
        {
            Stocks stock = null;

            if (movimiento.IdDispositivo == null)
                stock = context.Stocks.FirstOrDefault(s => s.IdAlmacen == movimiento.IdAlmacen
                                                        && s.IdUbicacion == movimiento.IdUbicacion
                                                        && s.IdProducto == movimiento.IdProducto
                                                        && s.IdDispositivo == null);
            else
                stock = context.Stocks.FirstOrDefault(s => s.IdAlmacen == movimiento.IdAlmacen
                                                        && s.IdUbicacion == movimiento.IdUbicacion
                                                        && s.IdProducto == movimiento.IdProducto
                                                        && s.IdDispositivo == movimiento.IdDispositivo);

            if (stock == null && crearStock)
            {
                stock = new Stocks
                {
                    IdAlmacen = movimiento.IdAlmacen,
                    IdUbicacion = movimiento.IdUbicacion,
                    IdProducto = movimiento.IdProducto,
                    IdDispositivo = movimiento.IdDispositivo,
                    DetalleUbicacion = movimiento.DetalleUbicacion,
                    Stock = (double)Math.Abs((decimal)movimiento.Stock)
                };

                SetZona(stock, movimiento);
                context.Stocks.Add(stock);
                context.SaveChanges();
                return;
            }

            if (stock != null)
            {
                if (!string.IsNullOrWhiteSpace(movimiento.DetalleUbicacion))
                    stock.DetalleUbicacion = movimiento.DetalleUbicacion;
                if (stock.Stock + movimiento.Stock < 0)
                    throw new Exception(this.Errors[GlobalConstants.NoTieneStockSuficiente]);
                stock.Stock += movimiento.Stock;
                SetZona(stock, movimiento);
                context.Stocks.Update(stock);
                context.SaveChanges();
            }
        }

        private void SetZona(Stocks stock, Movimientos movimiento)
        {
            switch (movimiento.IdZona)
            {
                case (int)Zona.BuenEstado:
                    if (stock.BuenEstado + movimiento.Stock < 0)
                        throw new Exception(this.Errors[GlobalConstants.NoTieneStockSuficienteBuenEstado]);
                    stock.BuenEstado += movimiento.Stock;
                    break;
                case (int)Zona.MalEstado:
                    if (stock.MalEstado + movimiento.Stock < 0)
                        throw new Exception(this.Errors[GlobalConstants.NoTieneStockSuficienteMalEstado]);
                    stock.MalEstado += movimiento.Stock;
                    break;
                case (int)Zona.PendienteClasificar:
                    if (stock.PendienteClasificar + movimiento.Stock < 0)
                        throw new Exception(this.Errors[GlobalConstants.NoTieneStockSuficientePendienteClasificar]);
                    stock.PendienteClasificar += movimiento.Stock;
                    break;
                case (int)Zona.PendienteRecibir:
                    if (stock.PendienteRecibir + movimiento.Stock < 0)
                        throw new Exception(this.Errors[GlobalConstants.NoTieneStockSuficientePendienteRecibir]);
                    stock.PendienteRecibir += movimiento.Stock;
                    break;
            }
        }

        #endregion

        public ResponseModel UpdateMovimiento(MovimientoFormModel movimientoModel, string user)
        {
            try
            {
                var movimientoDataToBeUpdated = MovimientoMapper.MapToEntity(movimientoModel);
                _unitOfWork.Movimientos.UpdateMovimiento(movimientoDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteMovimiento(MovimientoGridModel movimientoModel)
        {
            try
            {
                var movimientoDataToBeDeleted = MovimientoMapper.MapFromGridModelToEntity(movimientoModel);
                _unitOfWork.Movimientos.DeleteMovimiento(movimientoModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboTipoMovimiento()
        {
            try
            {
                var result = _unitOfWork.Movimientos.GetTipoMovimientos().Select(u => new ComboBoxModel<string>(u.Id.ToString(), u.Descripcion));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboProducto()
        {
            try
            {
                var result = _unitOfWork.Productos.GetAll().Select(u => new ComboBoxModel<string>(u.Id.ToString(), u.Nombre)).OrderBy(o => o.Value);
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboAlmacen(bool addEmpty = false)
        {
            try
            {
                var result = _unitOfWork.Almacenes.GetAll().Select(u => new ComboBoxModel<string>(u.Id.ToString(), u.Descripcion));
                if (addEmpty)
                {
                    List<ComboBoxModel<string>> almacenes = result.ToList();
                    almacenes.Insert(0, new ComboBoxModel<string>(string.Empty, string.Empty));
                    return almacenes;
                }
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboAlmacen(List<string> almacenesConStock)
        {
            try
            {
                var almacenes = _unitOfWork.Almacenes.GetAll().Select(u => new ComboBoxModel<string>(u.Id.ToString(), u.Descripcion)).ToList();
                almacenes = almacenes.Where(a => almacenesConStock.Any(id => id == a.Id)).ToList();
                almacenes.Insert(0, new ComboBoxModel<string>(string.Empty, string.Empty));

                return almacenes;
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboDispositivo(int idProducto)
        {
            try
            {
                var result = _unitOfWork.Dispositivos.GetAll().Where(u => u.IdProducto == idProducto).Select(u => new ComboBoxModel<string>(u.Id.ToString(), u.SerialNumber)).OrderBy(o => o.Value);
                List<ComboBoxModel<string>> dispositivos = result.ToList();
                dispositivos.Insert(0, new ComboBoxModel<string>(string.Empty, string.Empty));
                return dispositivos;
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboZona()
        {
            try
            {
                var result = _unitOfWork.Zonas.GetAll().Select(u => new ComboBoxModel<string>(u.Id.ToString(), u.Descripcion));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboUbicacion(int idAlmacen)
        {
            try
            {
                var result = _unitOfWork.Ubicaciones.GetAll().Where(u => u.IdAlmacen == idAlmacen).Select(u => new ComboBoxModel<string>(u.Id.ToString(), string.Format("{0:00-00-00-0}", int.Parse(u.CodigoUbicacion))));
                List<ComboBoxModel<string>> ubicaciones = result.ToList();
                ubicaciones.Insert(0, new ComboBoxModel<string>(string.Empty, string.Empty));
                return ubicaciones;
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public void AddMovimientoDeProducto(Productos producto, Stocks stock, Zona zona, TipoMovimiento tipoMovimiento, string user, SGADbContext context)
        {
            Movimientos movimiento = new Movimientos();

            movimiento.IdProducto = producto.Id;
            movimiento.IdDispositivo = null;

            AddMovimiento(movimiento, stock, zona, tipoMovimiento, user, context);
        }

        public void AddMovimientoDeDispositivo(Dispositivos dispositivo, Stocks stock, Zona zona, TipoMovimiento tipoMovimiento, string user, SGADbContext context, string detalleUbicacion = null)
        {
            Movimientos movimiento = new Movimientos();

            movimiento.IdProducto = dispositivo.IdProducto;
            movimiento.IdDispositivo = dispositivo.Id;

            AddMovimiento(movimiento, stock, zona, tipoMovimiento, user, context, detalleUbicacion);
        }

        private void AddMovimiento(Movimientos movimiento, Stocks stock, Zona zona, TipoMovimiento tipoMovimiento, string user, SGADbContext context, string detalleUbicacion = null)
        {
            movimiento.IdAlmacen = stock.IdAlmacen;
            movimiento.IdUbicacion = stock.IdUbicacion;
            movimiento.IdZona = (int)zona;
            movimiento.Stock = stock.Stock;
            movimiento.IdTipoMovimiento = (int)tipoMovimiento;
            if (!string.IsNullOrEmpty(stock.DetalleUbicacion))
                movimiento.DetalleUbicacion = stock.DetalleUbicacion;
            else if (!string.IsNullOrEmpty(detalleUbicacion))
                movimiento.DetalleUbicacion = detalleUbicacion;
            movimiento.Automatico = true;

            _unitOfWork.Movimientos.AddMovimientoInTransaction(movimiento, user, context);
        }
    }
}
