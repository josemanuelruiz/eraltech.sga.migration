﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Dispositivo;
using Eraltech.SGA.Shared.Models.Producto;
using static Eraltech.SGA.Shared.Enums;

namespace Eraltech.SGA.Services
{
    public class DispositivoService : IDispositivoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMovimientoService _movimientoService;

        public DispositivoService(IUnitOfWork unitOfWork, MovimientoService movimientoService)
        {
            this._unitOfWork = unitOfWork;
            _movimientoService = movimientoService;
        }

        public async Task<IEnumerable<DispositivoGridModel>> GetAllDispositivos()
        {
            var dispositivoDataCollection = await _unitOfWork.Dispositivos.GetAllDispositivosAsync();
            var dispositivoModelCollection = dispositivoDataCollection.Select(d => DispositivoMapper.MapToGridModel(d));
            return dispositivoModelCollection.ToList();
        }

        public DispositivoFormModel GetDispositivolById(int id)
        {
            var dispositivoData = _unitOfWork.Dispositivos.GetDispositivo(id);
            var dispositivoModel = DispositivoMapper.MapToModel(dispositivoData);
            return dispositivoModel;
        }

        public DispositivoGridModel GetDispositivoGridModelById(int id)
        {
            var dispositivoData = _unitOfWork.Dispositivos.GetDispositivo(id);
            var dispositivoModel = DispositivoMapper.MapToGridModel(dispositivoData);
            return dispositivoModel;
        }

        public Dispositivos GetDispositivolBySerialNumber(string serialNumber)
        {
            var dispositivoData = _unitOfWork.Dispositivos.GetDispositivoBySerialNumber(serialNumber);
            return dispositivoData;
        }

        public ResponseDataModel<DispositivoGridModel> CreateDispositivo(DispositivoFormModel newDispositivoModel, Zona zona, string user, bool identityOnOff = true)
        {
            DispositivoGridModel newDispositivoGridModel = null;
            Dispositivos newDispositivoData = null;

            try
            {
                using (var context = new SGADbContext())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        // Se crea el dispositivo
                        newDispositivoData = DispositivoMapper.MapToEntity(newDispositivoModel);
                        newDispositivoGridModel = DispositivoMapper.MapToGridModel(newDispositivoData);

                        _unitOfWork.Dispositivos.AddDispositivoWithTransaction(newDispositivoData, context);

                        //newDispositivoData.SerialNumber = GetNextSerialNumber(newDispositivoData.Id);
                        //_unitOfWork.Dispositivos.UpdateDispositivoWithTransaction(newDispositivoData, context);

                        // Se crea el stock del nuevo dispositivo
                        newDispositivoModel.Stock.IdProducto = newDispositivoData.IdProducto.ToString();
                        newDispositivoModel.Stock.IdDispositivo = newDispositivoData.Id.ToString();
                        var newStockData = StockMapper.MapToEntity(newDispositivoModel.Stock);
                        _unitOfWork.Stocks.AddStockWithTransaction(newStockData, context, identityOnOff);

                        // Se crea un movimiento de nuevo dispositivo y su stock
                        _movimientoService.AddMovimientoDeDispositivo(newDispositivoData, newStockData, zona, TipoMovimiento.Entrada, user, context);

                        transaction.Commit();
                    }
                }

                newDispositivoGridModel = DispositivoMapper.MapToGridModel(newDispositivoData);
                return new ResponseDataModel<DispositivoGridModel>()
                {
                    Ok = true,
                    Object = newDispositivoGridModel
                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<DispositivoGridModel>(newDispositivoGridModel, ex);
            }
        }

        public ResponseModel UpdateDispositivo(DispositivoFormModel dispositivoModel)
        {
            try
            {
                var dispositivoDataToBeUpdated = DispositivoMapper.MapToEntity(dispositivoModel);
                _unitOfWork.Dispositivos.UpdateDispositivo(dispositivoDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteDispositivo(DispositivoGridModel dispositivoModel)
        {
            try
            {
                var dispositivoDataToBeDeleted = DispositivoMapper.MapFromGridModelToEntity(dispositivoModel);
                _unitOfWork.Dispositivos.DeleteDispositivo(dispositivoModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<ProductoGridModel> GetProductosByName(string pattern)
        {
            try
            {
                var productoData = _unitOfWork.Productos.Find(p => p.Nombre.Contains(pattern));
                var productoModelCollection = productoData.Select(d => ProductoMapper.MapToGridModel(d));
                return productoModelCollection.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ProductoGridModel>();
            }
        }

        /// <summary>
        /// Calcula el siguiente serial number
        /// </summary>
        /// <returns>Devuelve el serial number</returns>
        public string GetNextSerialNumber(int idDispositivo)
        {
            string nextSerialNumber = $"D{idDispositivo.ToString().PadLeft(9, '0')}";

            return nextSerialNumber;
        }

        public IEnumerable<DispositivosNissan> GetDispositivosNissan()
        {
            var dispositivoDataCollection = _unitOfWork.Dispositivos.GetDispositivosNissan();
            return dispositivoDataCollection;
        }
    }
}
