﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Planta;

namespace Eraltech.SGA.Services
{
    public class PlantaService : IPlantaService
    {
        private readonly IUnitOfWork _unitOfWork;

        public PlantaService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<PlantaGridModel>> GetAllPlantas()
        {
            var plantaDataCollection = await _unitOfWork.Plantas.GetAllPlantasAsync();
            var plantaModelCollection = plantaDataCollection.Select(f => PlantaMapper.MapToGridModel(f));
            return plantaModelCollection.ToList();
        }

        public PlantaFormModel GetPlantalById(int id)
        {
            var plantaData = _unitOfWork.Plantas.GetPlanta(id);
            var plantaModel = PlantaMapper.MapToModel(plantaData);
            return plantaModel;
        }

        public PlantaGridModel GetPlantaGridModelById(int id)
        {
            var plantaData = _unitOfWork.Plantas.GetPlanta(id);
            var plantaModel = PlantaMapper.MapToGridModel(plantaData);
            return plantaModel;
        }

        public ResponseDataModel<PlantaGridModel> CreatePlanta(PlantaFormModel newPlantaModel)
        {
            try
            {
                var newPlantaData = PlantaMapper.MapToEntity(newPlantaModel);
                _unitOfWork.Plantas.AddPlanta(newPlantaData);

                var newPlantaGridModel = PlantaMapper.MapToGridModel(newPlantaData);
                return new ResponseDataModel<PlantaGridModel>()
                {
                    Ok = true,
                    Object = newPlantaGridModel
                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex) as ResponseDataModel<PlantaGridModel>;
            }
        }

        public ResponseModel UpdatePlanta(PlantaFormModel plantaModel)
        {
            try
            {
                var plantaDataToBeUpdated = PlantaMapper.MapToEntity(plantaModel);
                _unitOfWork.Plantas.UpdatePlanta(plantaDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeletePlanta(PlantaGridModel plantaModel)
        {
            try
            {
                var plantaDataToBeDeleted = PlantaMapper.MapFromGridModelToEntity(plantaModel);
                _unitOfWork.Plantas.DeletePlanta(plantaModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboFabrica()
        {
            try
            {
                var result = _unitOfWork.Fabricas.GetAll().Select(f => new ComboBoxModel<string>(f.Id.ToString(), f.Descripcion));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }
    }
}
