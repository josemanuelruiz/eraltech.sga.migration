﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eraltech.Common.Logs;
using Eraltech.Login;
using Eraltech.SGA.Core;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Usuario;

namespace Eraltech.SGA.Services
{
    public class UsuarioService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserManagement _userManagement;

        public UsuarioService(IUnitOfWork unitOfWork, IUserManagement userManagement)
        {
            this._unitOfWork = unitOfWork;
            this._userManagement = userManagement;
        }

        public IEnumerable<UsuarioGridModel> GetAllUsuarios()
        {            
            var usuarioDataCollection = _userManagement.CargarUsuarios();
            var usuarioModelCollection = usuarioDataCollection.Select(u => UsuarioMapper.MapToGridModel(u));
            return usuarioModelCollection.ToList();
        }

        public UsuarioFormModel GetUsuariolById(int id)
        {
            var usuarioData = _userManagement.CargarUsuario(id);
            var usuarioModel = UsuarioMapper.MapToModel(usuarioData);
            return usuarioModel;
        }

        public UsuarioGridModel GetUsuarioGridModelById(int id)
        {
            var usuarioData = _userManagement.CargarUsuario(id);
            var usuarioModel = UsuarioMapper.MapToGridModel(usuarioData);
            return usuarioModel;
        }

        public UsuarioFormModel GetUsuariolByLogin(string login)
        {
            var usuarioData = _userManagement.CargarUsuario(login);
            var usuarioModel = UsuarioMapper.MapToModel(usuarioData);
            return usuarioModel;
        }

        public ResponseDataModel<UsuarioGridModel> CreateUsuario(UsuarioFormModel newUsuarioModel)
        {
            UsuarioGridModel newUsuarioGridModel = null;
            try
            {
                var newUsuarioData = UsuarioMapper.MapToEntity(newUsuarioModel);
                newUsuarioGridModel = UsuarioMapper.MapToGridModel(newUsuarioData);

                _userManagement.GuardarUsuario(newUsuarioData);

                newUsuarioGridModel = UsuarioMapper.MapToGridModel(newUsuarioData);
                return new ResponseDataModel<UsuarioGridModel>()
                {
                    Ok = true,
                    Object = newUsuarioGridModel
                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<UsuarioGridModel>(newUsuarioGridModel, ex);
            }
        }

        public ResponseModel UpdateUsuario(UsuarioFormModel usuarioModel)
        {
            try
            {
                var usuarioDataToBeUpdated = UsuarioMapper.MapToEntity(usuarioModel);
                _userManagement.GuardarUsuario(usuarioDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteUsuario(UsuarioGridModel usuarioModel)
        {
            try
            {
                var usuarioDataToBeDeleted = UsuarioMapper.MapFromGridModelToEntity(usuarioModel);
                _userManagement.EliminarUsuario(usuarioModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboRol()
        {
            try
            {
                var result = _userManagement.CargarRoles().Select(p => new ComboBoxModel<string>(p.Id.ToString(), p.Nombre));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboEstado()
        {
            try
            {
                List<ComboBoxModel<string>> comboBoxModels = new List<ComboBoxModel<string>>();
                comboBoxModels.Add(new ComboBoxModel<string>(Login.Models.Usuarios.Estados.Activo, Login.Models.Usuarios.Estados.Activo));
                comboBoxModels.Add(new ComboBoxModel<string>(Login.Models.Usuarios.Estados.Baja, Login.Models.Usuarios.Estados.Baja));
                comboBoxModels.Add(new ComboBoxModel<string>(Login.Models.Usuarios.Estados.Bloqueado, Login.Models.Usuarios.Estados.Bloqueado));
                return comboBoxModels;
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public void ChangePassword(string login, string newPassword)
        {
            _userManagement.CambiarPassword(login, newPassword);
        }
    }
}
