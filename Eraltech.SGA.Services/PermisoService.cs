﻿using System.Collections.Generic;
using System.Linq;
using Eraltech.Login;
using Eraltech.SGA.Core;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared.Models.Permiso;

namespace Eraltech.SGA.Services
{
    public class PermisoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUserManagement _userManagement;

        public PermisoService(IUnitOfWork unitOfWork, IUserManagement userManagement)
        {
            this._unitOfWork = unitOfWork;
            this._userManagement = userManagement;
        }

        public IEnumerable<PermisoGridModel> GetAllPermisos()
        {            
            var rolDataCollection = _userManagement.CargarPermisos();
            var rolModelCollection = rolDataCollection.Select(u => PermisoMapper.MapToGridModel(u));
            return rolModelCollection.ToList();
        }
    }
}
