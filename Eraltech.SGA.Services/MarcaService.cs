﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Marca;

namespace Eraltech.SGA.Services
{
    public class MarcaService : IMarcaService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MarcaService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<MarcaGridModel> GetAllMarcas()
        {
            var marcaDataCollection = _unitOfWork.Marcas.GetAllMarcas();
            var marcaModelCollection = marcaDataCollection.Select(f => MarcaMapper.MapToGridModel(f));
            return marcaModelCollection.ToList();
        }

        public MarcaFormModel GetMarcalById(int id)
        {
            var marcaData = _unitOfWork.Marcas.GetMarca(id);
            var marcaModel = MarcaMapper.MapToModel(marcaData);
            return marcaModel;
        }

        public MarcaGridModel GetMarcaGridModelById(int id)
        {
            var marcaData = _unitOfWork.Marcas.GetMarca(id);
            var marcaModel = MarcaMapper.MapToGridModel(marcaData);
            return marcaModel;
        }

        public ResponseDataModel<MarcaGridModel> CreateMarca(MarcaFormModel newMarcaModel, bool identityOnOff = true)
        {
            MarcaGridModel newMarcaGridModel = null;
            try
            { 
                var newMarcaData = MarcaMapper.MapToEntity(newMarcaModel);
                newMarcaGridModel = MarcaMapper.MapToGridModel(newMarcaData);

                _unitOfWork.Marcas.AddMarca(newMarcaData, identityOnOff);

                newMarcaGridModel = MarcaMapper.MapToGridModel(newMarcaData);
                return new ResponseDataModel<MarcaGridModel>()
                {
                    Ok = true,
                    Object = newMarcaGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<MarcaGridModel>(newMarcaGridModel, ex);
            }
        }

        public ResponseModel UpdateMarca(MarcaFormModel marcaModel)
        {
            try
            {
                var marcaDataToBeUpdated = MarcaMapper.MapToEntity(marcaModel);
                _unitOfWork.Marcas.UpdateMarca(marcaDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteMarca(MarcaGridModel marcaModel)
        {
            try
            {
                var marcaDataToBeDeleted = MarcaMapper.MapFromGridModelToEntity(marcaModel);
                _unitOfWork.Marcas.DeleteMarca(marcaModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }
    }
}
