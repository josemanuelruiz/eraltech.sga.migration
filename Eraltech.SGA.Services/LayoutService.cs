﻿using Eraltech.Common.Logs;
//using Eraltech.QAIA.Common.BR;
//using Eraltech.QAIA.Common.DataAccess;
using Eraltech.SGA.Shared;
using Eraltech.SGA.State;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Configuration;
using System;

namespace Eraltech.SGA.Services
{
    [Authorize]
    public class LayoutService
    {
        private readonly IConfiguration Configuration;
        private readonly AppState AppState;

        public LayoutService(IConfiguration configuration, AppState appState)
        {
            this.Configuration = configuration;
            this.AppState = appState;
        }

        public ConfigurationModel Get()
        {
            var model = new ConfigurationModel();

            try
            {
                var permis = new Permissions(AppState.CurrentUser);
                int valor = (permis.Fabrica != PermissionEnum.Full) ? 1 : 7;
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);
            }

            return model;
        }
    }
}
