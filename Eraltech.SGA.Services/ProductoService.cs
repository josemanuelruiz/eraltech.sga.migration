﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Producto;
using static Eraltech.SGA.Shared.Enums;

namespace Eraltech.SGA.Services
{
    public class ProductoService : IProductoService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMovimientoService _movimientoService;

        public ProductoService(IUnitOfWork unitOfWork, MovimientoService movimientoService)
        {
            this._unitOfWork = unitOfWork;
            _movimientoService = movimientoService;
        }

        public IEnumerable<ProductoGridModel> GetAllProductos()
        {
            var productoDataCollection = _unitOfWork.Productos.GetAllProductos();
            var productoModelCollection = productoDataCollection.Select(f => ProductoMapper.MapToGridModel(f));
            return productoModelCollection.ToList();
        }

        public ProductoFormModel GetProductolById(int id)
        {
            var productoData = _unitOfWork.Productos.GetProducto(id);
            var productoModel = ProductoMapper.MapToModel(productoData);
            return productoModel;
        }

        public ProductoGridModel GetProductoGridModelById(int id)
        {
            var productoData = _unitOfWork.Productos.GetProducto(id);
            var productoModel = ProductoMapper.MapToGridModel(productoData);
            return productoModel;
        }

        public ProductoFormModel GetProductoByCodigoProducto(string codigoProducto)
        {
            var productoData = _unitOfWork.Productos.GetProductoByCodigoProducto(codigoProducto);
            if (productoData != null)
            {
                var productoModel = ProductoMapper.MapToModel(productoData);
                return productoModel;
            }
            else
                return null;
        }

        public ProductoFormModel GetProductoByNombre(string nombre)
        {
            var productoData = _unitOfWork.Productos.GetProductoByNombre(nombre);
            if (productoData != null)
            {
                var productoModel = ProductoMapper.MapToModel(productoData);
                return productoModel;
            }
            else
                return null;
        }

        public ResponseDataModel<ProductoGridModel> CreateProducto(ProductoFormModel newProductoModel, Zona zona, string user, bool identityOnOff = false)
        {
            ProductoGridModel newProductoGridModel = null;
            Productos newProductoData = null;

            try
            {
                using (var context = new SGADbContext())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        // Se crea el producto
                        newProductoData = ProductoMapper.MapToEntity(newProductoModel);
                        newProductoGridModel = ProductoMapper.MapToGridModel(newProductoData);

                        _unitOfWork.Productos.AddProductoWithTransaction(newProductoData, context, identityOnOff);

                        newProductoData.CodigoProducto = GetNextCodigoProducto(newProductoData.Id);
                        //string newFileName = SetProductoImage(newProductoData.ImageUrl, newProductoData.CodigoProducto);
                        //newProductoData.ImageUrl = newFileName;
                        _unitOfWork.Productos.UpdateProductoWithTransaction(newProductoData, context);

                        // Se crea el stock del nuevo producto
                        newProductoModel.Stock.IdProducto = newProductoData.Id.ToString();
                        var newStockData = StockMapper.MapToEntity(newProductoModel.Stock);
                        _unitOfWork.Stocks.AddStockWithTransaction(newStockData, context, identityOnOff);

                        //Se crea un movimiento de nuevo producto
                        _movimientoService.AddMovimientoDeProducto(newProductoData, newStockData, zona, TipoMovimiento.Entrada, user, context);

                        transaction.Commit();
                    }
                }

                newProductoGridModel = ProductoMapper.MapToGridModel(newProductoData);
                return new ResponseDataModel<ProductoGridModel>()
                {
                    Ok = true,
                    Object = newProductoGridModel
                };

            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<ProductoGridModel>(newProductoGridModel, ex);
            }
        }

        public ResponseDataModel<ProductoGridModel> CreateProductoMigracion(ProductoFormModel newProductoModel, Zona zona, string user)
        {
            ProductoGridModel newProductoGridModel = null;
            Productos newProductoData = null;

            try
            {
                using (var context = new SGADbContext())
                {
                    using (var transaction = context.Database.BeginTransaction())
                    {
                        // Se crea el producto
                        newProductoData = ProductoMapper.MapToEntity(newProductoModel);
                        newProductoGridModel = ProductoMapper.MapToGridModel(newProductoData);

                        _unitOfWork.Productos.AddProductoWithTransaction(newProductoData, context);

                        _unitOfWork.Productos.UpdateProductoWithTransaction(newProductoData, context);

                        // Se crea el stock del nuevo producto
                        newProductoModel.Stock.IdProducto = newProductoData.Id.ToString();
                        var newStockData = StockMapper.MapToEntity(newProductoModel.Stock);
                        _unitOfWork.Stocks.AddStockWithTransaction(newStockData, context);

                        // Se crea un movimiento de nuevo producto.
                        // En la migración de recambios almacén no hay que crar ningún movimiento, Se migran los movimientos existentes en REMO
                        //_movimientoService.AddMovimientoDeProducto(newProductoData, newStockData, zona, TipoMovimiento.Entrada, user, context);

                        transaction.Commit();
                    }
                }

                newProductoGridModel = ProductoMapper.MapToGridModel(newProductoData);
                return new ResponseDataModel<ProductoGridModel>()
                {
                    Ok = true,
                    Object = newProductoGridModel
                };

            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<ProductoGridModel>(newProductoGridModel, ex);
            }
        }

        public string SetProductoImage(string fileName, string codigoProducto)
        {
            string fileExtension = "";

            int positionPunto = fileName.IndexOf('.');
            if (positionPunto >= 0)
            {
                fileExtension = fileName.Substring(positionPunto);
            }
            return (codigoProducto + fileExtension);
        }

        public ResponseModel UpdateProducto(ProductoFormModel productoModel)
        {
            try
            {
                var productoDataToBeUpdated = ProductoMapper.MapToEntity(productoModel);
                _unitOfWork.Productos.UpdateProducto(productoDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteProducto(ProductoGridModel productoModel)
        {
            try
            {
                var productoDataToBeDeleted = ProductoMapper.MapFromGridModelToEntity(productoModel);
                _unitOfWork.Productos.DeleteProducto(productoModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboEspecialidad()
        {
            try
            {
                var result = _unitOfWork.Especialidades.GetAll().Select(e => new ComboBoxModel<string>(e.Id.ToString(), e.Descripcion));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboFamilia()
        {
            try
            {
                var result = _unitOfWork.Familias.GetAll().Select(f => new ComboBoxModel<string>(f.Id.ToString(), f.Descripcion));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboMarca()
        {
            try
            {
                var result = _unitOfWork.Marcas.GetAll().Select(m => new ComboBoxModel<string>(m.Id.ToString(), m.Descripcion));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        /// <summary>
        /// Calcula el siguiente código de producto
        /// </summary>
        /// <returns>Devuelve el código de producto</returns>
        public string GetNextCodigoProducto(int idProducto)
        {
            string nextCodigoProducto = $"P{idProducto.ToString().PadLeft(9, '0')}";

            return nextCodigoProducto;
        }
    }
}
