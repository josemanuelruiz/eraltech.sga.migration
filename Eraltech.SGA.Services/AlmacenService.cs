﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Almacen;

namespace Eraltech.SGA.Services
{
    public class AlmacenService : IAlmacenService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AlmacenService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<AlmacenGridModel>> GetAllAlmacenesAsync()
        {
            var almacenDataCollection = await _unitOfWork.Almacenes.GetAllAlmacenesAsync();
            var almacenModelCollection = almacenDataCollection.Select(f => AlmacenMapper.MapToGridModel(f));
            return almacenModelCollection.ToList();
        }

        public IEnumerable<AlmacenGridModel> GetAllAlmacenes()
        {
            var almacenDataCollection = _unitOfWork.Almacenes.GetAllAlmacenes();
            var almacenModelCollection = almacenDataCollection.Select(f => AlmacenMapper.MapToGridModel(f));
            return almacenModelCollection.ToList();
        }

        public AlmacenFormModel GetAlmacenlById(int id)
        {
            var almacenData = _unitOfWork.Almacenes.GetAlmacen(id);
            var almacenModel = AlmacenMapper.MapToModel(almacenData);
            return almacenModel;
        }

        public AlmacenGridModel GetAlmacenGridModelById(int id)
        {
            var almacenData = _unitOfWork.Almacenes.GetAlmacen(id);
            var almacenModel = AlmacenMapper.MapToGridModel(almacenData);
            return almacenModel;
        }

        public Almacenes GetAlmacenlByCodigoAlmacen(string codigoAlmacen)
        {
            var almacenData = _unitOfWork.Almacenes.GetAlmacenlByCodigoAlmacen(codigoAlmacen);
            return almacenData;
        }

        public ResponseDataModel<AlmacenGridModel> CreateAlmacen(AlmacenFormModel newAlmacenModel)
        {
            AlmacenGridModel newAlmacenGridModel = null;
            try
            { 
                var newAlmacenData = AlmacenMapper.MapToEntity(newAlmacenModel);
                newAlmacenGridModel = AlmacenMapper.MapToGridModel(newAlmacenData);

                _unitOfWork.Almacenes.AddAlmacen(newAlmacenData);

                newAlmacenGridModel = AlmacenMapper.MapToGridModel(newAlmacenData);
                return new ResponseDataModel<AlmacenGridModel>()
                {
                    Ok = true,
                    Object = newAlmacenGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<AlmacenGridModel>(newAlmacenGridModel, ex);
            }
        }

        public ResponseModel UpdateAlmacen(AlmacenFormModel almacenModel)
        {
            try
            {
                var almacenDataToBeUpdated = AlmacenMapper.MapToEntity(almacenModel);
                _unitOfWork.Almacenes.UpdateAlmacen(almacenDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteAlmacen(AlmacenGridModel almacenModel)
        {
            try
            {
                var almacenDataToBeDeleted = AlmacenMapper.MapFromGridModelToEntity(almacenModel);
                _unitOfWork.Almacenes.DeleteAlmacen(almacenModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboPlanta()
        {
        try
            {
                var result = _unitOfWork.Plantas.GetAll().Select(p => new ComboBoxModel<string>(p.Id.ToString(), p.Descripcion));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }
    }
}
