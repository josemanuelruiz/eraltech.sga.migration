﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.MovementTemplate;
using static Eraltech.SGA.Shared.Enums;

namespace Eraltech.SGA.Services
{
    public class MovementTemplateService : IMovementTemplateService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MovementTemplateService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<MovementTemplateGridModel>> GetAllMovementTemplates(string user)
        {
            var movementTemplateDataCollection = await _unitOfWork.MovementTemplates.GetAllMovementTemplatesAsync(user);
            var movementTemplateModelCollection = movementTemplateDataCollection.Select(m => MovementTemplateMapper.MapToGridModel(m));
            return movementTemplateModelCollection.ToList();
        }

        public MovementTemplateGridModel GetMovementTemplateGridModelById(int id)
        {
            MovementTemplates movementTemplateData = _unitOfWork.MovementTemplates.GetMovementTemplate(id);

            var movementTemplateModel = MovementTemplateMapper.MapToGridModel(movementTemplateData);
            return movementTemplateModel;
        }

        public ResponseDataModel<MovementTemplateGridModel> CreateMovementTemplate(MovementTemplateFormModel newMovementTemplateModel, string user)
        {
            MovementTemplateGridModel newMovementTemplateGridModel = null;
            try
            { 
                var newMovementTemplateData = MovementTemplateMapper.MapToEntity(newMovementTemplateModel);
                newMovementTemplateGridModel = MovementTemplateMapper.MapToGridModel(newMovementTemplateData);

                _unitOfWork.MovementTemplates.AddMovementTemplate(newMovementTemplateData, user);

                newMovementTemplateGridModel = MovementTemplateMapper.MapToGridModel(newMovementTemplateData);
                return new ResponseDataModel<MovementTemplateGridModel>()
                {
                    Ok = true,
                    Object = newMovementTemplateGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<MovementTemplateGridModel>(newMovementTemplateGridModel, ex);
            }
        }

        public ResponseModel UpdateMovementTemplate(MovementTemplateFormModel movementTemplateModel, string user)
        {
            try
            {
                var movementTemplateDataToBeUpdated = MovementTemplateMapper.MapToEntity(movementTemplateModel);
                _unitOfWork.MovementTemplates.UpdateMovementTemplate(movementTemplateDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteMovementTemplate(MovementTemplateGridModel movementTemplateModel)
        {
            try
            {
                var movementTemplateDataToBeDeleted = MovementTemplateMapper.MapFromGridModelToEntity(movementTemplateModel);
                _unitOfWork.MovementTemplates.DeleteMovementTemplate(movementTemplateModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }
    }
}
