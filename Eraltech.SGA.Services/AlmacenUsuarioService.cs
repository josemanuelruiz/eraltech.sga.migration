﻿using System.Collections.Generic;
using System.Linq;
using Eraltech.SGA.Core;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared.Models.AlmacenUsuario;

namespace Eraltech.SGA.Services
{
    public class AlmacenUsuarioService
    {
        private readonly IUnitOfWork _unitOfWork;

        public AlmacenUsuarioService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<AlmacenUsuarioGridModel> GetAllAlmacenUsuarios()
        {            
            var rolDataCollection = _unitOfWork.AlmacenesUsuarios.GetAllAlmacenUsuarios();
            var rolModelCollection = rolDataCollection.Select(u => AlmacenUsuarioMapper.MapToGridModel(u));
            return rolModelCollection.ToList();
        }
    }
}
