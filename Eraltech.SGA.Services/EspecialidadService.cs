﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Especialidad;

namespace Eraltech.SGA.Services
{
    public class EspecialidadService : IEspecialidadService
    {
        private readonly IUnitOfWork _unitOfWork;

        public EspecialidadService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<EspecialidadGridModel> GetAllEspecialidades()
        {
            var especialidadDataCollection = _unitOfWork.Especialidades.GetAllEspecialidades();
            var especialidadModelCollection = especialidadDataCollection.Select(f => EspecialidadMapper.MapToGridModel(f));
            return especialidadModelCollection.ToList();
        }

        public EspecialidadFormModel GetEspecialidadlById(int id)
        {
            var especialidadData = _unitOfWork.Especialidades.GetEspecialidad(id);
            var especialidadModel = EspecialidadMapper.MapToModel(especialidadData);
            return especialidadModel;
        }

        public EspecialidadGridModel GetEspecialidadGridModelById(int id)
        {
            var especialidadData = _unitOfWork.Especialidades.GetEspecialidad(id);
            var especialidadModel = EspecialidadMapper.MapToGridModel(especialidadData);
            return especialidadModel;
        }

        public ResponseDataModel<EspecialidadGridModel> CreateEspecialidad(EspecialidadFormModel newEspecialidadModel, bool identityOnOff = true)
        {
            EspecialidadGridModel newEspecialidadGridModel = null;
            try
            { 
                var newEspecialidadData = EspecialidadMapper.MapToEntity(newEspecialidadModel);
                newEspecialidadGridModel = EspecialidadMapper.MapToGridModel(newEspecialidadData);

                _unitOfWork.Especialidades.AddEspecialidad(newEspecialidadData, identityOnOff);

                newEspecialidadGridModel = EspecialidadMapper.MapToGridModel(newEspecialidadData);
                return new ResponseDataModel<EspecialidadGridModel>()
                {
                    Ok = true,
                    Object = newEspecialidadGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<EspecialidadGridModel>(newEspecialidadGridModel, ex);
            }
        }

        public ResponseModel UpdateEspecialidad(EspecialidadFormModel especialidadModel)
        {
            try
            {
                var especialidadDataToBeUpdated = EspecialidadMapper.MapToEntity(especialidadModel);
                _unitOfWork.Especialidades.UpdateEspecialidad(especialidadDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteEspecialidad(EspecialidadGridModel especialidadModel)
        {
            try
            {
                var especialidadDataToBeDeleted = EspecialidadMapper.MapFromGridModelToEntity(especialidadModel);
                _unitOfWork.Especialidades.DeleteEspecialidad(especialidadModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }
    }
}
