﻿using System;
using System.Collections.Generic;
using System.Linq;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Ubicacion;

namespace Eraltech.SGA.Services
{
    public class UbicacionService : IUbicacionService
    {
        private readonly IUnitOfWork _unitOfWork;

        public UbicacionService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<UbicacionGridModel> GetAllUbicaciones()
        {
            var ubicacionDataCollection = _unitOfWork.Ubicaciones.GetAllUbicaciones();
            var ubicacionModelCollection = ubicacionDataCollection.Select(f => UbicacionMapper.MapToGridModel(f));
            return ubicacionModelCollection.ToList();
        }

        public UbicacionFormModel GetUbicacionlById(int id)
        {
            var ubicacionData = _unitOfWork.Ubicaciones.GetUbicacion(id);
            var ubicacionModel = UbicacionMapper.MapToModel(ubicacionData);
            return ubicacionModel;
        }

        public UbicacionFormModel GetUbicacionlByCodigoUbicacion(string codigoUbicacion)
        {
            var ubicacionData = _unitOfWork.Ubicaciones.GetUbicacionByCodigoUbicacion(codigoUbicacion);
            if (ubicacionData != null)
            {
                var ubicacionModel = UbicacionMapper.MapToModel(ubicacionData);
                return ubicacionModel;
            }
            return null;
        }

        public UbicacionGridModel GetUbicacionGridModelById(int id)
        {
            var ubicacionData = _unitOfWork.Ubicaciones.GetUbicacion(id);
            var ubicacionModel = UbicacionMapper.MapToGridModel(ubicacionData);
            return ubicacionModel;
        }

        public ResponseDataModel<UbicacionGridModel> CreateUbicacion(UbicacionFormModel newUbicacionModel, bool identityOnOff = true)
        {
            UbicacionGridModel newUbicacionGridModel = null;
            try
            { 
                var newUbicacionData = UbicacionMapper.MapToEntity(newUbicacionModel);
                newUbicacionGridModel = UbicacionMapper.MapToGridModel(newUbicacionData);

                _unitOfWork.Ubicaciones.AddUbicacion(newUbicacionData, identityOnOff);

                newUbicacionGridModel = UbicacionMapper.MapToGridModel(newUbicacionData);
                return new ResponseDataModel<UbicacionGridModel>()
                {
                    Ok = true,
                    Object = newUbicacionGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<UbicacionGridModel>(newUbicacionGridModel, ex);
            }
        }

        public ResponseModel UpdateUbicacion(UbicacionFormModel ubicacionModel)
        {
            try
            {
                var ubicacionDataToBeUpdated = UbicacionMapper.MapToEntity(ubicacionModel);
                _unitOfWork.Ubicaciones.UpdateUbicacion(ubicacionDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteUbicacion(UbicacionGridModel ubicacionModel)
        {
            try
            {
                var ubicacionDataToBeDeleted = UbicacionMapper.MapFromGridModelToEntity(ubicacionModel);
                _unitOfWork.Ubicaciones.DeleteUbicacion(ubicacionModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboAlmacen()
        {
            try
            {
                var result = _unitOfWork.Almacenes.GetAll().Select(a => new ComboBoxModel<string>(a.Id.ToString(), a.Descripcion, a.UbicacionEstandar));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public ResponseModel UpdateCodigoUbicacion()
        {
            try
            {
                _unitOfWork.Ubicaciones.UpdateCodigoUbicacion();
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
               return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }
    }
}
