﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.Common.Logs;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Stock;

namespace Eraltech.SGA.Services
{
    public class StockService : IStockService
    {
        private readonly IUnitOfWork _unitOfWork;

        public StockService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public async Task<IEnumerable<StockGridModel>> GetAllStocks()
        {
            var stockDataCollection = await _unitOfWork.Stocks.GetAllStocksAsync();
            var stockModelCollection = stockDataCollection.Select(f => StockMapper.MapToGridModel(f));
            return stockModelCollection.ToList();
        }

        public StockFormModel GetStocklById(int id)
        {
            var stockData = _unitOfWork.Stocks.GetStock(id);
            var stockModel = StockMapper.MapToModel(stockData);
            return stockModel;
        }

        public StockGridModel GetStockGridModelById(int id)
        {
            var stockData = _unitOfWork.Stocks.GetStock(id);
            var stockModel = StockMapper.MapToGridModel(stockData);
            return stockModel;
        }

        public ResponseDataModel<StockGridModel> CreateStock(StockFormModel newStockModel)
        {
            StockGridModel newStockGridModel = null;
            try
            { 
                var newStockData = StockMapper.MapToEntity(newStockModel);
                newStockGridModel = StockMapper.MapToGridModel(newStockData);

                _unitOfWork.Stocks.AddStock(newStockData);

                newStockGridModel = StockMapper.MapToGridModel(newStockData);
                return new ResponseDataModel<StockGridModel>()
                {
                    Ok = true,
                    Object = newStockGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<StockGridModel>(newStockGridModel, ex);
            }
        }

        public ResponseModel UpdateStock(StockFormModel stockModel)
        {
            try
            {
                var stockDataToBeUpdated = StockMapper.MapToEntity(stockModel);
                _unitOfWork.Stocks.UpdateStock(stockDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteStock(StockGridModel stockModel)
        {
            try
            {
                var stockDataToBeDeleted = StockMapper.MapFromGridModelToEntity(stockModel);
                _unitOfWork.Stocks.DeleteStock(stockModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public IEnumerable<StockGridModel> GetProductoStocks(int idProducto)
        {
            var stockDataCollection = _unitOfWork.Stocks.GetStockOfProduct(idProducto);
            var stockModelCollection = stockDataCollection.Select(f => StockMapper.MapToGridModel(f));
            return stockModelCollection.ToList();
        }

        public IEnumerable<StockGridModel> GetDispositivoStocks(int idDispositivo)
        {
            var stockDataCollection = _unitOfWork.Stocks.GetStockOfDevice(idDispositivo);
            var stockModelCollection = stockDataCollection.Select(f => StockMapper.MapToGridModel(f));
            return stockModelCollection.ToList();
        }

        public IEnumerable<ComboBoxModel<string>> GetComboAlmacen()
        {
            try
            {
                var result = _unitOfWork.Almacenes.GetAll().Select(u => new ComboBoxModel<string>(u.Id.ToString(), u.Descripcion));
                return result.ToList();
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }

        public IEnumerable<ComboBoxModel<string>> GetComboUbicacion(int idAlmacen)
        {
            try
            {
                var result = _unitOfWork.Ubicaciones.GetAll().Where(u => u.IdAlmacen == idAlmacen).Select(u => new ComboBoxModel<string>(u.Id.ToString(), string.Format("{0:00-00-00-0}", int.Parse(u.CodigoUbicacion))));
                List<ComboBoxModel<string>> ubicaciones = result.ToList();
                ubicaciones.Insert(0, new ComboBoxModel<string>(string.Empty, string.Empty));
                return ubicaciones;
            }
            catch (Exception ex)
            {
                sLogger.WriteError(ex);

                return new List<ComboBoxModel<string>>();
            }
        }
    }
}
