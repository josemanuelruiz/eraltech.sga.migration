﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Services.Common;
using Eraltech.SGA.Services.Mappers;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Familia;

namespace Eraltech.SGA.Services
{
    public class FamiliaService : IFamiliaService
    {
        private readonly IUnitOfWork _unitOfWork;

        public FamiliaService(IUnitOfWork unitOfWork)
        {
            this._unitOfWork = unitOfWork;
        }

        public IEnumerable<FamiliaGridModel> GetAllFamilias()
        {
            var familiaDataCollection = _unitOfWork.Familias.GetAllFamilias();
            var familiaModelCollection = familiaDataCollection.Select(f => FamiliaMapper.MapToGridModel(f));
            return familiaModelCollection.ToList();
        }

        public FamiliaFormModel GetFamilialById(int id)
        {
            var familiaData = _unitOfWork.Familias.GetFamilia(id);
            var familiaModel = FamiliaMapper.MapToModel(familiaData);
            return familiaModel;
        }

        public FamiliaGridModel GetFamiliaGridModelById(int id)
        {
            var familiaData = _unitOfWork.Familias.GetFamilia(id);
            var familiaModel = FamiliaMapper.MapToGridModel(familiaData);
            return familiaModel;
        }

        public ResponseDataModel<FamiliaGridModel> CreateFamilia(FamiliaFormModel newFamiliaModel, bool identityOnOff = true)
        {
            FamiliaGridModel newFamiliaGridModel = null;
            try
            { 
                var newFamiliaData = FamiliaMapper.MapToEntity(newFamiliaModel);
                newFamiliaGridModel = FamiliaMapper.MapToGridModel(newFamiliaData);

                _unitOfWork.Familias.AddFamilia(newFamiliaData, identityOnOff);

                newFamiliaGridModel = FamiliaMapper.MapToGridModel(newFamiliaData);
                return new ResponseDataModel<FamiliaGridModel>()
                {
                    Ok = true,
                    Object = newFamiliaGridModel

                };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseDataFromException<FamiliaGridModel>(newFamiliaGridModel, ex);
            }
        }

        public ResponseModel UpdateFamilia(FamiliaFormModel familiaModel)
        {
            try
            {
                var familiaDataToBeUpdated = FamiliaMapper.MapToEntity(familiaModel);
                _unitOfWork.Familias.UpdateFamilia(familiaDataToBeUpdated);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }

        public ResponseModel DeleteFamilia(FamiliaGridModel familiaModel)
        {
            try
            {
                var familiaDataToBeDeleted = FamiliaMapper.MapFromGridModelToEntity(familiaModel);
                _unitOfWork.Familias.DeleteFamilia(familiaModel.Id);
                return new ResponseModel() { Ok = true };
            }
            catch (Exception ex)
            {
                return ServiceExceptionResponses.GetResponseFromException(ex);
            }
        }
    }
}
