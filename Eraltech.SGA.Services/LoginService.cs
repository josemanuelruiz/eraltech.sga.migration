﻿using Eraltech.Login;
using Eraltech.Login.Models;
using Eraltech.SGA.Shared;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace Eraltech.SGA.Services
{
    public class LoginService
    {
        private readonly IConfiguration _configuration;
        private readonly IUserAuthentication _userAuthentication;
        public LoginService(IConfiguration configuration, IUserAuthentication userAuthentication)
        {
            _configuration = configuration;
            _userAuthentication = userAuthentication;
        }

        public LoginResult Login(LoginModel login)
        {
            try
            {
                Usuarios result = _userAuthentication.ValidarUsuario(login.Username, login.Password);

                if (result == null) return new LoginResult { Successful = false, Error = "Credenciales no válidas" };

                var claims = new[] {
                    new Claim(ClaimTypes.Name, result.Login)
                };

                var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSecurityKey"]));
                var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
                var expiry = DateTime.Now.AddDays(Convert.ToInt32(_configuration["JwtExpiryInDays"]));

                var token = new JwtSecurityToken(
                    _configuration["JwtIssuer"],
                    _configuration["JwtAudience"],
                    claims,
                    expires: expiry,
                    signingCredentials: creds
                );

                return new LoginResult { Successful = true, Expiry = expiry, Token = new JwtSecurityTokenHandler().WriteToken(token) };
            }
            catch (Exception ex)
            {
                string message;
                if (ex.InnerException != null)
                    message = ex.InnerException.Message;
                else
                    message = ex.Message;

                return new LoginResult { Successful = false, Error = message };
            }
        }
    }
}