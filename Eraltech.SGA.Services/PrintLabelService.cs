﻿using System;
using System.IO;
using System.Collections.Generic;
using Eraltech.SGA.Core.Services;
using Eraltech.SGA.Shared.Models.Producto;
using Eraltech.SGA.Services.Common;
using Microsoft.Extensions.Configuration;
using Eraltech.SGA.Shared.Models.Dispositivo;
using Syncfusion.Pdf;
using Syncfusion.Pdf.Graphics;

namespace Eraltech.SGA.Services
{
    public class PrintLabelService : IPrintLabelService
    {
        protected readonly IConfiguration Configuration;
        protected readonly IBarcodeGenerator BarcodeGenerator;
        protected readonly IFileUtil FileUtil;

        public PrintLabelService(IConfiguration configuration, IBarcodeGenerator barcodeGenerator, IFileUtil fileUtil)
        {
            Configuration = configuration;
            BarcodeGenerator = barcodeGenerator;
            FileUtil = fileUtil;
        }

        public MemoryStream ConvertLabelsToPDF<T>(IEnumerable<T> modelList)
        {
            var imageFullPath = "";
            try
            {
                FileUtil.DeleteTemporalFiles();

                List<string> barcodes = new List<string>();

                foreach(T model in modelList)
                {
                    if (typeof(T).Name == typeof(IProductoModel).Name)
                        imageFullPath = BarcodeGenerator.BarcodeToImage(model as IProductoModel);
                    if (typeof(T).Name == typeof(DispositivoFormModel).Name)
                        imageFullPath = BarcodeGenerator.BarcodeToImage(model as DispositivoFormModel);

                    barcodes.Add(imageFullPath);
                }

                //Creación PDF tantas veces como se quiera
                string barcodeImageFileOutputPath = Configuration.GetValue<string>("FilesOutputPath");

                MemoryStream memoryStream = ConvertLabelsToPDF(barcodes, barcodeImageFileOutputPath);

                return memoryStream;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}", ex.Message));
            }
        }

        private MemoryStream ConvertLabelsToPDF(List<string> labels, string labelpath)
        {
            //Se crea un nuevo documento PDF
            PdfDocument document = new PdfDocument();
            PdfPage page = null;
            PdfGraphics graphics = null;

            int count = 0;
            int x = 0;
            int y = 0;
            int higth = 150;
            foreach (var label in labels)
            {
                if (count % 10 == 0)
                {
                    //Se añade una pagina al documento
                    page = document.Pages.Add();
                    //Se crea un grafico pdf para la página
                    graphics = page.Graphics;
                    // Se inicializan las coordenadas x, y
                    x = 0;
                    y = 0;
                }

                if (label.Length > 0)
                {
                    if ((count % 2) == 0)
                    {
                        // Etiqueta a la izquierda
                        x = 50;
                        if ((count % 10) >= 2)
                            // Etiqueta en nueva fila
                            y += higth;
                    }
                    else
                    {
                        // Etiqueta a la derecha
                        x = 260;
                    }

                    // Se pinta la imagen de la etiqueta en la página del PDF
                    FileStream imageStream = new FileStream(label, FileMode.Open, FileAccess.Read);
                    PdfImage image = PdfImage.FromStream(imageStream);
                    graphics.DrawImage(image, x, y);
                    count++;
                }
            }

            // Se guarda el PDF en MemoryStream
            MemoryStream stream = new MemoryStream();

            document.Save(stream);

            return stream;
        }
    }
}
