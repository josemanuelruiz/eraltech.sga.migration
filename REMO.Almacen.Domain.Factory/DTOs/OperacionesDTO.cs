﻿using System;
using REMO.Almacen.Infrastructure.Data.EntityModel;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class OperacionesDTO
    {
        public OperacionesDTO(zSYS_M_Operaciones entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            Operacion = entity.Operacion;
            DeletedReg = entity.DeletedReg;
        }

        public OperacionesDTO()
        {
        }

        public int Id { get; set; }
        public string Operacion { get; set; }
        public bool DeletedReg { get; set; }

    }

}

