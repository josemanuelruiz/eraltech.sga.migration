﻿namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class RecambiosAlmacenExcelDTO
    {
        public RecambiosAlmacenExcelDTO(RecambiosAlmacenDTO Dto)
        {
            if (Dto == null) return;
            Id = Dto.Id;
            CodigoAG = Dto.CodigoAG;
            NumeroRecambio = Dto.NumeroRecambio;
            Especialidad = Dto.Especialidad.Description;
            Familia = Dto.Familia.Description;
            NombreRecambio = Dto.NombreRecambio;
            MArcaRecambio = Dto.MArcaRecambio.Description;
            Ubicacion  = Dto.Ubicacion.Description;
            Referencia = Dto.Referencia;
            StockAlmacen = Dto.StockAlmacen;
            StockMinimo = Dto.StockMinimo;
            Precio = Dto.Precio;
            FechaPedido = Dto.FechaPedido;
            Pedido = Dto.Pedido;
            Observaciones = Dto.Observaciones;
            Cajon = Dto.Cajon;
            Seleccionado = Dto.Seleccionado;
            PerteneceKIT = Dto.PerteneceKIT;
            Instalacion = Dto.Instalacion;
            StockMaximo = Dto.StockMaximo;
            CodigoAntiguo = Dto.CodigoAntiguo;
            Nuevos = Dto.Nuevos;
            Usados = Dto.Usados;
            Reparados = Dto.Reparados;
            ImageUrl = Dto.ImageUrl;

        }
        public RecambiosAlmacenExcelDTO()
        {
        }
        public int Id { get; set; }
        public string CodigoAG { get; set; }
        public double? NumeroRecambio { get; set; }
        public string Especialidad { get; set; }
        public string Familia { get; set; }
        public string NombreRecambio { get; set; }
        public string MArcaRecambio { get; set; }
        public string Ubicacion { get; set; }
        public string Referencia { get; set; }
        public double StockAlmacen { get; set; }
        public double StockMinimo { get; set; }
        public decimal Precio { get; set; }
        public string FechaPedido { get; set; }
        public bool Pedido { get; set; }
        public string Observaciones { get; set; }
        public string Cajon { get; set; }
        public bool Seleccionado { get; set; }
        public bool PerteneceKIT { get; set; }
        public string Instalacion { get; set; }
        public double StockMaximo { get; set; }
        public string CodigoAntiguo { get; set; }
        public double Nuevos { get; set; }
        public double Usados { get; set; }
        public double Reparados { get; set; }
        public string ImageUrl { get; set; }
        public string ImageSource { get; set; }


    }

}

