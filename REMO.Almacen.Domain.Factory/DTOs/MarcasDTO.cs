﻿using REMO.Almacen.Infrastructure.Data.EntityModel;


namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class MarcasDTO
    {
        public MarcasDTO(zSYS_M_Marcas entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            Marca = entity.Marca;
            Proveedor = entity.Proveedor;
            DeletedReg = entity.DeletedReg;

        }
        public MarcasDTO()
        {
        }
        public int Id { get; set; }
        public string Marca { get; set; }
        public string Proveedor { get; set; }
        public bool DeletedReg { get; set; }
    }


}

