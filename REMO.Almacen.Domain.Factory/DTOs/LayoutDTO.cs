﻿using REMO.Almacen.Infrastructure.Data.EntityModel;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class LayoutDTO
    {
        public LayoutDTO(zSYS_M_Layout entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            Ruta = entity.Ruta;
            Layout = entity.Layout;
            DeletedReg = entity.DeletedReg;

        }
        public LayoutDTO()
        {
        }
        public int Id { get; set; }
        public string Layout { get; set; }
        public string Ruta { get; set; }
        public bool DeletedReg { get; set; }
    }

}

