﻿using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Infrastructure.Data.EntityModel;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class AlmacenDTO
    {
        public AlmacenDTO(zSYS_M_Almacenes entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            CodigoAlmacen = entity.CodigoAlmacen.ToString();
            Descripcion = entity.Descripcion;
            Planta.Id = entity.IdPlanta;
            DeletedReg = entity.DeletedReg;
        }
        public AlmacenDTO()
        {
        }
        public int Id { get; set; }
        public string CodigoAlmacen { get; set; }
        public string Descripcion { get; set; }
        public IdAndDescClass Planta { get; set; }
        public bool DeletedReg { get; set; }

    }

}

