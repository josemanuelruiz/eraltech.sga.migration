﻿namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class LogDtoPath
    {
        public LogDtoPath()
        {
        }
        public string rutaErr { get; set; }
        public string extErr { get; set; }
        public string rutaLog { get; set; }
        public string extLog { get; set; }
    }

    public class LogDto
    {
       public LogDto( )
        {   
        }
        public string IdUsuari { get; set; }
        public string Idsessio { get; set; }
        public string IdData { get; set; }
        public string IdHora { get; set; }
        public string Nvl_Log { get; set; }
        public string IdResultat { get; set; }
        public string IdFuncio { get; set; }
        public string Desc_Parametres { get; set; }
        public string Desc_Descripcio { get; set; }
        public string IdError { get; set; }
        public string DescError { get; set; }
        public LogDtoPath path { get; set; }

    }
      
}

