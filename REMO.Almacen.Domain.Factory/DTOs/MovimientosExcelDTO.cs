﻿using REMO.Almacen.Domain.Factory.DTOs.Common;
using System.Globalization;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class MovimientosExcelDTO
    {
        public MovimientosExcelDTO(MovimientosDTO Dto)
        {
            if (Dto == null) return;
            Id = Dto.Id;
            NumeroMovimiento = Dto.NumeroMovimiento;
            RecambiosAlmacen = Dto.IdRecambiosAlmacen.Description;
            NombreRecambio = Dto.NombreRecambio;
            MovementTypes = Dto.IdMovementTypes.Description;
            Ubicacion = Dto.IdUbicacion.Description;
            RecambiosAlmacen = Dto.IdRecambiosAlmacen.Description;
            FechaExtraccion = string.Format(Dto.FechaExtraccion.ToString(), "DD/MM/YYYY", CultureInfo.InvariantCulture);
            Extraidos = Dto.Extraidos;
            Vaciado = Dto.Vaciado;
            FechaRellenado = string.Format(Dto.FechaRellenado.ToString(), "DD/MM/YYYY", CultureInfo.InvariantCulture);
            Repuestos = Dto.Repuestos;
            Rellenado = Dto.Rellenado;
            Stock = Dto.Stock;
            Nuevos = Dto.Nuevos;
            Usados = Dto.Usados;
            Reparados = Dto.Reparados;
            DeletedReg = Dto.DeletedReg;
        }
        public MovimientosExcelDTO()
        {
        }
        public int Id { get; set; }
        public double NumeroMovimiento { get; set; }
        public string MovementTypes { get; set; }
        public string Ubicacion { get; set; }
        public string RecambiosAlmacen { get; set; }
        public string NombreRecambio { get; set; }
        public string FechaExtraccion { get; set; }
        public double Extraidos { get; set; }
        public bool Vaciado { get; set; }
        public string FechaRellenado { get; set; }
        public double Repuestos { get; set; }
        public bool Rellenado { get; set; }
        public double Stock { get; set; }
        public bool Nuevos { get; set; }
        public bool Usados { get; set; }
        public bool Reparados { get; set; }
        public bool DeletedReg { get; set; }

    }

}

