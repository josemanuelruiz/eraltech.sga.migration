﻿
using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Infrastructure.Data.EntityModel;
 

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class EquiposDTO
    {
        public EquiposDTO(zSYS_M_Equipos entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            CodigoEquipo = entity.CodigoEquipo ;
            Equipo = entity.Equipo ;
            Descripcion = entity.Descripcion ;
            Trafo_Subequipo = entity.Trafo_Subequipo;
            Fase.Id = entity.IdFase;            
            ProductoTrabajo = entity.ProductoTrabajo;
            DeletedReg = entity.DeletedReg;
        }
        public EquiposDTO()
        {
        }
        public int Id { get; set; }
        public double? CodigoEquipo { get; set; }
        public string Equipo { get; set; }
        public string Descripcion { get; set; }
        public string Trafo_Subequipo { get; set; }
        public IdAndDescClass Fase { get; set; }
        public string ProductoTrabajo { get; set; }
        public bool DeletedReg { get; set; }
    }

}

