﻿using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System;
using System.Globalization;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class MovimientosDTO
    {
        public MovimientosDTO(zSYS_T_Movimientos entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            NumeroMovimiento = entity.NumeroMovimiento.HasValue ? entity.NumeroMovimiento.Value: 0;
            IdRecambiosAlmacen.Id = Convert.ToInt32(entity.IdRecambiosAlmacen);
            NombreRecambio = entity.NombreRecambio;
            IdUbicacion.Id = entity.IdUbicacion;
            FechaExtraccion = string.Format(entity.FechaExtraccion.ToString(), "DD/MM/YYYY", CultureInfo.InvariantCulture);
            Extraidos = entity.Extraidos.HasValue ? entity.Extraidos.Value : 0;
            Vaciado = entity.Vaciado;
            FechaRellenado = string.Format(entity.FechaRellenado.ToString(), "DD/MM/YYYY", CultureInfo.InvariantCulture);
            Repuestos = entity.Repuestos.HasValue ? entity.Repuestos.Value : 0;
            Rellenado = entity.Rellenado;
            Stock = entity.Stock.HasValue ? entity.Stock.Value : 0;
            Nuevos = entity.Nuevos;
            Usados = entity.Usados;
            Reparados = entity.Reparados;
            DeletedReg = entity.DeletedReg;
        }
        public MovimientosDTO()
        {
        }
        public int Id { get; set; }
        public double NumeroMovimiento { get; set; }
        public IdAndDescClass IdMovementTypes { get; set; }
        public IdAndDescClass IdUbicacion { get; set; }
        public IdAndDescClass IdRecambiosAlmacen { get; set; }
        public string NombreRecambio { get; set; }
        public string FechaExtraccion { get; set; }
        public double Extraidos { get; set; }
        public bool Vaciado { get; set; }
        public string FechaRellenado { get; set; }
        public double Repuestos { get; set; }
        public bool Rellenado { get; set; }
        public double Stock { get; set; }
        public bool Nuevos { get; set; }
        public bool Usados { get; set; }
        public bool Reparados { get; set; }
        public bool DeletedReg { get; set; }

    }

}

