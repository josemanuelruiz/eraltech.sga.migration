﻿namespace REMO.Almacen.Domain.Factory.DTOs.Common
{
    public class IdAndDescClass
    {
        public IdAndDescClass()
        {
        }
        public int Id { get; set; }
        public string Description { get; set; }
    }
}