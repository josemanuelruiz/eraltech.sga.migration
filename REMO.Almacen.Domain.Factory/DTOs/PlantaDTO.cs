﻿using REMO.Almacen.Infrastructure.Data.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class PlantaDTO
    {
        public PlantaDTO(zSYS_M_Planta entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            CodigoPlanta = entity.CodigoPlanta.ToString();
            Descripcion = entity.Descripcion;
            CodigoPostal = entity.CodigoPostal;
            Direccion = entity.Direccion;
            DeletedReg = entity.DeletedReg;
        }

       public PlantaDTO()
        {
        }
        public int Id { get; set; }
        public string CodigoPlanta { get; set; }
        public string Descripcion { get; set; }
        public string Ubicacion { get; set; }
        public string CodigoPostal { get; set; }
        public string Direccion { get; set; }
        public bool DeletedReg { get; set; }
    }
}

