﻿using REMO.Almacen.Infrastructure.Data.EntityModel;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class FamiliasDTO
    {
        public FamiliasDTO(zSYS_M_Familias entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            Familia = entity.Familia;
            DeletedReg = entity.DeletedReg;

        }
        public FamiliasDTO()
        {
        }
        public int Id { get; set; }
        public string Familia { get; set; }
        public bool DeletedReg { get; set; }

    }

}

