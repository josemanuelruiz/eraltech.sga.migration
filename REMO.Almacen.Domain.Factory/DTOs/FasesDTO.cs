﻿using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Infrastructure.Data.EntityModel;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class FasesDTO
    {
        public FasesDTO(zSYS_M_Fases entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            Fase = entity.Fase;
            Linea.Id = entity.IdLinea;
            Layout.Id = entity.IdLayout;
            NombreArchivo = entity.NombreArchivo;
            DeletedReg = entity.DeletedReg; 
        }
        public FasesDTO()
        {
        }
        public int    Id { get; set; }
        public string Fase { get; set; }
        public IdAndDescClass Linea { get; set; }
        public IdAndDescClass Layout { get; set; }
        public string NombreArchivo { get; set; }
        public bool DeletedReg { get; set; }
    }

}

