﻿using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Infrastructure.Data.EntityModel;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class RecambiosAlmacenDTO
    {
        public RecambiosAlmacenDTO(zSYS_T_RecambiosAlmacen entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            CodigoAG = entity.CodigoAG;
            NumeroRecambio = entity.NumeroRecambio;
            //Especialidad = new IdAndDescClass { Id = entity.IdEspecialidad } ;
            //Familia= new IdAndDescClass { Id = entity.IdFamilia };
            NombreRecambio = entity.NombreRecambio;
            //MArcaRecambio = new IdAndDescClass { Id = entity.IdMArcaRecambio };
            //Ubicacion = new IdAndDescClass { Id = entity.IdUbicacion };
            Referencia = entity.Referencia;
            StockAlmacen = entity.StockAlmacen.HasValue ? entity.StockAlmacen.Value : 0;
            StockMinimo = entity.StockMinimo.HasValue ? entity.StockMinimo.Value : 0;
            Precio = entity.Precio.HasValue ? entity.Precio.Value : 0;
            FechaPedido = entity.TiempoEntrega;
            Pedido = entity.Pedido;
            Observaciones = entity.Observaciones;
            Cajon = entity.Cajon;
            Seleccionado = entity.Seleccionado;
            PerteneceKIT = entity.PerteneceKIT;
            Instalacion = entity.Instalacion;
            StockMaximo = entity.StockMaximo.HasValue ? entity.StockMaximo.Value : 0;
            CodigoAntiguo = entity.CodigoAntiguo;
            Nuevos = entity.Nuevos.HasValue ? entity.Nuevos.Value : 0;
            Usados = entity.Usados.HasValue ? entity.Usados.Value : 0;
            Reparados = entity.Reparados.HasValue ? entity.Reparados.Value : 0;
            ImageUrl = entity.ImageUrl;


        }
        public RecambiosAlmacenDTO()
        {
        }
        public int Id { get; set; }
        public string CodigoAG { get; set; }
        public double? NumeroRecambio { get; set; }
        public IdAndDescClass Especialidad { get; set; }
        public IdAndDescClass Familia { get; set; }
        public string NombreRecambio { get; set; }
        public IdAndDescClass MArcaRecambio { get; set; }
        public IdAndDescClass Ubicacion { get; set; }
        public string UbicacionDescription { get; set; }
        public string Referencia { get; set; }
        public double StockAlmacen { get; set; }
        public double StockMinimo { get; set; }
        public decimal Precio { get; set; }
        public string FechaPedido { get; set; }
        public bool Pedido { get; set; }
        public string Observaciones { get; set; }
        public string Cajon { get; set; }
        public bool Seleccionado { get; set; }
        public bool PerteneceKIT { get; set; }
        public string Instalacion { get; set; }
        public double StockMaximo { get; set; }
        public string CodigoAntiguo { get; set; }
        public double Nuevos { get; set; }
        public double Usados { get; set; }
        public double Reparados { get; set; }
        public string ImageUrl { get; set; }
        public byte[] ImageSource { get; set; }
    }
}

