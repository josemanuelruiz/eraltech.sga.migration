﻿using REMO.Almacen.Infrastructure.Data.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class MovementTypesDTO
    {
        public MovementTypesDTO(zSYS_M_MovementTypes entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            Description = entity.Description;
            DeletedReg = entity.DeletedReg;
        }
        public MovementTypesDTO()
        {
        }
        public int Id { get; set; }
        public string Description { get; set; }
        public bool DeletedReg { get; set; }

    }
}
