﻿using REMO.Almacen.Infrastructure.Data.EntityModel;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class EspecialidadesDTO
    {
        public EspecialidadesDTO(zSYS_M_Especialidades entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            Especialidad = entity.Especialidad;
            DeletedReg = entity.DeletedReg;
        }
        public EspecialidadesDTO()
        {

        }
        public int Id { get; set; }
        public string Especialidad { get; set; }
        public bool DeletedReg { get; set; }
    }
}

