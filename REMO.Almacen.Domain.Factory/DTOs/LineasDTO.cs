﻿using REMO.Almacen.Infrastructure.Data.EntityModel;

namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class LineasDTO
    {
        public LineasDTO(zSYS_M_Lineas entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            Linea = entity.Linea;
            DeletedReg = entity.DeletedReg;
        }
        public LineasDTO()
        {
        }
        public int Id { get; set; }
        public string Linea { get; set; }
        public string NewLinea { get; set; }
        public bool DeletedReg { get; set; }

    }

}

