﻿using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Infrastructure.Data.EntityModel;


namespace REMO.Almacen.Domain.Factory.DTOs
{
    public class UbicacionDTO
    {
        public UbicacionDTO(zSYS_M_Ubicaciones entity)
        {
            if (entity == null) return;
            Id = entity.Id;
            DescUbicacion = entity.DescUbicacion;
            IdAlmacen = entity.IdAlmacen;
            DeletedReg = entity.DeletedReg;
        }

        public UbicacionDTO()
        {
        }
        public int Id { get; set; }
        public string DescUbicacion { get; set; }
        public int IdAlmacen { get; set; }
        public IdAndDescClass Almacen { get; set; }
        public bool DeletedReg { get; set; }

    }

}