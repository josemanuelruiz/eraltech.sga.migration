﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.PlantaFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REMO.Almacen.Domain.Factory.Factories.PlantaFactory.Implementation
{
    public class PlantaFactory : IPlantaFactory
    {
        PlantaDTO IPlantaFactory.Create(zSYS_M_Planta entity)
        {
            return new PlantaDTO(entity);
        }

        zSYS_M_Planta IPlantaFactory.Parse(PlantaDTO model)
        {
            return new zSYS_M_Planta
            {
                Id = model.Id,
                CodigoPlanta = model.CodigoPlanta,
                Descripcion = model.Descripcion,
                CodigoPostal = model.CodigoPostal,
                Direccion = model.Direccion,
                DeletedReg = model.DeletedReg

            };
        }

        IEnumerable<PlantaDTO> IPlantaFactory.CreateList(IEnumerable<zSYS_M_Planta> entities)
        {
            return entities.Select(model => new PlantaDTO(model));
        }

        IEnumerable<zSYS_M_Planta> IPlantaFactory.ParseList(IEnumerable<PlantaDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Planta
            {
                Id = model.Id,
                CodigoPlanta = model.CodigoPlanta,
                Descripcion = model.Descripcion,
                CodigoPostal = model.CodigoPostal,
                Direccion = model.Direccion,
                DeletedReg = model.DeletedReg

            });
        }


    }
}
