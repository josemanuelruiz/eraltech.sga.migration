﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.PlantaFactory.Contract
{
    public interface IPlantaFactory
    {
        PlantaDTO Create(zSYS_M_Planta entity);
        IEnumerable<zSYS_M_Planta> ParseList(IEnumerable<PlantaDTO> modelList);
        IEnumerable<PlantaDTO> CreateList(IEnumerable<zSYS_M_Planta> entities);
        zSYS_M_Planta Parse(PlantaDTO model);
    }
}
