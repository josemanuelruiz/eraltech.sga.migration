﻿using REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;
using System;

namespace REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Implementation
{
    public class EquiposFactory : IEquiposFactory
    {
        EquiposDTO IEquiposFactory.Create(zSYS_M_Equipos entity)
        {
            return new EquiposDTO(entity);
        }

        zSYS_M_Equipos IEquiposFactory.Parse(EquiposDTO model)
        {
            return new zSYS_M_Equipos
            {
                Id = model.Id,
                CodigoEquipo = model.CodigoEquipo,
                Equipo = model.Equipo,
                Descripcion = model.Descripcion,
                Trafo_Subequipo = model.Trafo_Subequipo,
                IdFase = model.Fase.Id,
                ProductoTrabajo = model.ProductoTrabajo,
                DeletedReg = model.DeletedReg           
            };
        }

        IEnumerable<EquiposDTO> IEquiposFactory.CreateList(IEnumerable<zSYS_M_Equipos> entities)
        {
            return entities.Select(model => new EquiposDTO(model));
        }

        IEnumerable<zSYS_M_Equipos> IEquiposFactory.ParseList(IEnumerable<EquiposDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Equipos
            {
                Id = model.Id,
                CodigoEquipo = model.CodigoEquipo,
                Equipo = model.Equipo,
                Descripcion = model.Descripcion,
                Trafo_Subequipo = model.Trafo_Subequipo,
                IdFase = model.Fase.Id,
                ProductoTrabajo = model.ProductoTrabajo,
                DeletedReg = model.DeletedReg
        });
        }
    }

}

