﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Contract
{
    public interface IEquiposFactory
    {
        EquiposDTO Create(zSYS_M_Equipos entity);
        IEnumerable<zSYS_M_Equipos> ParseList(IEnumerable<EquiposDTO> modelList);
        IEnumerable<EquiposDTO> CreateList(IEnumerable<zSYS_M_Equipos> entities);
        zSYS_M_Equipos Parse(EquiposDTO model);
    }
}
