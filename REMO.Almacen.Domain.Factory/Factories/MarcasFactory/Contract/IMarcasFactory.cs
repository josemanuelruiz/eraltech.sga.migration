﻿using System;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Contract
{
    public interface IMarcasFactory
    {
        MarcasDTO Create(zSYS_M_Marcas entity);
        IEnumerable<zSYS_M_Marcas> ParseList(IEnumerable<MarcasDTO> modelList);
        IEnumerable<MarcasDTO> CreateList(IEnumerable<zSYS_M_Marcas> entities);
        zSYS_M_Marcas Parse(MarcasDTO model);
    }
}
