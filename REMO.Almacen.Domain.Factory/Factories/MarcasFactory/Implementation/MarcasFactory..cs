﻿using REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Implementation
{
    public class MarcasFactory : IMarcasFactory
    {
        MarcasDTO IMarcasFactory.Create(zSYS_M_Marcas entity)
        {
            return new MarcasDTO(entity);
        }

        zSYS_M_Marcas IMarcasFactory.Parse(MarcasDTO model)
        {
            return new zSYS_M_Marcas
            {
                Id = model.Id,
                Marca = model.Marca,
                Proveedor = model.Proveedor,
                DeletedReg = model.DeletedReg
        };
        }

        IEnumerable<MarcasDTO> IMarcasFactory.CreateList(IEnumerable<zSYS_M_Marcas> entities)
        {
            return entities.Select(model => new MarcasDTO(model));
        }

        IEnumerable<zSYS_M_Marcas> IMarcasFactory.ParseList(IEnumerable<MarcasDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Marcas
            {
                Id = model.Id,
                Marca = model.Marca,
                Proveedor = model.Proveedor,
                DeletedReg = model.DeletedReg
            });
        }
    }

}

