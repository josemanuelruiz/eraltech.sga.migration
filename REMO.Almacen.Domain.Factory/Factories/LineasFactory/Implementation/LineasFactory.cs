﻿using REMO.Almacen.Domain.Factory.Factories.LineasFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace REMO.Almacen.Domain.Factory.Factories.LineasFactory.Implementation
{
    public class LineasFactory : ILineasFactory
    {
        LineasDTO ILineasFactory.Create(zSYS_M_Lineas entity)
        {
            return new LineasDTO(entity);
        }

        zSYS_M_Lineas ILineasFactory.Parse(LineasDTO model)
        {
            if (!string.IsNullOrEmpty(model.NewLinea))
            {
                return new zSYS_M_Lineas
                {
                    Id = model.Id,
                    Linea =  model.NewLinea,
                    DeletedReg = model.DeletedReg
            };
            }
            else
            {
                return new zSYS_M_Lineas
                {
                    Id = model.Id,   
                    Linea = model.Linea,
                    DeletedReg = model.DeletedReg
                };
            }

        }

        IEnumerable<LineasDTO> ILineasFactory.CreateList(IEnumerable<zSYS_M_Lineas> entities)
        {
            return entities.Select(model => new LineasDTO(model));
        }

        IEnumerable<zSYS_M_Lineas> ILineasFactory.ParseList(IEnumerable<LineasDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Lineas
            {
                Id = model.Id,
                Linea = model.Linea,
                DeletedReg = model.DeletedReg
            });
        }
    }

}

