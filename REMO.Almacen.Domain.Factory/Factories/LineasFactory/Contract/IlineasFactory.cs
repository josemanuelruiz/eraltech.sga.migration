﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.LineasFactory.Contract
{
    public interface ILineasFactory
    {
        LineasDTO Create(zSYS_M_Lineas entity);
        IEnumerable<zSYS_M_Lineas> ParseList(IEnumerable<LineasDTO> modelList);
        IEnumerable<LineasDTO> CreateList(IEnumerable<zSYS_M_Lineas> entities);
        zSYS_M_Lineas Parse(LineasDTO model);
    }
}
