﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;

namespace REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Contract
{
    public interface IRecambiosAlmacenFactory
    {
        RecambiosAlmacenDTO Create(zSYS_T_RecambiosAlmacen entity);
        IEnumerable<zSYS_T_RecambiosAlmacen> ParseList(IEnumerable<RecambiosAlmacenDTO> modelList);
        IEnumerable<RecambiosAlmacenDTO> CreateList(IEnumerable<zSYS_T_RecambiosAlmacen> entities);
        zSYS_T_RecambiosAlmacen Parse(RecambiosAlmacenDTO model);
    }
}
