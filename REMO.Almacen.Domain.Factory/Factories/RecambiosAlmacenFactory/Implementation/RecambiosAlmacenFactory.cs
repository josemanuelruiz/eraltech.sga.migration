﻿using REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;
using System;

namespace REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Implementation
{
    public class RecambiosAlmacenFactory : IRecambiosAlmacenFactory
    {
        RecambiosAlmacenDTO IRecambiosAlmacenFactory.Create(zSYS_T_RecambiosAlmacen entity)
        {
            return new RecambiosAlmacenDTO(entity);
        }

        zSYS_T_RecambiosAlmacen IRecambiosAlmacenFactory.Parse(RecambiosAlmacenDTO model)
        {
            return new zSYS_T_RecambiosAlmacen
            {
                Id = model.Id,
                CodigoAG = model.CodigoAG,
                NumeroRecambio = model.NumeroRecambio.HasValue ? Convert.ToDouble(model.NumeroRecambio) : 0,
                IdEspecialidad = model.Especialidad != null ? model.Especialidad.Id : 0,
                IdFamilia = model.Familia != null ? model.Familia.Id : 0,
                NombreRecambio = model.NombreRecambio,
                IdMArcaRecambio = model.MArcaRecambio != null ? model.MArcaRecambio.Id : 0,
                IdUbicacion = model.Ubicacion != null ? model.Ubicacion.Id : 0,
                Referencia = model.Referencia,
                StockAlmacen = Convert.ToDouble(model.StockAlmacen),
                StockMinimo = Convert.ToDouble(model.StockMinimo),
                Precio = Convert.ToDecimal(model.Precio),
                TiempoEntrega = model.FechaPedido,
                Pedido = Convert.ToBoolean(model.Pedido),
                Observaciones = model.Observaciones,
                Cajon = model.Cajon,
                Seleccionado = Convert.ToBoolean(model.Seleccionado),
                PerteneceKIT = Convert.ToBoolean(model.PerteneceKIT),
                Instalacion = model.Instalacion,
                StockMaximo = Convert.ToDouble(model.StockMaximo),
                CodigoAntiguo = model.CodigoAntiguo,
                Nuevos = Convert.ToDouble(model.Nuevos),
                Usados = Convert.ToDouble(model.Usados),
                Reparados = Convert.ToDouble(model.Reparados),
                ImageUrl = model.ImageUrl
         
            };
        }

        IEnumerable<RecambiosAlmacenDTO> IRecambiosAlmacenFactory.CreateList(IEnumerable<zSYS_T_RecambiosAlmacen> entities)
        {
            return entities.Select(model => new RecambiosAlmacenDTO(model));
        }

        IEnumerable<zSYS_T_RecambiosAlmacen> IRecambiosAlmacenFactory.ParseList(IEnumerable<RecambiosAlmacenDTO> modelList)
        {
            return modelList.Select(model => new zSYS_T_RecambiosAlmacen
            {
                Id = model.Id,
                CodigoAG = model.CodigoAG,
                NumeroRecambio = Convert.ToDouble(model.NumeroRecambio),
                IdEspecialidad = Convert.ToInt32(model.Especialidad),
                IdFamilia = Convert.ToInt32(model.Familia),
                NombreRecambio = model.NombreRecambio,
                IdMArcaRecambio = Convert.ToInt32(model.MArcaRecambio),
                IdUbicacion = Convert.ToInt32(model.Ubicacion),
                Referencia = model.Referencia,
                StockAlmacen = Convert.ToDouble(model.StockAlmacen),
                StockMinimo = Convert.ToDouble(model.StockMinimo),
                Precio = Convert.ToDecimal(model.Precio),
                TiempoEntrega = model.FechaPedido,
                Pedido = Convert.ToBoolean(model.Pedido),
                Observaciones = model.Observaciones,
                Cajon = model.Cajon,
                Seleccionado = Convert.ToBoolean(model.Seleccionado),
                PerteneceKIT = Convert.ToBoolean(model.PerteneceKIT),
                Instalacion = model.Instalacion,
                StockMaximo = Convert.ToDouble(model.StockMaximo),
                CodigoAntiguo = model.CodigoAntiguo,
                Nuevos = Convert.ToDouble(model.Nuevos),
                Usados = Convert.ToDouble(model.Usados),
                Reparados = Convert.ToDouble(model.Reparados),
                ImageUrl = model.ImageUrl

            });
        }
    }

}

