﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Contract
{
    public interface IAlmacenesFactory
    {
        AlmacenDTO Create(zSYS_M_Almacenes entity);
        IEnumerable<zSYS_M_Almacenes> ParseList(IEnumerable<AlmacenDTO> modelList);
        IEnumerable<AlmacenDTO> CreateList(IEnumerable<zSYS_M_Almacenes> entities);
        zSYS_M_Almacenes Parse(AlmacenDTO model);
    }
}
