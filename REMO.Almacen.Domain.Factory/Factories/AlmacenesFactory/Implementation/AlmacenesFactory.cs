﻿using REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System;

namespace REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Implementation
{
    public class AlmacenesFactory : IAlmacenesFactory
    {
        AlmacenDTO IAlmacenesFactory.Create(zSYS_M_Almacenes model)
        {
            return new AlmacenDTO(model);
        }

        zSYS_M_Almacenes IAlmacenesFactory.Parse(AlmacenDTO model)
        {
            return new zSYS_M_Almacenes
            {
                Id = model.Id,
                CodigoAlmacen = model.CodigoAlmacen,
                Descripcion = model.Descripcion,
                IdPlanta = model.Planta.Id,
                DeletedReg = model.DeletedReg
        };
        }

        IEnumerable<AlmacenDTO> IAlmacenesFactory.CreateList(IEnumerable<zSYS_M_Almacenes> entities)
        {
            return entities.Select(model => new AlmacenDTO(model));
        }

        IEnumerable<zSYS_M_Almacenes> IAlmacenesFactory.ParseList(IEnumerable<AlmacenDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Almacenes
            {
                Id = model.Id,
                CodigoAlmacen = model.CodigoAlmacen,
                Descripcion = model.Descripcion,
                IdPlanta = model.Planta.Id,
                DeletedReg = model.DeletedReg
            });
        }
    }

}

