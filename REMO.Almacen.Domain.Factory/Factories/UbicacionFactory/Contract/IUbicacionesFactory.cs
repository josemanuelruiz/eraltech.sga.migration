﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;

namespace REMO.Almacen.Domain.Factory.Factories.UbicacionFactory.Contract
{
    public interface IUbicacionesFactory
    {
        UbicacionDTO Create(zSYS_M_Ubicaciones entity);
        IEnumerable<zSYS_M_Ubicaciones> ParseList(IEnumerable<UbicacionDTO> modelList);
        IEnumerable<UbicacionDTO> CreateList(IEnumerable<zSYS_M_Ubicaciones> entities);
        zSYS_M_Ubicaciones Parse(UbicacionDTO model);
    }
}
