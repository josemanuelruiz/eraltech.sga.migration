﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.UbicacionFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;
using System.Linq;

namespace REMO.Almacen.Domain.Factory.Factories.UbicacionFactory.Implementation
{
    public class UbicacionesFactory : IUbicacionesFactory
    {
        UbicacionDTO IUbicacionesFactory.Create(zSYS_M_Ubicaciones entity)
        {
            return new UbicacionDTO(entity);
        }

        zSYS_M_Ubicaciones IUbicacionesFactory.Parse(UbicacionDTO model)
        {
            return new zSYS_M_Ubicaciones
            {
                Id = model.Id,
                DescUbicacion = model.DescUbicacion,
                IdAlmacen = model.IdAlmacen,
                DeletedReg = model.DeletedReg

            };
        }

        IEnumerable<UbicacionDTO> IUbicacionesFactory.CreateList(IEnumerable<zSYS_M_Ubicaciones> entities)
        {
            return entities.Select(model => new UbicacionDTO(model));
        }

        IEnumerable<zSYS_M_Ubicaciones> IUbicacionesFactory.ParseList(IEnumerable<UbicacionDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Ubicaciones
            {
                Id = model.Id,
                DescUbicacion = model.DescUbicacion,
                IdAlmacen = model.IdAlmacen,
                DeletedReg = model.DeletedReg
            });
        }
    }
}

