﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;

namespace REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Contrat
{
    public interface IEspecialidadesFactory
    {
        EspecialidadesDTO Create(zSYS_M_Especialidades entity);
        IEnumerable<zSYS_M_Especialidades> ParseList(IEnumerable<EspecialidadesDTO> modelList);
        IEnumerable<EspecialidadesDTO> CreateList(IEnumerable<zSYS_M_Especialidades> entities);
        zSYS_M_Especialidades Parse(EspecialidadesDTO model);
    }
}

