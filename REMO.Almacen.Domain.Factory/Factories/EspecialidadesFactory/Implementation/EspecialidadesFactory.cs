﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Contrat;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;
using System.Linq;

namespace REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Implementation
{
    public class EspecialidadesFactory : IEspecialidadesFactory
    {
        EspecialidadesDTO IEspecialidadesFactory.Create(zSYS_M_Especialidades entity)
        {
            return new EspecialidadesDTO(entity);
        }

        zSYS_M_Especialidades IEspecialidadesFactory.Parse(EspecialidadesDTO model)
        {
            return new zSYS_M_Especialidades
            {
                Id = model.Id,
                Especialidad = model.Especialidad,
                DeletedReg = model.DeletedReg
            };
        }

        IEnumerable<EspecialidadesDTO> IEspecialidadesFactory.CreateList(IEnumerable<zSYS_M_Especialidades> entities)
        {
            return entities.Select(model => new EspecialidadesDTO(model));
        }

        IEnumerable<zSYS_M_Especialidades> IEspecialidadesFactory.ParseList(IEnumerable<EspecialidadesDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Especialidades
            {
                Id = model.Id,
                Especialidad = model.Especialidad,
                DeletedReg = model.DeletedReg
            });
        }

    }
}
