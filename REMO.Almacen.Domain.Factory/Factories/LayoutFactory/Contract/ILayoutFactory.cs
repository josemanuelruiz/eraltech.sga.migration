﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;

namespace REMO.Almacen.Domain.Factory.Factories.LayoutFactory.Contract
{
    public interface ILayoutFactory
    {
        LayoutDTO Create(zSYS_M_Layout entity);
        IEnumerable<zSYS_M_Layout> ParseList(IEnumerable<LayoutDTO> modelList);
        IEnumerable<LayoutDTO> CreateList(IEnumerable<zSYS_M_Layout> entities);
        zSYS_M_Layout Parse(LayoutDTO model);
    }
}
