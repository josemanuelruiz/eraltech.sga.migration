﻿using REMO.Almacen.Domain.Factory.Factories.LayoutFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace REMO.Almacen.Domain.Factory.Factories.LayoutFactory.Implementation
{
    public class LayoutFactory : ILayoutFactory
    {
        LayoutDTO ILayoutFactory.Create(zSYS_M_Layout entity)
        {
            return new LayoutDTO(entity);
        }

        zSYS_M_Layout ILayoutFactory.Parse(LayoutDTO model)
        {
            return new zSYS_M_Layout
            {
                Id = model.Id,
                Layout = model.Layout,
                Ruta = model.Ruta,
                DeletedReg = model.DeletedReg

        };
        }

        IEnumerable<LayoutDTO> ILayoutFactory.CreateList(IEnumerable<zSYS_M_Layout> entities)
        {
            return entities.Select(model => new LayoutDTO(model));
        }

        IEnumerable<zSYS_M_Layout> ILayoutFactory.ParseList(IEnumerable<LayoutDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Layout
            {
                Id = model.Id,
                Layout = model.Layout,
                Ruta = model.Ruta,
                DeletedReg = model.DeletedReg
            });
        }
    }

}

