﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.MovimientosFactory.Contract
{
    public interface IMovimientosFactory
    {
        MovimientosDTO Create(zSYS_T_Movimientos entity);
        IEnumerable<zSYS_T_Movimientos> ParseList(IEnumerable<MovimientosDTO> modelList);
        IEnumerable<MovimientosDTO> CreateList(IEnumerable<zSYS_T_Movimientos> entities);
        zSYS_T_Movimientos Parse(MovimientosDTO model);
    }
}
