﻿using REMO.Almacen.Domain.Factory.Factories.MovimientosFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using System;

namespace REMO.Almacen.Domain.Factory.Factories.MovimientosFactory.Implementation
{
    public class MovimientosFactory : IMovimientosFactory
    {
        MovimientosDTO IMovimientosFactory.Create(zSYS_T_Movimientos entity)
        {
            return new MovimientosDTO(entity);
        }

        zSYS_T_Movimientos IMovimientosFactory.Parse(MovimientosDTO model)
        {
            return new zSYS_T_Movimientos
            {
                Id = model.Id,
                NumeroMovimiento = Convert.ToDouble(model.NumeroMovimiento),
                IdRecambiosAlmacen = model.IdRecambiosAlmacen.Id,
                FechaExtraccion = DateTime.ParseExact(model.FechaExtraccion, "DD/MM/YYYY", CultureInfo.InvariantCulture),
                Extraidos = Convert.ToDouble(model.Extraidos),                
                Vaciado = Convert.ToBoolean(model.Vaciado),
                FechaRellenado = DateTime.ParseExact(model.FechaRellenado, "DD/MM/YYYY", CultureInfo.InvariantCulture),
                Repuestos = Convert.ToDouble(model.Repuestos),
                Rellenado = Convert.ToBoolean(model.Rellenado),
                Stock = Convert.ToDouble(model.Stock),
                Nuevos = Convert.ToBoolean(model.Nuevos),
                Usados = Convert.ToBoolean(model.Usados),
                Reparados = Convert.ToBoolean(model.Reparados),

                   
        };
        }

        IEnumerable<MovimientosDTO> IMovimientosFactory.CreateList(IEnumerable<zSYS_T_Movimientos> entities)
        {
            return entities.Select(model => new MovimientosDTO(model));
        }

        IEnumerable<zSYS_T_Movimientos> IMovimientosFactory.ParseList(IEnumerable<MovimientosDTO> modelList)
        {
            return modelList.Select(model => new zSYS_T_Movimientos
            {
                Id = model.Id,
                NumeroMovimiento = Convert.ToDouble(model.NumeroMovimiento),
                IdRecambiosAlmacen = model.IdRecambiosAlmacen.Id,                
                FechaExtraccion = DateTime.ParseExact(model.FechaExtraccion, "DD/MM/YYYY", CultureInfo.InvariantCulture),
                Extraidos = Convert.ToDouble(model.Extraidos),
                Vaciado = Convert.ToBoolean(model.Vaciado),
                FechaRellenado = DateTime.ParseExact(model.FechaRellenado, "DD/MM/YYYY", CultureInfo.InvariantCulture),
                Repuestos = Convert.ToDouble(model.Repuestos),
                Rellenado = Convert.ToBoolean(model.Rellenado),
                Stock = Convert.ToDouble(model.Stock),
                Nuevos = Convert.ToBoolean(model.Nuevos),
                Usados = Convert.ToBoolean(model.Usados),
                Reparados = Convert.ToBoolean(model.Reparados),


            });
        }
    }

}

