﻿using REMO.Almacen.Domain.Factory.Factories.FasesFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;
using System;

namespace REMO.Almacen.Domain.Factory.Factories.FasesFactory.Implementation
{
    public class FasesFactory : IFasesFactory
    {
        FasesDTO IFasesFactory.Create(zSYS_M_Fases entity)
        {
            return new FasesDTO(entity);
        }

        zSYS_M_Fases IFasesFactory.Parse(FasesDTO model)
        {
            return new zSYS_M_Fases
            {
             Id = model.Id,
             Fase = model.Fase,
             IdLinea = model.Linea.Id,
             IdLayout = model.Layout.Id,
             NombreArchivo = model.NombreArchivo,
             DeletedReg = model.DeletedReg
        };
        }

        IEnumerable<FasesDTO> IFasesFactory.CreateList(IEnumerable<zSYS_M_Fases> entities)
        {
            return entities.Select(model => new FasesDTO(model));
        }

        IEnumerable<zSYS_M_Fases> IFasesFactory.ParseList(IEnumerable<FasesDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Fases
            {
                Id = model.Id,
                Fase = model.Fase,
                IdLinea = model.Linea.Id,
                IdLayout = model.Layout.Id,
                NombreArchivo = model.NombreArchivo,
                DeletedReg = model.DeletedReg
        });
        }
    }

}

