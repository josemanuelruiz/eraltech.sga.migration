﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.FasesFactory.Contract
{
    public interface IFasesFactory
    {
        FasesDTO Create(zSYS_M_Fases entity);
        IEnumerable<zSYS_M_Fases> ParseList(IEnumerable<FasesDTO> modelList);
        IEnumerable<FasesDTO> CreateList(IEnumerable<zSYS_M_Fases> entities);
        zSYS_M_Fases Parse(FasesDTO model);
    }
}
