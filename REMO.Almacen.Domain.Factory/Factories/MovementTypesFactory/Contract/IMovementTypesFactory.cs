﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.MovementTypesFactory.Contract
{
    public interface IMovementTypesFactory
    {
        MovementTypesDTO Create(zSYS_M_MovementTypes entity);
        IEnumerable<zSYS_M_MovementTypes> ParseList(IEnumerable<MovementTypesDTO> modelList);
        IEnumerable<MovementTypesDTO> CreateList(IEnumerable<zSYS_M_MovementTypes> entities);
        zSYS_M_MovementTypes Parse(MovementTypesDTO model);
    }
}
