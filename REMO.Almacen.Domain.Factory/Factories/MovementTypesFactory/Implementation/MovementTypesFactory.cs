﻿using REMO.Almacen.Domain.Factory.Factories.MovementTypesFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace REMO.Almacen.Domain.Factory.Factories.MovementTypesFactory.Implementation
{
    public class MovementTypesFactory : IMovementTypesFactory
    {
        MovementTypesDTO IMovementTypesFactory.Create(zSYS_M_MovementTypes entity)
        {
            return new MovementTypesDTO(entity);
        }

        zSYS_M_MovementTypes IMovementTypesFactory.Parse(MovementTypesDTO model)
        {
            if (!string.IsNullOrEmpty(model.Description))
            {
                return new zSYS_M_MovementTypes
                {
                    Id = model.Id,
                    Description =  model.Description,
                    DeletedReg = model.DeletedReg
            };
            }
            else
            {
                return new zSYS_M_MovementTypes
                {
                    Id = model.Id,
                    Description = model.Description,
                    DeletedReg = model.DeletedReg
                };
            }

        }

        IEnumerable<MovementTypesDTO> IMovementTypesFactory.CreateList(IEnumerable<zSYS_M_MovementTypes> entities)
        {
            return entities.Select(model => new MovementTypesDTO(model));
        }

        IEnumerable<zSYS_M_MovementTypes> IMovementTypesFactory.ParseList(IEnumerable<MovementTypesDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_MovementTypes
            {
                Id = model.Id,
                Description = model.Description,
                DeletedReg = model.DeletedReg
            });
        }
    }

}

