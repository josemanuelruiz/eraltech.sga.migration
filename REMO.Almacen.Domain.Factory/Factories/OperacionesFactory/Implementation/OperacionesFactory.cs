﻿using REMO.Almacen.Domain.Factory.Factories.OperacionesFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace REMO.Almacen.Domain.Factory.Factories.OperacionesFactory.Implementation
{
    public class OperacionesFactory : IOperacionesFactory
    {
        OperacionesDTO IOperacionesFactory.Create(zSYS_M_Operaciones entity)
        {
            return new OperacionesDTO(entity);
        }

        zSYS_M_Operaciones IOperacionesFactory.Parse(OperacionesDTO model)
        {
            return new zSYS_M_Operaciones
            {
                Id = model.Id,
                Operacion = model.Operacion,
                DeletedReg = model.DeletedReg

            };
        }

        IEnumerable<OperacionesDTO> IOperacionesFactory.CreateList(IEnumerable<zSYS_M_Operaciones> entities)
        {
            return entities.Select(model => new OperacionesDTO(model));
        }

        IEnumerable<zSYS_M_Operaciones> IOperacionesFactory.ParseList(IEnumerable<OperacionesDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Operaciones
            {
                Id = model.Id,
                Operacion = model.Operacion,
                DeletedReg = model.DeletedReg
            });
        }
    }

}

