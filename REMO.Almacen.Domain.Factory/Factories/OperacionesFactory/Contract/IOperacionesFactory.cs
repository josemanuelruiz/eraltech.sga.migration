﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;


namespace REMO.Almacen.Domain.Factory.Factories.OperacionesFactory.Contract
{
    public interface IOperacionesFactory
    {
        OperacionesDTO Create(zSYS_M_Operaciones entity);
        IEnumerable<zSYS_M_Operaciones> ParseList(IEnumerable<OperacionesDTO> modelList);
        IEnumerable<OperacionesDTO> CreateList(IEnumerable<zSYS_M_Operaciones> entities);
        zSYS_M_Operaciones Parse(OperacionesDTO model);
    }
}
