﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;

namespace REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Contract
{
    public interface IFamiliasFactory
    {
        FamiliasDTO Create(zSYS_M_Familias entity);
        IEnumerable<zSYS_M_Familias> ParseList(IEnumerable<FamiliasDTO> modelList);
        IEnumerable<FamiliasDTO> CreateList(IEnumerable<zSYS_M_Familias> entities);
        zSYS_M_Familias Parse(FamiliasDTO model);
    }
}
