﻿using REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Implementation
{
    public class FamiliasFactory : IFamiliasFactory
    {
        FamiliasDTO IFamiliasFactory.Create(zSYS_M_Familias entity)
        {
            return new FamiliasDTO(entity);
        }

        zSYS_M_Familias IFamiliasFactory.Parse(FamiliasDTO model)
        {
            return new zSYS_M_Familias
            {
                Id = model.Id,
                Familia = model.Familia,
                DeletedReg = model.DeletedReg    
            };
        }

        IEnumerable<FamiliasDTO> IFamiliasFactory.CreateList(IEnumerable<zSYS_M_Familias> entities)
        {
            return entities.Select(model => new FamiliasDTO(model));
        }

        IEnumerable<zSYS_M_Familias> IFamiliasFactory.ParseList(IEnumerable<FamiliasDTO> modelList)
        {
            return modelList.Select(model => new zSYS_M_Familias
            {
                Id = model.Id,
                Familia = model.Familia,
                DeletedReg = model.DeletedReg
            });
        }
    }

}

