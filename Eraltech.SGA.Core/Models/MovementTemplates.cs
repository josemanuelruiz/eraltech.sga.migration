﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eraltech.SGA.Core.Models
{
    public partial class MovementTemplates
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int IdTipoMovimiento { get; set; }
        public int? IdAlmacenOrigen { get; set; }
        public int? IdUbicacionOrigen { get; set; }
        public int? IdZonaOrigen { get; set; }
        public int? IdAlmacenDestino { get; set; }
        public int? IdUbicacionDestino { get; set; }
        public int? IdZonaDestino { get; set; }
        public int? IdProducto { get; set; }
        public DateTime Created { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? Modified { get; set; }
        public string ModifiedUser { get; set; }
        public bool Public { get; set; }
        public bool Automatico { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Almacenes IdAlmacenDestinoNavigation { get; set; }
        public virtual Almacenes IdAlmacenOrigenNavigation { get; set; }
        public virtual Productos IdProductoNavigation { get; set; }
        public virtual TiposMovimientos IdTipoMovimientoNavigation { get; set; }
        public virtual Ubicaciones IdUbicacionDestinoNavigation { get; set; }
        public virtual Ubicaciones IdUbicacionOrigenNavigation { get; set; }
        public virtual Zonas IdZonaDestinoNavigation { get; set; }
        public virtual Zonas IdZonaOrigenNavigation { get; set; }
    }
}
