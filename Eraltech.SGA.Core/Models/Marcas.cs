﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Marcas
    {
        public Marcas()
        {
            Productos = new HashSet<Productos>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public int? IdProveedor { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Proveedores IdProveedorNavigation { get; set; }
        public virtual ICollection<Productos> Productos { get; set; }
    }
}
