﻿
namespace Eraltech.SGA.Core.Models
{
    public partial class AlmacenesUsuarios
    {
        public int IdUsuario { get; set; }
        public int IdAlmacen { get; set; }
        public bool Default { get; set; }

        public virtual Almacenes IdAlmacenNavigation { get; set; }
        public virtual Usuarios IdUsuarioNavigation { get; set; }
    }
}
