﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Ubicaciones
    {
        public Ubicaciones()
        {
            MovementTemplatesIdUbicacionDestinoNavigation = new HashSet<MovementTemplates>();
            MovementTemplatesIdUbicacionOrigenNavigation = new HashSet<MovementTemplates>();
            Movimientos = new HashSet<Movimientos>();
            Stocks = new HashSet<Stocks>();
        }

        public int Id { get; set; }
        public string CodigoUbicacion { get; set; }
        public string Descripcion { get; set; }
        public int IdAlmacen { get; set; }
        public bool DeletedReg { get; set; }
        public int? PosX { get; set; }
        public int? PosY { get; set; }

        public virtual Almacenes IdAlmacenNavigation { get; set; }
        public virtual ICollection<MovementTemplates> MovementTemplatesIdUbicacionDestinoNavigation { get; set; }
        public virtual ICollection<MovementTemplates> MovementTemplatesIdUbicacionOrigenNavigation { get; set; }
        public virtual ICollection<Movimientos> Movimientos { get; set; }
        public virtual ICollection<Stocks> Stocks { get; set; }
    }
}
