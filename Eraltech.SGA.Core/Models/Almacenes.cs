﻿using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Almacenes
    {
        public Almacenes()
        {
            AlmacenesUsuarios = new HashSet<AlmacenesUsuarios>();
            MovementTemplatesIdAlmacenDestinoNavigation = new HashSet<MovementTemplates>();
            MovementTemplatesIdAlmacenOrigenNavigation = new HashSet<MovementTemplates>();
            Movimientos = new HashSet<Movimientos>();
            Stocks = new HashSet<Stocks>();
            Ubicaciones = new HashSet<Ubicaciones>();
        }

        public int Id { get; set; }
        public string CodigoAlmacen { get; set; }
        public string Descripcion { get; set; }
        public int IdPlanta { get; set; }
        public bool UbicacionEstandar { get; set; }        
        public bool CodigoProductoManual { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Plantas IdPlantaNavigation { get; set; }
        public virtual ICollection<AlmacenesUsuarios> AlmacenesUsuarios { get; set; }
        public virtual ICollection<MovementTemplates> MovementTemplatesIdAlmacenDestinoNavigation { get; set; }
        public virtual ICollection<MovementTemplates> MovementTemplatesIdAlmacenOrigenNavigation { get; set; }
        public virtual ICollection<Movimientos> Movimientos { get; set; }
        public virtual ICollection<Stocks> Stocks { get; set; }
        public virtual ICollection<Ubicaciones> Ubicaciones { get; set; }
    }
}
