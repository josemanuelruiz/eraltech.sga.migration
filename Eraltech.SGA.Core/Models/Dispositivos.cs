﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Dispositivos
    {
        public Dispositivos()
        {
            Movimientos = new HashSet<Movimientos>();
            Stocks = new HashSet<Stocks>();
        }

        public int Id { get; set; }
        public string SerialNumber { get; set; }
        public string Descripcion { get; set; }

        public int IdProducto { get; set; }

        public DateTime FechaAlta { get; set; }

        public DateTime? FechaBaja { get; set; }

        public bool DeletedReg { get; set; }

        public virtual Productos IdProductoNavigation { get; set; }
        public virtual ICollection<Movimientos> Movimientos { get; set; }
        public virtual ICollection<Stocks> Stocks { get; set; }
    }
}
