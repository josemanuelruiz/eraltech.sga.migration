﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Plantas
    {
        public Plantas()
        {
            Almacenes = new HashSet<Almacenes>();
        }

        public int Id { get; set; }
        public string CodigoPlanta { get; set; }
        public string Descripcion { get; set; }
        public int IdFabrica { get; set; }
        public string CodigoPostal { get; set; }
        public string Direccion { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Fabricas IdFabricaNavigation { get; set; }
        public virtual ICollection<Almacenes> Almacenes { get; set; }
    }
}
