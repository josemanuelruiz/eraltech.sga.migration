﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class LineasPedidos
    {
        public int Id { get; set; }
        public int IdPedido { get; set; }
        public int IdProducto { get; set; }
        public double Cantiad { get; set; }
        public double PrecioUnitario { get; set; }
        public double Descuento { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Pedidos IdPedidoNavigation { get; set; }
        public virtual Productos IdProductoNavigation { get; set; }
    }
}
