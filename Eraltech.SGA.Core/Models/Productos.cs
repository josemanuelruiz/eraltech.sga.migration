﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Productos
    {
        public Productos()
        {
            Dispositivos = new HashSet<Dispositivos>();
            LineasPedidos = new HashSet<LineasPedidos>();
            MovementTemplates = new HashSet<MovementTemplates>();
            Movimientos = new HashSet<Movimientos>();
            Stocks = new HashSet<Stocks>();
        }

        public int Id { get; set; }
        public string CodigoProducto { get; set; }
        public string Nombre { get; set; }
        public string Referencia { get; set; }
        public int IdEspecialidad { get; set; }
        public int IdFamilia { get; set; }
        public int IdMarca { get; set; }
        public string Observaciones { get; set; }
        public string Instalacion { get; set; }
        public decimal? Precio { get; set; }
        public DateTime? FechaEntrega { get; set; }
        public bool Pedido { get; set; }
        public bool PerteneceKit { get; set; }
        public bool Seleccionado { get; set; }
        public bool SerialNumberRequired { get; set; }
        public string ImageUrl { get; set; }
        public DateTime FechaAlta { get; set; }
        public DateTime? FechaBaja { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Especialidades IdEspecialidadNavigation { get; set; }
        public virtual Familias IdFamiliaNavigation { get; set; }
        public virtual Marcas IdMarcaNavigation { get; set; }
        public virtual ICollection<Dispositivos> Dispositivos { get; set; }
        public virtual ICollection<LineasPedidos> LineasPedidos { get; set; }
        public virtual ICollection<MovementTemplates> MovementTemplates { get; set; }
        public virtual ICollection<Movimientos> Movimientos { get; set; }
        public virtual ICollection<Stocks> Stocks { get; set; }
    }
}
