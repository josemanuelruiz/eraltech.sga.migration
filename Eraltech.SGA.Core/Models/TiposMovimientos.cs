﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class TiposMovimientos
    {
        public TiposMovimientos()
        {
            MovementTemplates = new HashSet<MovementTemplates>();
            Movimientos = new HashSet<Movimientos>();
        }

        public int Id { get; set; }
        public string Descripcion { get; set; }
        public bool DeletedReg { get; set; }

        public virtual ICollection<MovementTemplates> MovementTemplates { get; set; }
        public virtual ICollection<Movimientos> Movimientos { get; set; }
    }
}
