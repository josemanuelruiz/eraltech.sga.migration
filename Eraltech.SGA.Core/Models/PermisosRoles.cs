﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class PermisosRoles
    {
        public int IdRol { get; set; }
        public int IdPermiso { get; set; }

        public virtual Permisos IdPermisoNavigation { get; set; }
        public virtual Roles IdRolNavigation { get; set; }
    }
}
