﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class DispositivosNissan
    {
        public string ORDEN { get; set; }
        public string MARCA { get; set; }
        public string MODELO { get; set; }
        public string N_SERIE { get; set; }
        public string TRAMO { get; set; }
        public string PUESTO_MAQUINA { get; set; }
        public string VERSION_CONTROLADOR { get; set; }
        public string IP { get; set; }
    }
}
