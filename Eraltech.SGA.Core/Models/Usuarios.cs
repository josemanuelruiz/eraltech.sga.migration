﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Usuarios
    {
        public Usuarios()
        {
            AlmacenesUsuarios = new HashSet<AlmacenesUsuarios>();
        }

        public int Id { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string Descripcion { get; set; }
        public string Estado { get; set; }
        public int? IdRol { get; set; }
        public DateTime Creacion { get; set; }
        public int? IntentosLogin { get; set; }
        public string Email { get; set; }

        public virtual Roles IdRolNavigation { get; set; }
        public virtual ICollection<AlmacenesUsuarios> AlmacenesUsuarios { get; set; }
    }
}
