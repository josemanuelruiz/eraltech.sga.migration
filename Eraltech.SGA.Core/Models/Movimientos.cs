﻿using System;

namespace Eraltech.SGA.Core.Models
{
    public partial class Movimientos
    {
        public int Id { get; set; }
        public int IdTipoMovimiento { get; set; }
        public int IdAlmacen { get; set; }
        public int IdUbicacion { get; set; }
        public int IdZona { get; set; }
        public int IdProducto { get; set; }
        public int? IdDispositivo { get; set; }
        public double Stock { get; set; }
        public string DetalleUbicacion { get; set; }
        public DateTime Created { get; set; }
        public string CreatedUser { get; set; }
        public DateTime? Modified { get; set; }
        public string ModifiedUser { get; set; }
        public bool Automatico { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Almacenes IdAlmacenNavigation { get; set; }
        public virtual Dispositivos IdDispositivoNavigation { get; set; }
        public virtual Productos IdProductoNavigation { get; set; }
        public virtual TiposMovimientos IdTipoMovimientoNavigation { get; set; }
        public virtual Ubicaciones IdUbicacionNavigation { get; set; }
        public virtual Zonas IdZonaNavigation { get; set; }
    }
}
