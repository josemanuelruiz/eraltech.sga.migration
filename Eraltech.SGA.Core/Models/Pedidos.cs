﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Pedidos
    {
        public Pedidos()
        {
            LineasPedidos = new HashSet<LineasPedidos>();
        }

        public int Id { get; set; }
        public int IdProveedor { get; set; }
        public double Iva { get; set; }
        public DateTime FechaPedido { get; set; }
        public DateTime FechaEntrega { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Proveedores IdProveedorNavigation { get; set; }
        public virtual ICollection<LineasPedidos> LineasPedidos { get; set; }
    }
}
