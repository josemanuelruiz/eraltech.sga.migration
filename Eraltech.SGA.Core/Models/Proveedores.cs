﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Proveedores
    {
        public Proveedores()
        {
            Marcas = new HashSet<Marcas>();
            Pedidos = new HashSet<Pedidos>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Cif { get; set; }
        public string Direccion { get; set; }
        public string CodigoPostal { get; set; }
        public string Localidad { get; set; }
        public string Telefono { get; set; }
        public string Email { get; set; }
        public bool DeletedReg { get; set; }

        public virtual ICollection<Marcas> Marcas { get; set; }
        public virtual ICollection<Pedidos> Pedidos { get; set; }
    }
}
