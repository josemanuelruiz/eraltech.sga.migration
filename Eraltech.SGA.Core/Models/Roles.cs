﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Roles
    {
        public Roles()
        {
            PermisosRoles = new HashSet<PermisosRoles>();
            Usuarios = new HashSet<Usuarios>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string GrupoDominio { get; set; }

        public virtual ICollection<PermisosRoles> PermisosRoles { get; set; }
        public virtual ICollection<Usuarios> Usuarios { get; set; }
    }
}
