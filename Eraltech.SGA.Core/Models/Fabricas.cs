﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Eraltech.SGA.Core.Models
{
    public partial class Fabricas
    {
        public Fabricas()
        {
            Plantas = new HashSet<Plantas>();
        }

        public int Id { get; set; }
        public string CodigoFabrica { get; set; }
        public string Descripcion { get; set; }
        public string CodigoPostal { get; set; }
        public string Direccion { get; set; }
        public bool DeletedReg { get; set; }

        public virtual ICollection<Plantas> Plantas { get; set; }
    }
}
