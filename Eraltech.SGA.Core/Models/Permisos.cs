﻿using System;
using System.Collections.Generic;

namespace Eraltech.SGA.Core.Models
{
    public partial class Permisos
    {
        public Permisos()
        {
            PermisosRoles = new HashSet<PermisosRoles>();
        }

        public int Id { get; set; }
        public string Codigo { get; set; }
        public string Descripcion { get; set; }

        public virtual ICollection<PermisosRoles> PermisosRoles { get; set; }
    }
}
