﻿
namespace Eraltech.SGA.Core.Models
{
    public partial class Stocks
    {
        public int Id { get; set; }
        public int IdAlmacen { get; set; }
        public int IdUbicacion { get; set; }
        public int IdProducto { get; set; }
        public int? IdDispositivo { get; set; }
        public double Stock { get; set; }
        public double StockMinimo { get; set; }
        public double? StockMaximo { get; set; }
        public double BuenEstado { get; set; }
        public double MalEstado { get; set; }
        public double PendienteClasificar { get; set; }
        public double PendienteRecibir { get; set; }
        public string DetalleUbicacion { get; set; }
        public bool DeletedReg { get; set; }

        public virtual Almacenes IdAlmacenNavigation { get; set; }
        public virtual Dispositivos IdDispositivoNavigation { get; set; }
        public virtual Productos IdProductoNavigation { get; set; }
        public virtual Ubicaciones IdUbicacionNavigation { get; set; }
    }
}
