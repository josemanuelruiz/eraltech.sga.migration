﻿using System;
using System.Threading.Tasks;
using Eraltech.SGA.Core.Repositories;

namespace Eraltech.SGA.Core
{
    public interface IUnitOfWork : IDisposable
    {
        IFabricaRepository Fabricas { get; }
        IPlantaRepository Plantas { get; }
        IAlmacenRepository Almacenes { get; }
        IUbicacionRepository Ubicaciones { get; }
        IEspecialidadRepository Especialidades { get; }
        IFamiliaRepository Familias { get; }
        IMarcaRepository Marcas { get; }
        IProductoRepository Productos { get; }
        IStockRepository Stocks { get; }
        IDispositivoRepository Dispositivos { get; }
        IMovimientoRepository Movimientos { get; }
        IZonaRepository Zonas { get; }
        IMovementTemplateRepository MovementTemplates { get; }
        IAlmacenUsuarioRepository AlmacenesUsuarios { get; }
        Task<int> CommitAsync();
    }
}