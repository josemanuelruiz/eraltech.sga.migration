﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IDispositivoRepository : IRepository<Dispositivos>
    {
        Task<IEnumerable<Dispositivos>> GetAllDispositivosAsync();

        Dispositivos GetDispositivo(int id);

        Dispositivos GetDispositivoBySerialNumber(string serialNumber);

        Dispositivos AddDispositivo(Dispositivos dispositivo);

        Dispositivos AddDispositivoWithTransaction(Dispositivos dispositivo, SGADbContext context);

        void UpdateDispositivo(Dispositivos dispositivo);

        void UpdateDispositivoWithTransaction(Dispositivos dispositivo, SGADbContext context);

        void DeleteDispositivo(int id);

        List<DispositivosNissan> GetDispositivosNissan();
    }
}
