﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IProductoRepository : IRepository<Productos>
    {
        IEnumerable<Productos> GetAllProductos();

        Productos GetProducto(int id);
        
        Productos GetProductoByCodigoProducto(string codigoProducto);

        Productos GetProductoByNombre(string nombre);

        Productos AddProducto(Productos producto);

        Productos AddProductoWithTransaction(Productos producto, SGADbContext context, bool identityOnOff = true);

        void UpdateProducto(Productos producto);

        void UpdateProductoWithTransaction(Productos producto, SGADbContext context);

        void DeleteProducto(int id);
    }
}
