﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IUbicacionRepository : IRepository<Ubicaciones>
    {
        IEnumerable<Ubicaciones> GetAllUbicaciones();

        Ubicaciones GetUbicacion(int id);

        Ubicaciones GetUbicacionByCodigoUbicacion(string codigoUbicacion);

        Ubicaciones AddUbicacion(Ubicaciones ubicacion, bool identityOnOff = true);

        void UpdateUbicacion(Ubicaciones ubicacion);

        void DeleteUbicacion(int id);

        void UpdateCodigoUbicacion();
    }
}
