﻿using Eraltech.SGA.Core.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IStockRepository : IRepository<Stocks>
    {
        Task<IEnumerable<Stocks>> GetAllStocksAsync();

        Stocks GetStock(int id);

        Stocks AddStock(Stocks stock);

        Stocks AddStockWithTransaction(Stocks Stock, SGADbContext dbContext, bool identityOnOff = true);
        
        void UpdateStock(Stocks stock);

        void DeleteStock(int id);

        IEnumerable<Stocks> GetStockOfProduct(int idProduct);

        IEnumerable<Stocks> GetStockOfDevice(int idDispositiv);
    }
}
