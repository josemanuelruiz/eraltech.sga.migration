﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IAlmacenUsuarioRepository : IRepository<AlmacenesUsuarios>
    {
        IEnumerable<AlmacenesUsuarios> GetAllAlmacenUsuarios();

        AlmacenesUsuarios AddAlmacenUsuario(AlmacenesUsuarios almacenUsuario);

        void UpdateAlmacenUsuario(AlmacenesUsuarios almacenUsuario);

        void DeleteAlmacenUsuario(int id);
    }
}
