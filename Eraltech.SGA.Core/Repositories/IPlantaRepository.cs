﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IPlantaRepository : IRepository<Plantas>
    {
        Task<IEnumerable<Plantas>> GetAllPlantasAsync();

        Plantas GetPlanta(int id);

        Plantas AddPlanta(Plantas planta);

        void UpdatePlanta(Plantas planta);

        void DeletePlanta(int id);
    }
}
