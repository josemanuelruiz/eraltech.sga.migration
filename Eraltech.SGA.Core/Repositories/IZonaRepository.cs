﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IZonaRepository : IRepository<Zonas>
    {
        Task<IEnumerable<Zonas>> GetAllZonasAsync();

        Zonas GetZona(int id);

        Zonas AddZona(Zonas zona);

        void UpdateZona(Zonas zona);

        void DeleteZona(int id);
    }
}
