﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IMovementTemplateRepository : IRepository<MovementTemplates>
    {
        Task<IEnumerable<MovementTemplates>> GetAllMovementTemplatesAsync(string user);

        MovementTemplates GetMovementTemplate(int id);

        MovementTemplates AddMovementTemplate(MovementTemplates movimiento, string user);

        void UpdateMovementTemplate(MovementTemplates movimiento);

        void DeleteMovementTemplate(int id);
    }
}
