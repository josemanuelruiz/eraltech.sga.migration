﻿using Eraltech.SGA.Core.Models;
using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IMovimientoRepository : IRepository<Movimientos>
    {
        Task<IEnumerable<Movimientos>> GetAllMovimientosAsync();

        Movimientos GetMovimiento(int id);

        Movimientos GetMovimientoInTransaction(int id, SGADbContext context);

        Movimientos AddMovimiento(Movimientos movimiento, string user);

        void Create__Migracion_Movimientos_RMO_SGA(MovimientosDTO Movimiento, int idSGA);

        Movimientos AddMovimientoInTransaction(Movimientos movimiento, string user, SGADbContext dbContext);

        void UpdateMovimiento(Movimientos movimiento);

        void DeleteMovimiento(int id);

        IEnumerable<TiposMovimientos> GetTipoMovimientos();
    }
}
