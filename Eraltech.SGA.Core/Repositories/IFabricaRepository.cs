﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IFabricaRepository : IRepository<Fabricas>
    {
        Task<IEnumerable<Fabricas>> GetAllFabricasAsync();

        Fabricas GetFabrica(int id);

        Fabricas AddFabrica(Fabricas fabrica);

        void UpdateFabrica(Fabricas fabrica);

        void DeleteFabrica(int id);
    }
}
