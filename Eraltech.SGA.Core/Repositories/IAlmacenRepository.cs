﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IAlmacenRepository : IRepository<Almacenes>
    {
        Task<IEnumerable<Almacenes>> GetAllAlmacenesAsync();

        IEnumerable<Almacenes> GetAllAlmacenes();

        Almacenes GetAlmacen(int id);

        Almacenes GetAlmacenlByCodigoAlmacen(string codigoAlmacen);

        Almacenes AddAlmacen(Almacenes almacen);

        void UpdateAlmacen(Almacenes almacen);

        void DeleteAlmacen(int id);
    }
}
