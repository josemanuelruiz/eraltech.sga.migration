﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IEspecialidadRepository : IRepository<Especialidades>
    {
        IEnumerable<Especialidades> GetAllEspecialidades();

        Especialidades GetEspecialidad(int id);

        Especialidades AddEspecialidad(Especialidades especialidad, bool identityOnOff = true);

        void UpdateEspecialidad(Especialidades especialidad);

        void DeleteEspecialidad(int id);
    }
}
