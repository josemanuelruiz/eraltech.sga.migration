﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IFamiliaRepository : IRepository<Familias>
    {
        IEnumerable<Familias> GetAllFamilias();

        Familias GetFamilia(int id);

        Familias AddFamilia(Familias familia, bool identityOnOff = true);

        void UpdateFamilia(Familias familia);

        void DeleteFamilia(int id);
    }
}
