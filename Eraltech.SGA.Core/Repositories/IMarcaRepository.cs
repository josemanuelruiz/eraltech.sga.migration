﻿using Eraltech.SGA.Core.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Eraltech.SGA.Core.Repositories
{
    public interface IMarcaRepository : IRepository<Marcas>
    {
        IEnumerable<Marcas> GetAllMarcas();

        Marcas GetMarca(int id);

        Marcas AddMarca(Marcas marca, bool identityOnOff = true);

        void UpdateMarca(Marcas marca);

        void DeleteMarca(int id);
    }
}
