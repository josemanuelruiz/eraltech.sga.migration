﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Dispositivo;
using static Eraltech.SGA.Shared.Enums;

namespace Eraltech.SGA.Core.Services
{
    public interface IDispositivoService
    {
        Task<IEnumerable<DispositivoGridModel>> GetAllDispositivos();
        DispositivoFormModel GetDispositivolById(int id);
        DispositivoGridModel GetDispositivoGridModelById(int id);
        Dispositivos GetDispositivolBySerialNumber(string serialNumber);
        ResponseDataModel<DispositivoGridModel> CreateDispositivo(DispositivoFormModel newDispositivoModel, Zona zona, string user, bool identityOnOff = true);
        ResponseModel UpdateDispositivo(DispositivoFormModel dispositivoModel);
        ResponseModel DeleteDispositivo(DispositivoGridModel dispositivoModel);
        IEnumerable<DispositivosNissan> GetDispositivosNissan();
    }
}
