﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Almacen;

namespace Eraltech.SGA.Core.Services
{
    public interface IAlmacenService
    {
        Task<IEnumerable<AlmacenGridModel>> GetAllAlmacenesAsync();
        IEnumerable<AlmacenGridModel> GetAllAlmacenes();
        AlmacenFormModel GetAlmacenlById(int id);
        AlmacenGridModel GetAlmacenGridModelById(int id);
        Almacenes GetAlmacenlByCodigoAlmacen(string codigoAlmacen);
        ResponseDataModel<AlmacenGridModel> CreateAlmacen(AlmacenFormModel newAlmacenModel);
        ResponseModel UpdateAlmacen(AlmacenFormModel almacenModel);
        ResponseModel DeleteAlmacen(AlmacenGridModel almacenModel);
    }
}
