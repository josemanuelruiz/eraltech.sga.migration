﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Ubicacion;

namespace Eraltech.SGA.Core.Services
{
    public interface IUbicacionService
    {
        IEnumerable<UbicacionGridModel> GetAllUbicaciones();
        UbicacionFormModel GetUbicacionlById(int id);
        UbicacionFormModel GetUbicacionlByCodigoUbicacion(string codigoUbicacion);
        UbicacionGridModel GetUbicacionGridModelById(int id);
        ResponseDataModel<UbicacionGridModel> CreateUbicacion(UbicacionFormModel newUbicacionModel, bool identityOnOff);
        ResponseModel UpdateUbicacion(UbicacionFormModel ubicacionModel);
        ResponseModel DeleteUbicacion(UbicacionGridModel ubicacionModel);
    }
}
