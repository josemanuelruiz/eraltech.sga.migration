﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Stock;

namespace Eraltech.SGA.Core.Services
{
    public interface IStockService
    {
        Task<IEnumerable<StockGridModel>> GetAllStocks();
        StockFormModel GetStocklById(int id);
        StockGridModel GetStockGridModelById(int id);
        ResponseDataModel<StockGridModel> CreateStock(StockFormModel newStockModel);
        ResponseModel UpdateStock(StockFormModel stockModel);
        ResponseModel DeleteStock(StockGridModel stockModel);
        IEnumerable<StockGridModel> GetProductoStocks(int idProducto);
        IEnumerable<StockGridModel> GetDispositivoStocks(int idDispositivo);
    }
}
