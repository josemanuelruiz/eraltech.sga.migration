﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Marca;

namespace Eraltech.SGA.Core.Services
{
    public interface IMarcaService
    {
        IEnumerable<MarcaGridModel> GetAllMarcas();
        MarcaFormModel GetMarcalById(int id);
        MarcaGridModel GetMarcaGridModelById(int id);
        ResponseDataModel<MarcaGridModel> CreateMarca(MarcaFormModel newMarcaModel, bool identityOnOff = true);
        ResponseModel UpdateMarca(MarcaFormModel marcaModel);
        ResponseModel DeleteMarca(MarcaGridModel ubicacionModel);
    }
}
