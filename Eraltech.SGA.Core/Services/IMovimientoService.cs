﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Movimiento;
using REMO.Almacen.Domain.Factory.DTOs;
using static Eraltech.SGA.Shared.Enums;

namespace Eraltech.SGA.Core.Services
{
    public interface IMovimientoService
    {
        Task<IEnumerable<MovimientoGridModel>> GetAllMovimientos();
        MovimientoFormModel GetMovimientoFormModelById(int id, SGADbContext dbContext = null);
        MovimientoGridModel GetMovimientoGridModelById(int id, SGADbContext dbContext = null);
        ResponseDataModel<MovimientoGridModel> CreateMovimiento(MovimientoFormModel newMovimientoModel, string user);
        ResponseDataModel<MovimientosDTO> Create__Migracion_Movimientos_RMO_SGA(MovimientosDTO Movimiento, int idSGA);
        ResponseModel UpdateMovimiento(MovimientoFormModel movimientoModel, string user);
        ResponseModel DeleteMovimiento(MovimientoGridModel movimientoModel);
        void AddMovimientoDeProducto(Productos producto, Stocks stock, Zona zona, TipoMovimiento tipoMovimiento, string user, SGADbContext context);
        void AddMovimientoDeDispositivo(Dispositivos dispositivo, Stocks stock, Zona zona, TipoMovimiento tipoMovimiento, string user, SGADbContext context, string detalleUbicacion = "");
    }
}
