﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Especialidad;

namespace Eraltech.SGA.Core.Services
{
    public interface IEspecialidadService
    {
        IEnumerable<EspecialidadGridModel> GetAllEspecialidades();
        EspecialidadFormModel GetEspecialidadlById(int id);
        EspecialidadGridModel GetEspecialidadGridModelById(int id);
        ResponseDataModel<EspecialidadGridModel> CreateEspecialidad(EspecialidadFormModel newEspecialidadModel, bool identityOnOff = true);
        ResponseModel UpdateEspecialidad(EspecialidadFormModel especialidadModel);
        ResponseModel DeleteEspecialidad(EspecialidadGridModel ubicacionModel);
    }
}
