﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Producto;
using static Eraltech.SGA.Shared.Enums;

namespace Eraltech.SGA.Core.Services
{
    public interface IProductoService
    {
        IEnumerable<ProductoGridModel> GetAllProductos();
        ProductoFormModel GetProductolById(int id);
        ProductoGridModel GetProductoGridModelById(int id);
        ProductoFormModel GetProductoByCodigoProducto(string codigoProducto);
        ResponseDataModel<ProductoGridModel> CreateProducto(ProductoFormModel newProductoModel, Zona zona, string user, bool identityOnOff = true);
        ResponseDataModel<ProductoGridModel> CreateProductoMigracion(ProductoFormModel newProductoModel, Zona zona, string user);
        ResponseModel UpdateProducto(ProductoFormModel productoModel);
        ResponseModel DeleteProducto(ProductoGridModel productoModel);
    }
}
