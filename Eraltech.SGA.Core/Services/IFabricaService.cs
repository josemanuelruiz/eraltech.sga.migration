﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Fabrica;

namespace Eraltech.SGA.Core.Services
{
    public interface IFabricaService
    {
        Task<IEnumerable<FabricaGridModel>> GetAllFabricas();
        FabricaFormModel GetFabricalById(int id);
        FabricaGridModel GetFabricaGridModelById(int id);
        ResponseDataModel<FabricaGridModel> CreateFabrica(FabricaFormModel newFabricaModel);
        ResponseModel UpdateFabrica(FabricaFormModel fabricaModel);
        ResponseModel DeleteFabrica(FabricaGridModel fabricaModel);
    }
}
