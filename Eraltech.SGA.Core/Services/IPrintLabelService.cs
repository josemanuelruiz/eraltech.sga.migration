﻿using Eraltech.SGA.Shared.Models.Dispositivo;
using Eraltech.SGA.Shared.Models.Producto;
using System.Collections.Generic;
using System.IO;

namespace Eraltech.SGA.Core.Services
{
    public interface IPrintLabelService
    {
        MemoryStream ConvertLabelsToPDF<T>(IEnumerable<T> modelList);
    }
}
