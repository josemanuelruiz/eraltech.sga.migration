﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Familia;

namespace Eraltech.SGA.Core.Services
{
    public interface IFamiliaService
    {
        IEnumerable<FamiliaGridModel> GetAllFamilias();
        FamiliaFormModel GetFamilialById(int id);
        FamiliaGridModel GetFamiliaGridModelById(int id);
        ResponseDataModel<FamiliaGridModel> CreateFamilia(FamiliaFormModel newFamiliaModel, bool identityOnOff = true);
        ResponseModel UpdateFamilia(FamiliaFormModel familiaModel);
        ResponseModel DeleteFamilia(FamiliaGridModel ubicacionModel);
    }
}
