﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Fabrica;
using Eraltech.SGA.Shared.Models.Planta;

namespace Eraltech.SGA.Core.Services
{
    public interface IPlantaService
    {
        Task<IEnumerable<PlantaGridModel>> GetAllPlantas();
        PlantaFormModel GetPlantalById(int id);
        PlantaGridModel GetPlantaGridModelById(int id);
        ResponseDataModel<PlantaGridModel> CreatePlanta(PlantaFormModel newPlantaModel);
        ResponseModel UpdatePlanta(PlantaFormModel plantaModel);
        ResponseModel DeletePlanta(PlantaGridModel plantaModel);
    }
}
