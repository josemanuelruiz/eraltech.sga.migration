﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.MovementTemplate;
using static Eraltech.SGA.Shared.Enums;

namespace Eraltech.SGA.Core.Services
{
    public interface IMovementTemplateService
    {
        Task<IEnumerable<MovementTemplateGridModel>> GetAllMovementTemplates(string user);
        MovementTemplateGridModel GetMovementTemplateGridModelById(int id);
        ResponseDataModel<MovementTemplateGridModel> CreateMovementTemplate(MovementTemplateFormModel newMovementTemplateModel, string user);
        ResponseModel UpdateMovementTemplate(MovementTemplateFormModel movimientoModel, string user);
        ResponseModel DeleteMovementTemplate(MovementTemplateGridModel movimientoModel);
    }
}
