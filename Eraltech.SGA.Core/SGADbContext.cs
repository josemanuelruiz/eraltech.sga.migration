﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Eraltech.SGA.Core.Models
{
    public partial class SGADbContext : DbContext
    {
        //private readonly string _connectionString;

        public SGADbContext()
        {
        }

        public SGADbContext(DbContextOptions<SGADbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Almacenes> Almacenes { get; set; }
        public virtual DbSet<AlmacenesUsuarios> AlmacenesUsuarios { get; set; }
        public virtual DbSet<Dispositivos> Dispositivos { get; set; }
        public virtual DbSet<DispositivosNissan> DispositivosNissan { get; set; }
        public virtual DbSet<Especialidades> Especialidades { get; set; }
        public virtual DbSet<Fabricas> Fabricas { get; set; }
        public virtual DbSet<Familias> Familias { get; set; }
        public virtual DbSet<LineasPedidos> LineasPedidos { get; set; }
        public virtual DbSet<Marcas> Marcas { get; set; }
        public virtual DbSet<MovementTemplates> MovementTemplates { get; set; }
        public virtual DbSet<Movimientos> Movimientos { get; set; }
        public virtual DbSet<Pedidos> Pedidos { get; set; }
        public virtual DbSet<Permisos> Permisos { get; set; }
        public virtual DbSet<PermisosRoles> PermisosRoles { get; set; }
        public virtual DbSet<Plantas> Plantas { get; set; }
        public virtual DbSet<Productos> Productos { get; set; }
        public virtual DbSet<Proveedores> Proveedores { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<Stocks> Stocks { get; set; }
        public virtual DbSet<TiposMovimientos> TiposMovimientos { get; set; }
        public virtual DbSet<Ubicaciones> Ubicaciones { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }
        public virtual DbSet<Zonas> Zonas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(Helper.ConnectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Almacenes>(entity =>
            {
                entity.Property(e => e.CodigoAlmacen)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UbicacionEstandar)
                    .IsRequired()
                    .HasDefaultValueSql("((1))");

                entity.HasOne(d => d.IdPlantaNavigation)
                    .WithMany(p => p.Almacenes)
                    .HasForeignKey(d => d.IdPlanta)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Almacenes_Plantas");
            });

            modelBuilder.Entity<AlmacenesUsuarios>(entity =>
            {
                entity.HasKey(e => new { e.IdUsuario, e.IdAlmacen })
                    .HasName("PK_ALMACENESUSUARIOS");

                entity.HasOne(d => d.IdAlmacenNavigation)
                    .WithMany(p => p.AlmacenesUsuarios)
                    .HasForeignKey(d => d.IdAlmacen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AlmacenesUsuarios_Almacenes");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.AlmacenesUsuarios)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_AlmacenesUsuarios_Usuarios");

                entity.Property(e => e.Default)
                    .IsRequired()
                    .HasDefaultValueSql("((0))");
            });

            modelBuilder.Entity<Dispositivos>(entity =>
            {
                entity.HasIndex(e => e.SerialNumber)
                    .HasName("Unique_Dispositivos_SerialNumber")
                    .IsUnique();

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SerialNumber)
                    .HasColumnName("SerialNumber")
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FechaAlta).HasColumnType("datetime");
                entity.Property(e => e.FechaBaja).HasColumnType("datetime");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.Dispositivos)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Dispositivos_Productos");
            });

            modelBuilder.Entity<DispositivosNissan>(entity =>
            {
                entity.HasNoKey();
            });

            modelBuilder.Entity<Especialidades>(entity =>
            {
                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Fabricas>(entity =>
            {
                entity.Property(e => e.CodigoFabrica)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoPostal)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasMaxLength(150)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Familias>(entity =>
            {
                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<LineasPedidos>(entity =>
            {
                entity.HasOne(d => d.IdPedidoNavigation)
                    .WithMany(p => p.LineasPedidos)
                    .HasForeignKey(d => d.IdPedido)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LineasPedidos_Pedidos");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.LineasPedidos)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_LineasPedidos_Productos");
            });

            modelBuilder.Entity<Marcas>(entity =>
            {
                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdProveedorNavigation)
                    .WithMany(p => p.Marcas)
                    .HasForeignKey(d => d.IdProveedor)
                    .HasConstraintName("FK_Marcas_Proveedores");
            });

            modelBuilder.Entity<MovementTemplates>(entity =>
            {
                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAlmacenDestinoNavigation)
                    .WithMany(p => p.MovementTemplatesIdAlmacenDestinoNavigation)
                    .HasForeignKey(d => d.IdAlmacenDestino)
                    .HasConstraintName("FK_MovementTemplates_Almacenes_Destino");

                entity.HasOne(d => d.IdAlmacenOrigenNavigation)
                    .WithMany(p => p.MovementTemplatesIdAlmacenOrigenNavigation)
                    .HasForeignKey(d => d.IdAlmacenOrigen)
                    .HasConstraintName("FK_MovementTemplates_Almacenes_Origen");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.MovementTemplates)
                    .HasForeignKey(d => d.IdProducto)
                    .HasConstraintName("FK_MovementTemplates_Productos");

                entity.HasOne(d => d.IdTipoMovimientoNavigation)
                    .WithMany(p => p.MovementTemplates)
                    .HasForeignKey(d => d.IdTipoMovimiento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_MovementTemplates_TiposMovimientos");

                entity.HasOne(d => d.IdUbicacionDestinoNavigation)
                    .WithMany(p => p.MovementTemplatesIdUbicacionDestinoNavigation)
                    .HasForeignKey(d => d.IdUbicacionDestino)
                    .HasConstraintName("FK_MovementTemplates_Ubicaciones_Destino");

                entity.HasOne(d => d.IdUbicacionOrigenNavigation)
                    .WithMany(p => p.MovementTemplatesIdUbicacionOrigenNavigation)
                    .HasForeignKey(d => d.IdUbicacionOrigen)
                    .HasConstraintName("FK_MovementTemplates_Ubicaciones_Origen");

                entity.HasOne(d => d.IdZonaDestinoNavigation)
                    .WithMany(p => p.MovementTemplatesIdZonaDestinoNavigation)
                    .HasForeignKey(d => d.IdZonaDestino)
                    .HasConstraintName("FK_MovementTemplates_Zonas_Destino");

                entity.HasOne(d => d.IdZonaOrigenNavigation)
                    .WithMany(p => p.MovementTemplatesIdZonaOrigenNavigation)
                    .HasForeignKey(d => d.IdZonaOrigen)
                    .HasConstraintName("FK_MovementTemplates_Zonas_Origen");
            });

            modelBuilder.Entity<Movimientos>(entity =>
            {
                entity.Property(e => e.Created).HasColumnType("datetime");

                entity.Property(e => e.CreatedUser)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.DetalleUbicacion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Modified).HasColumnType("datetime");

                entity.Property(e => e.ModifiedUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAlmacenNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdAlmacen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimientos_Almacenes");

                entity.HasOne(d => d.IdDispositivoNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdDispositivo)
                    .HasConstraintName("FK_Movimientos_Dispositivos");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimientos_Productos");

                entity.HasOne(d => d.IdTipoMovimientoNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdTipoMovimiento)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimientos_TiposMovimientos");

                entity.HasOne(d => d.IdUbicacionNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdUbicacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimientos_Ubicaciones");

                entity.HasOne(d => d.IdZonaNavigation)
                    .WithMany(p => p.Movimientos)
                    .HasForeignKey(d => d.IdZona)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Movimientos_Zonas");
            });

            modelBuilder.Entity<Pedidos>(entity =>
            {
                entity.Property(e => e.FechaEntrega).HasColumnType("datetime");

                entity.Property(e => e.FechaPedido).HasColumnType("datetime");

                entity.Property(e => e.Iva).HasColumnName("IVA");

                entity.HasOne(d => d.IdProveedorNavigation)
                    .WithMany(p => p.Pedidos)
                    .HasForeignKey(d => d.IdProveedor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Pedidos_Proveedores");
            });

            modelBuilder.Entity<Permisos>(entity =>
            {
                entity.Property(e => e.Codigo)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<PermisosRoles>(entity =>
            {
                entity.HasKey(e => new { e.IdRol, e.IdPermiso })
                    .HasName("PK_PERMISOSROLES");

                entity.HasOne(d => d.IdPermisoNavigation)
                    .WithMany(p => p.PermisosRoles)
                    .HasForeignKey(d => d.IdPermiso)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PermisosRoles_Permisos");

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.PermisosRoles)
                    .HasForeignKey(d => d.IdRol)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PermisosRoles_Roles");
            });

            modelBuilder.Entity<Plantas>(entity =>
            {
                entity.Property(e => e.CodigoPlanta)
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoPostal)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdFabricaNavigation)
                    .WithMany(p => p.Plantas)
                    .HasForeignKey(d => d.IdFabrica)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Plantas_Fabricas");
            });

            modelBuilder.Entity<Productos>(entity =>
            {
                entity.HasIndex(e => e.CodigoProducto)
                    .HasName("Unique_Productos_CodigoProducto")
                    .IsUnique();

                entity.Property(e => e.CodigoProducto)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.FechaEntrega).HasColumnType("datetime");
                entity.Property(e => e.FechaAlta).HasColumnType("datetime");
                entity.Property(e => e.FechaBaja).HasColumnType("datetime");


                entity.Property(e => e.ImageUrl)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Instalacion)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Observaciones).IsUnicode(false);

                entity.Property(e => e.PerteneceKit).HasColumnName("PerteneceKIT");

                entity.Property(e => e.Precio).HasColumnType("money");

                entity.Property(e => e.Referencia)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEspecialidadNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdEspecialidad)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Productos_Especialidades");

                entity.HasOne(d => d.IdFamiliaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdFamilia)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Productos_Familias");

                entity.HasOne(d => d.IdMarcaNavigation)
                    .WithMany(p => p.Productos)
                    .HasForeignKey(d => d.IdMarca)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Productos_Marcas");
            });

            modelBuilder.Entity<Proveedores>(entity =>
            {
                entity.Property(e => e.Cif)
                    .HasColumnName("CIF")
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.CodigoPostal)
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Localidad)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .HasMaxLength(150)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(20)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.Property(e => e.GrupoDominio)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Stocks>(entity =>
            {
                entity.Property(e => e.DetalleUbicacion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAlmacenNavigation)
                    .WithMany(p => p.Stocks)
                    .HasForeignKey(d => d.IdAlmacen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Stocks_Almacenes");

                entity.HasOne(d => d.IdDispositivoNavigation)
                    .WithMany(p => p.Stocks)
                    .HasForeignKey(d => d.IdDispositivo)
                    .HasConstraintName("FK_Stocks_Dispositivos");

                entity.HasOne(d => d.IdProductoNavigation)
                    .WithMany(p => p.Stocks)
                    .HasForeignKey(d => d.IdProducto)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Stocks_Productos");

                entity.HasOne(d => d.IdUbicacionNavigation)
                    .WithMany(p => p.Stocks)
                    .HasForeignKey(d => d.IdUbicacion)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Stocks_Ubicaciones");
            });

            modelBuilder.Entity<TiposMovimientos>(entity =>
            {
                entity.Property(e => e.Descripcion)
                    .HasMaxLength(40)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Ubicaciones>(entity =>
            {
                entity.Property(e => e.CodigoUbicacion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdAlmacenNavigation)
                    .WithMany(p => p.Ubicaciones)
                    .HasForeignKey(d => d.IdAlmacen)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Ubicaciones_Almacenes");
            });

            modelBuilder.Entity<Usuarios>(entity =>
            {
                entity.HasKey(e => e.Id)
                    .IsClustered(false);

                entity.HasIndex(e => e.Login)
                    .HasName("Unique_Usuarios_Login")
                    .IsUnique();

                entity.Property(e => e.Creacion).HasColumnType("datetime");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Email)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Estado)
                    .IsRequired()
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.Login)
                    .IsRequired()
                    .HasMaxLength(15)
                    .IsUnicode(false);

                entity.Property(e => e.Password)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdRolNavigation)
                    .WithMany(p => p.Usuarios)
                    .HasForeignKey(d => d.IdRol)
                    .HasConstraintName("FK_Usuarios_Roles");
            });

            modelBuilder.Entity<Zonas>(entity =>
            {
                entity.Property(e => e.Descripcion)
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
