﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class UbicacionRepository : RepositoryBase<Ubicaciones>, IUbicacionRepository
    {
        public UbicacionRepository(SGADbContext context)
            : base(context)
        { }

        public IEnumerable<Ubicaciones> GetAllUbicaciones()
        {
            using (var context = new SGADbContext())
            {
                var ubicaciones = context.Ubicaciones.Include(a => a.IdAlmacenNavigation)
                                                     .Include(u => u.Stocks).ThenInclude(x => x.IdProductoNavigation)
                                                     .Include(u => u.Stocks).ThenInclude(x => x.IdDispositivoNavigation)
                                                     .ToList();
                return ubicaciones;
            }
        }

        public Ubicaciones GetUbicacion(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Ubicaciones.Include(a => a.IdAlmacenNavigation).FirstOrDefault(f => f.Id == id);
            }
        }

        public Ubicaciones GetUbicacionByCodigoUbicacion(string codigoUbicacion)
        {
            using (var context = new SGADbContext())
            {
                return context.Ubicaciones.Include(a => a.IdAlmacenNavigation).FirstOrDefault(f => f.CodigoUbicacion == codigoUbicacion);
            }
        }

        public Ubicaciones AddUbicacion(Ubicaciones ubicacion, bool identityOnOff = true)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Ubicaciones.Add(ubicacion);

                if (identityOnOff)
                {
                    context.Database.OpenConnection();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Ubicaciones ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Ubicaciones OFF");
                    context.Database.CloseConnection();
                }
                else
                    context.SaveChanges();

                var id = entity.Entity.Id;
                return context.Ubicaciones.FirstOrDefault(f => f.Id == id);
            }
        }

        public void UpdateUbicacion(Ubicaciones ubicacion)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Ubicaciones>().Local.FirstOrDefault(x => x.Id == ubicacion.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(ubicacion).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteUbicacion(int id)
        {
            using (var context = new SGADbContext())
            {
                Ubicaciones ubicacion = context.Ubicaciones.Find(id);
                context.Ubicaciones.Remove(ubicacion);
                context.SaveChanges();
            }
        }

        public void UpdateCodigoUbicacion()
        {
            string updateSql = "UPDATE Ubicaciones SET CodigoUbicacion = RIGHT('00' + LTRIM(STR((ASCII(LEFT(descripcion, 1))) - 64)), 2) + REPLACE(RIGHT(descripcion, 5), '-', '') + '0' WHERE LEN(descripcion) <= 7";
            using (var context = new SGADbContext())
            {
                context.Database.OpenConnection();
                context.Database.ExecuteSqlCommand(updateSql);
                context.SaveChanges();
                context.Database.CloseConnection();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
