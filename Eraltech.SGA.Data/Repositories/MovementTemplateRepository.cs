﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;
using System;

namespace Eraltech.SGA.Data.Repositories
{
    public class MovementTemplateRepository : RepositoryBase<MovementTemplates>, IMovementTemplateRepository
    {
        public MovementTemplateRepository(SGADbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<MovementTemplates>> GetAllMovementTemplatesAsync(string user)
        {
            using (var context = new SGADbContext())
            {
                var movementTemplates = context.MovementTemplates.Where(t => t.CreatedUser == user || t.Public == true)
                                                     .Include(e => e.IdTipoMovimientoNavigation)
                                                     .Include(u => u.IdProductoNavigation)
                                                     .Include(p => p.IdAlmacenOrigenNavigation)
                                                     .Include(p => p.IdUbicacionOrigenNavigation)
                                                     .Include(p => p.IdZonaOrigenNavigation)
                                                     .Include(p => p.IdAlmacenDestinoNavigation)
                                                     .Include(p => p.IdUbicacionDestinoNavigation)
                                                     .Include(p => p.IdZonaDestinoNavigation)
                                                     .ToListAsync();
                return await movementTemplates;
            }
        }

        public MovementTemplates GetMovementTemplate(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.MovementTemplates.Include(e => e.IdTipoMovimientoNavigation)
                                                     .Include(u => u.IdProductoNavigation)
                                                     .Include(p => p.IdAlmacenOrigenNavigation)
                                                     .Include(p => p.IdUbicacionOrigenNavigation)
                                                     .Include(p => p.IdZonaOrigenNavigation)
                                                     .Include(p => p.IdAlmacenDestinoNavigation)
                                                     .Include(p => p.IdUbicacionDestinoNavigation)
                                                     .Include(p => p.IdZonaDestinoNavigation)
                                                     .FirstOrDefault(s => s.Id == id);
            }
        }

        public MovementTemplates AddMovementTemplate(MovementTemplates MovementTemplate, string user)
        {
            using (var context = new SGADbContext())
            {
                //MovementTemplate.Created = DateTime.Now;
                //MovementTemplate.CreatedUser = user;
                var entity = context.MovementTemplates.Add(MovementTemplate);
                context.SaveChanges();
                var id = entity.Entity.Id;
                return context.MovementTemplates.FirstOrDefault(s => s.Id == id);
            }
        }

        public void UpdateMovementTemplate(MovementTemplates MovementTemplate)
        {
            using (var context = new SGADbContext())
            {
                //MovementTemplate.Modified = DateTime.Now;
                var local = context.Set<MovementTemplates>().Local.FirstOrDefault(x => x.Id == MovementTemplate.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(MovementTemplate).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteMovementTemplate(int id)
        {
            using (var context = new SGADbContext())
            {
                MovementTemplates MovementTemplate = context.MovementTemplates.Find(id);
                context.MovementTemplates.Remove(MovementTemplate);
                context.SaveChanges();
            }
        }
    }
}
