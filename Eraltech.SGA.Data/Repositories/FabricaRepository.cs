﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class FabricaRepository : RepositoryBase<Fabricas>, IFabricaRepository
    {
        public FabricaRepository(SGADbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Fabricas>> GetAllFabricasAsync()
        {
            using (var context = new SGADbContext())
            {
                var fabricas = context.Fabricas.Include(f => f.Plantas).ToListAsync();
                return await fabricas;
            }
        }

        public Fabricas GetFabrica(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Fabricas.FirstOrDefault(f => f.Id == id);
            }
        }

        public Fabricas AddFabrica(Fabricas fabrica)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Fabricas.Add(fabrica);
                context.SaveChanges();
                var id = entity.Entity.Id;
                return context.Fabricas.FirstOrDefault(f => f.Id == id);
            }
        }

        public void UpdateFabrica(Fabricas fabrica)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Fabricas>().Local.FirstOrDefault(x => x.Id == fabrica.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(fabrica).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteFabrica(int id)
        {
            using (var context = new SGADbContext())
            {
                Fabricas fabrica = context.Fabricas.Find(id);
                context.Fabricas.Remove(fabrica);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
