﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;
using System;
using REMO.Almacen.Domain.Factory.DTOs;

namespace Eraltech.SGA.Data.Repositories
{
    public class MovimientoRepository : RepositoryBase<Movimientos>, IMovimientoRepository
    {
        public MovimientoRepository(SGADbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Movimientos>> GetAllMovimientosAsync()
        {
            using (var context = new SGADbContext())
            {
                var movimientos = context.Movimientos.Include(e => e.IdTipoMovimientoNavigation)
                                                     .Include(u => u.IdProductoNavigation)
                                                     .Include(p => p.IdAlmacenNavigation)
                                                     .Include(p => p.IdUbicacionNavigation)
                                                     .Include(p => p.IdDispositivoNavigation)
                                                     .Include(p => p.IdZonaNavigation)
                                                     .ToListAsync();
                return await movimientos;
            }
        }

        public Movimientos GetMovimiento(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Movimientos.Include(e => e.IdTipoMovimientoNavigation)
                                        .Include(u => u.IdProductoNavigation)
                                        .Include(p => p.IdAlmacenNavigation)
                                        .Include(p => p.IdUbicacionNavigation)
                                        .Include(p => p.IdDispositivoNavigation)
                                        .Include(p => p.IdZonaNavigation).FirstOrDefault(s => s.Id == id);
            }
        }

        public Movimientos GetMovimientoInTransaction(int id, SGADbContext context)
        {
            return context.Movimientos.Include(e => e.IdTipoMovimientoNavigation)
                                        .Include(u => u.IdProductoNavigation)
                                        .Include(p => p.IdAlmacenNavigation)
                                        .Include(p => p.IdUbicacionNavigation)
                                        .Include(p => p.IdDispositivoNavigation)
                                        .Include(p => p.IdZonaNavigation).FirstOrDefault(s => s.Id == id);
        }

        public Movimientos AddMovimiento(Movimientos Movimiento, string user)
        {
            using (var context = new SGADbContext())
            {
                Movimiento.Created = DateTime.Now;
                Movimiento.CreatedUser = user;
                var entity = context.Movimientos.Add(Movimiento);
                context.SaveChanges();
                var id = entity.Entity.Id;
                //Create__Migracion_Movimientos_RMO_SGA(Movimiento, id);
                return context.Movimientos.FirstOrDefault(s => s.Id == id);
            }
        }

        public void Create__Migracion_Movimientos_RMO_SGA(MovimientosDTO Movimiento, int idSGA)
        {
            try
            {
                string nombreProducto = Movimiento.NombreRecambio.Replace("'", "''");
                //object fechaRellenado = DBNull.Value;
                //if (!string.IsNullOrEmpty(Movimiento.FechaRellenado))
                //    fechaRellenado = DateTime.Parse(Movimiento.FechaRellenado);

                //object fechaExtraccion = DBNull.Value;
                //if (!string.IsNullOrEmpty(Movimiento.FechaExtraccion))
                //    fechaExtraccion = DateTime.Parse(Movimiento.FechaExtraccion);

                string fechaRellenado = "NULL";
                if (!string.IsNullOrEmpty(Movimiento.FechaRellenado))
                    fechaRellenado = $"'{Movimiento.FechaRellenado}'";

                object fechaExtraccion = "NULL";
                if (!string.IsNullOrEmpty(Movimiento.FechaExtraccion))
                    fechaExtraccion = $"'{Movimiento.FechaExtraccion}'";

                int vaciado = Movimiento.Vaciado ? 1 : 0;
                int rellenado = Movimiento.Rellenado ? 1 : 0;
                int nuevos = Movimiento.Nuevos ? 1 : 0;
                int usados = Movimiento.Usados ? 1 : 0;
                int reparados = Movimiento.Reparados ? 1 : 0;
                int deletedReg = Movimiento.DeletedReg ? 1 : 0;

                string sql = $"INSERT INTO _Migracion_Movimientos_RMO_SGA VALUES({idSGA}," +
                    $"{Movimiento.Id},{Movimiento.NumeroMovimiento},{Movimiento.IdMovementTypes.Id},{Movimiento.IdUbicacion.Id}," +
                    $"{Movimiento.IdRecambiosAlmacen.Id},'{nombreProducto}',{fechaExtraccion},{Movimiento.Extraidos}," +
                    $"{vaciado},{fechaRellenado},{Movimiento.Repuestos},{rellenado},{Movimiento.Stock}," +
                    $"{nuevos},{usados},{reparados},{deletedReg})";

                using (var context = new SGADbContext())
                {
                    context.Database.OpenConnection();
                    context.Database.ExecuteSqlCommand(sql);
                    context.SaveChanges();
                    context.Database.CloseConnection();
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message + ex.InnerException?.Message;
            }
        }

        public Movimientos AddMovimientoInTransaction(Movimientos Movimiento, string user, SGADbContext context)
        {
            Movimiento.Created = DateTime.Now;
            Movimiento.CreatedUser = user;
            var entity = context.Movimientos.Add(Movimiento);
            context.SaveChanges();
            var id = entity.Entity.Id;
            return context.Movimientos.FirstOrDefault(s => s.Id == id);
        }

        public void UpdateMovimiento(Movimientos Movimiento)
        {
            using (var context = new SGADbContext())
            {
                Movimiento.Modified = DateTime.Now;
                var local = context.Set<Movimientos>().Local.FirstOrDefault(x => x.Id == Movimiento.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(Movimiento).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteMovimiento(int id)
        {
            using (var context = new SGADbContext())
            {
                Movimientos Movimiento = context.Movimientos.Find(id);
                context.Movimientos.Remove(Movimiento);
                context.SaveChanges();
            }
        }

        public IEnumerable<TiposMovimientos> GetTipoMovimientos()
        {
            List<TiposMovimientos> tiposMovimientos = null;
            using (var context = new SGADbContext())
            {
                tiposMovimientos = context.TiposMovimientos.Where(t => !t.DeletedReg).ToList();
            }
            return tiposMovimientos;
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
