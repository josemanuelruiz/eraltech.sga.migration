﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class AlmacenUsuarioRepository : RepositoryBase<AlmacenesUsuarios>, IAlmacenUsuarioRepository
    {
        public AlmacenUsuarioRepository(SGADbContext context)
            : base(context)
        { }

        public IEnumerable<AlmacenesUsuarios> GetAllAlmacenUsuarios()
        {
            using (var context = new SGADbContext())
            {
                var almacenesUsuarios = context.AlmacenesUsuarios.Include(a => a.IdAlmacenNavigation).Include(u => u.IdUsuarioNavigation).ToList();
                return almacenesUsuarios;
            }
        }

        public AlmacenesUsuarios AddAlmacenUsuario(AlmacenesUsuarios almacenUsuario)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.AlmacenesUsuarios.Add(almacenUsuario);
                context.SaveChanges();
                return context.AlmacenesUsuarios.FirstOrDefault(x => x.IdAlmacen == almacenUsuario.IdAlmacen && x.IdUsuario == x.IdUsuario);
            }
        }

        public void UpdateAlmacenUsuario(AlmacenesUsuarios almacenUsuario)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<AlmacenesUsuarios>().Local.FirstOrDefault(x => x.IdAlmacen == almacenUsuario.IdAlmacen && x.IdUsuario == x.IdUsuario);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(almacenUsuario).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteAlmacenUsuario(int id)
        {
            using (var context = new SGADbContext())
            {
                AlmacenesUsuarios almacenUsuario = context.AlmacenesUsuarios.Find(id);
                context.AlmacenesUsuarios.Remove(almacenUsuario);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
