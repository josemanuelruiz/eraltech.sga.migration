﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class ZonaRepository : RepositoryBase<Zonas>, IZonaRepository
    {
        public ZonaRepository(SGADbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Zonas>> GetAllZonasAsync()
        {
            using (var context = new SGADbContext())
            {
                var zonas = context.Zonas.ToListAsync();
                return await zonas;
            }
        }

        public Zonas GetZona(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Zonas.FirstOrDefault(z => z.Id == id);
            }
        }

        public Zonas AddZona(Zonas zona)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Zonas.Add(zona);
                context.SaveChanges();
                var id = entity.Entity.Id;
                return context.Zonas.FirstOrDefault(z => z.Id == id);
            }
        }

        public void UpdateZona(Zonas zona)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Zonas>().Local.FirstOrDefault(x => x.Id == zona.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(zona).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteZona(int id)
        {
            using (var context = new SGADbContext())
            {
                Zonas zona = context.Zonas.Find(id);
                context.Zonas.Remove(zona);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
