﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class PlantaRepository : RepositoryBase<Plantas>, IPlantaRepository
    {
        public PlantaRepository(SGADbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Plantas>> GetAllPlantasAsync()
        {
            using (var context = new SGADbContext())
            {
                var plantas = context.Plantas.Include(f => f.IdFabricaNavigation).Include(a => a.Almacenes).ToListAsync();
                return await plantas;
            }
        }

        public Plantas GetPlanta(int id)
        {
            using (var context = new SGADbContext())
            {
                return SGADbContext.Plantas.FirstOrDefault(f => f.Id == id);
            }
        }

        public Plantas AddPlanta(Plantas planta)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Plantas.Add(planta);
                context.SaveChanges();
                var id = entity.Entity.Id;
                return context.Plantas.FirstOrDefault(f => f.Id == id);
            }
        }

        public void UpdatePlanta(Plantas planta)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Plantas>().Local.FirstOrDefault(x => x.Id == planta.Id);
                if (local != null)
                    context.Entry(local).State = EntityState.Detached;

                context.Entry(planta).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeletePlanta(int id)
        {
            using (var context = new SGADbContext())
            {
                Plantas planta = context.Plantas.Find(id);
                context.Plantas.Remove(planta);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
