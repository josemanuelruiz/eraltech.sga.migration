﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class EspecialidadRepository : RepositoryBase<Especialidades>, IEspecialidadRepository
    {
        public EspecialidadRepository(SGADbContext context)
            : base(context)
        { }

        public IEnumerable<Especialidades> GetAllEspecialidades()
        {
            using (var context = new SGADbContext())
            {
                var Especialidades = context.Especialidades.Include(p => p.Productos).ThenInclude(x => x.IdEspecialidadNavigation)
                                                           .Include(p => p.Productos).ThenInclude(x => x.IdFamiliaNavigation)
                                                           .Include(p => p.Productos).ThenInclude(x => x.IdMarcaNavigation)
                                                           .ToList();
                return Especialidades;
            }
        }

        public Especialidades GetEspecialidad(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Especialidades.FirstOrDefault(f => f.Id == id);
            }
        }

        public Especialidades AddEspecialidad(Especialidades especialidad, bool identityOnOff = true)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Especialidades.Add(especialidad);

                if (identityOnOff)
                {
                    context.Database.OpenConnection();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Especialidades ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Especialidades OFF");
                    context.Database.CloseConnection();
                }
                else
                    context.SaveChanges();

                var id = entity.Entity.Id;
                return context.Especialidades.FirstOrDefault(f => f.Id == id);
            }
        }

        public void UpdateEspecialidad(Especialidades especialidad)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Especialidades>().Local.FirstOrDefault(x => x.Id == especialidad.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(especialidad).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteEspecialidad(int id)
        {
            using (var context = new SGADbContext())
            {
                Especialidades especialidad = context.Especialidades.Find(id);
                context.Especialidades.Remove(especialidad);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
