﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class FamiliaRepository : RepositoryBase<Familias>, IFamiliaRepository
    {
        public FamiliaRepository(SGADbContext context)
            : base(context)
        { }

        public IEnumerable<Familias> GetAllFamilias()
        {
            using (var context = new SGADbContext())
            {
                var familias = context.Familias.Include(p => p.Productos).ThenInclude(x => x.IdEspecialidadNavigation)
                                               .Include(p => p.Productos).ThenInclude(x => x.IdFamiliaNavigation)
                                               .Include(p => p.Productos).ThenInclude(x => x.IdMarcaNavigation)
                                               .ToList();
                return familias;
            }
        }

        public Familias GetFamilia(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Familias.FirstOrDefault(f => f.Id == id);
            }
        }

        public Familias AddFamilia(Familias familia, bool identityOnOff = true)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Familias.Add(familia);

                if (identityOnOff)
                {
                    context.Database.OpenConnection();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Familias ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Familias OFF");
                    context.Database.CloseConnection();
                }
                else
                    context.SaveChanges();

                var id = entity.Entity.Id;
                return context.Familias.FirstOrDefault(f => f.Id == id);
            }
        }

        public void UpdateFamilia(Familias familia)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Familias>().Local.FirstOrDefault(x => x.Id == familia.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(familia).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteFamilia(int id)
        {
            using (var context = new SGADbContext())
            {
                Familias familia = context.Familias.Find(id);
                context.Familias.Remove(familia);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
