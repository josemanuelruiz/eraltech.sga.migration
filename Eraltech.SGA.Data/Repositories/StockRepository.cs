﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class StockRepository : RepositoryBase<Stocks>, IStockRepository
    {
        public StockRepository(SGADbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Stocks>> GetAllStocksAsync()
        {
            using (var context = new SGADbContext())
            {
                var Stocks = context.Stocks.Include(e => e.IdAlmacenNavigation)
                                           .Include(u => u.IdUbicacionNavigation)
                                           .Include(p => p.IdProductoNavigation)
                                           .Include(d => d.IdDispositivoNavigation)
                                           .ToListAsync();
                return await Stocks;
            }
        }

        public Stocks GetStock(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Stocks.Include(e => e.IdAlmacenNavigation)
                                     .Include(u => u.IdUbicacionNavigation)
                                     .Include(p => p.IdProductoNavigation)
                                     .Include(d => d.IdDispositivoNavigation).FirstOrDefault(s => s.Id == id);
            }
        }

        public Stocks AddStock(Stocks Stock)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Stocks.Add(Stock);
                context.SaveChanges();
                var id = entity.Entity.Id;
                return context.Stocks.FirstOrDefault(s => s.Id == id);
            }
        }

        public Stocks AddStockWithTransaction(Stocks Stock, SGADbContext dbContext, bool identityOnOff = true)
        {
            var entity = dbContext.Stocks.Add(Stock);

            if (identityOnOff)
            {
                dbContext.Database.OpenConnection();
                dbContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Stocks ON");
                dbContext.SaveChanges();
                dbContext.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Stocks OFF");
                dbContext.Database.CloseConnection();
            }
            else
                dbContext.SaveChanges();

            var id = entity.Entity.Id;
            return dbContext.Stocks.FirstOrDefault(s => s.Id == id);
        }

        public void UpdateStock(Stocks Stock)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Stocks>().Local.FirstOrDefault(x => x.Id == Stock.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(Stock).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteStock(int id)
        {
            using (var context = new SGADbContext())
            {
                Stocks Stock = context.Stocks.Find(id);
                context.Stocks.Remove(Stock);
                context.SaveChanges();
            }
        }

        public IEnumerable<Stocks> GetStockOfProduct(int idProduct)
        {
            using (var context = new SGADbContext())
            {
                var Stocks = context.Stocks.Where(s => s.IdProducto == idProduct && s.IdDispositivo == null).ToList();
                return Stocks;
            }
        }

        public IEnumerable<Stocks> GetStockOfDevice(int idDispositiv)
        {
            using (var context = new SGADbContext())
            {
                var Stocks = context.Stocks.Where(s => s.IdDispositivo == idDispositiv).ToList();
                return Stocks;
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
