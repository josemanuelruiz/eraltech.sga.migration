﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class AlmacenRepository : RepositoryBase<Almacenes>, IAlmacenRepository
    {
        public AlmacenRepository(SGADbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Almacenes>> GetAllAlmacenesAsync()
        {
            using (var context = new SGADbContext())
            {
                var almacenes = context.Almacenes.Include(a => a.IdPlantaNavigation).Include(u => u.Ubicaciones).Include(m => m.Movimientos).ToListAsync();
                return await almacenes;
            }
        }

        public IEnumerable<Almacenes> GetAllAlmacenes()
        {
            using (var context = new SGADbContext())
            {
                var almacenes = context.Almacenes.Include(a => a.IdPlantaNavigation).Include(u => u.Ubicaciones).Include(m => m.Movimientos).ToList();
                return almacenes;
            }
        }

        public Almacenes GetAlmacen(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Almacenes.FirstOrDefault(f => f.Id == id);
            }
        }

        public Almacenes GetAlmacenlByCodigoAlmacen(string codigoAlmacen)
        {
            using (var context = new SGADbContext())
            {
                return context.Almacenes.FirstOrDefault(a => a.CodigoAlmacen == codigoAlmacen);
            }
        }

        public Almacenes AddAlmacen(Almacenes almacen)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Almacenes.Add(almacen);
                context.SaveChanges();
                var id = entity.Entity.Id;
                return context.Almacenes.FirstOrDefault(f => f.Id == id);
            }
        }

        public void UpdateAlmacen(Almacenes almacen)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Almacenes>().Local.FirstOrDefault(x => x.Id == almacen.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(almacen).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteAlmacen(int id)
        {
            using (var context = new SGADbContext())
            {
                Almacenes almacen = context.Almacenes.Find(id);
                context.Almacenes.Remove(almacen);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
