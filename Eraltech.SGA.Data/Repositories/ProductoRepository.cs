﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class ProductoRepository : RepositoryBase<Productos>, IProductoRepository
    {
        public ProductoRepository(SGADbContext context)
            : base(context)
        { }

        public IEnumerable<Productos> GetAllProductos()
        {
            using (var context = new SGADbContext())
            {
                var Productos = context.Productos.Include(e => e.IdEspecialidadNavigation).Include(f => f.IdFamiliaNavigation).Include(m => m.IdMarcaNavigation)
                                                 //.Include(s => s.Stocks).ThenInclude(x => x.IdAlmacenNavigation)
                                                 //.Include(s => s.Stocks).ThenInclude(x => x.IdUbicacionNavigation)
                                                 //.Include(m => m.Movimientos).ThenInclude(x => x.IdTipoMovimientoNavigation)
                                                 //.Include(m => m.Movimientos).ThenInclude(x => x.IdAlmacenNavigation)
                                                 //.Include(m => m.Movimientos).ThenInclude(x => x.IdUbicacionNavigation)
                                                 //.Include(m => m.Movimientos).ThenInclude(x => x.IdProductoNavigation)
                                                 //.Include(m => m.Movimientos).ThenInclude(x => x.IdDispositivoNavigation)
                                                 //.Include(m => m.Movimientos).ThenInclude(x => x.IdZonaNavigation)
                                                 .Include(d => d.Dispositivos).ThenInclude(x => x.Stocks).ToList();
                return Productos;
            }
        }

        public Productos GetProductoByCodigoProducto(string codigoProducto)
        {
            using (var context = new SGADbContext())
            {
                return context.Productos.Include(e => e.IdEspecialidadNavigation).Include(f => f.IdFamiliaNavigation).Include(m => m.IdMarcaNavigation)
                                        .Include(s => s.Stocks).ThenInclude(x => x.IdAlmacenNavigation)
                                        .Include(s => s.Stocks).ThenInclude(x => x.IdUbicacionNavigation).FirstOrDefault(f => f.CodigoProducto == codigoProducto);
            }
        }

        public Productos GetProductoByNombre(string nombre)
        {
            using (var context = new SGADbContext())
            {
                return context.Productos.Include(e => e.IdEspecialidadNavigation).Include(f => f.IdFamiliaNavigation).Include(m => m.IdMarcaNavigation)
                                        .FirstOrDefault(f => f.Nombre == nombre);
            }
        }

        public Productos GetProducto(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Productos.Include(e => e.IdEspecialidadNavigation).Include(f => f.IdFamiliaNavigation).Include(m => m.IdMarcaNavigation)
                                        .Include(s => s.Stocks).ThenInclude(x => x.IdAlmacenNavigation)
                                        .Include(s => s.Stocks).ThenInclude(x => x.IdUbicacionNavigation).FirstOrDefault(f => f.Id == id);
            }
        }

        public Productos AddProducto(Productos Producto)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Productos.Add(Producto);
                context.SaveChanges();
                var id = entity.Entity.Id;
                return context.Productos.FirstOrDefault(f => f.Id == id);
            }
        }

        public Productos AddProductoWithTransaction(Productos Producto, SGADbContext context, bool identityOnOff = true)
        {
            var entity = context.Productos.Add(Producto);

            if (identityOnOff)
            {
                context.Database.OpenConnection();
                context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Productos ON");
                context.SaveChanges();
                context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Productos OFF");
                context.Database.CloseConnection();
            }
            else
                context.SaveChanges();

            var id = entity.Entity.Id;
            return context.Productos.FirstOrDefault(f => f.Id == id);
        }

        public void UpdateProducto(Productos Producto)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Productos>().Local.FirstOrDefault(x => x.Id == Producto.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(Producto).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void UpdateProductoWithTransaction(Productos Producto, SGADbContext context)
        {
            var local = context.Set<Productos>().Local.FirstOrDefault(x => x.Id == Producto.Id);
            if (local != null)
                context.Entry(local).State = EntityState.Detached;

            context.Entry(Producto).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void DeleteProducto(int id)
        {
            using (var context = new SGADbContext())
            {
                Productos Producto = context.Productos.Find(id);
                context.Productos.Remove(Producto);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
