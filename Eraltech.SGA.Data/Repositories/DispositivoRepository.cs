﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class DispositivoRepository : RepositoryBase<Dispositivos>, IDispositivoRepository
    {
        public DispositivoRepository(SGADbContext context)
            : base(context)
        { }

        public async Task<IEnumerable<Dispositivos>> GetAllDispositivosAsync()
        {
            using (var context = new SGADbContext())
            {
                var Dispositivos = context.Dispositivos.Include(p => p.IdProductoNavigation)
                                                       .Include(s => s.Stocks).ThenInclude(x => x.IdAlmacenNavigation)
                                                       .Include(s => s.Stocks).ThenInclude(x => x.IdUbicacionNavigation)
                                                       .Include(m => m.Movimientos).ThenInclude(x => x.IdTipoMovimientoNavigation)
                                                       .Include(m => m.Movimientos).ThenInclude(x => x.IdAlmacenNavigation)
                                                       .Include(m => m.Movimientos).ThenInclude(x => x.IdUbicacionNavigation)
                                                       .Include(m => m.Movimientos).ThenInclude(x => x.IdProductoNavigation)
                                                       .Include(m => m.Movimientos).ThenInclude(x => x.IdDispositivoNavigation)
                                                       .Include(m => m.Movimientos).ThenInclude(x => x.IdZonaNavigation)
                                                       .ToListAsync();
                return await Dispositivos;
            }
        }

        public Dispositivos GetDispositivoBySerialNumber(string serialNumber)
        {
            using (var context = new SGADbContext())
            {
                return context.Dispositivos.Include(s => s.Stocks).Include(p => p.IdProductoNavigation).FirstOrDefault(d => d.SerialNumber == serialNumber);
            }
        }

        public Dispositivos GetDispositivo(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Dispositivos.Include(p => p.IdProductoNavigation).FirstOrDefault(d => d.Id == id);
            }
        }

        public Dispositivos AddDispositivo(Dispositivos Dispositivo)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Dispositivos.Add(Dispositivo);
                context.SaveChanges();
                var id = entity.Entity.Id;
                return context.Dispositivos.FirstOrDefault(d => d.Id == id);
            }
        }

        public Dispositivos AddDispositivoWithTransaction(Dispositivos Dispositivo, SGADbContext context)
        {
            var entity = context.Dispositivos.Add(Dispositivo);
            context.SaveChanges();
            var id = entity.Entity.Id;
            return context.Dispositivos.FirstOrDefault(d => d.Id == id);
        }

        public void UpdateDispositivo(Dispositivos Dispositivo)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Dispositivos>().Local.FirstOrDefault(x => x.Id == Dispositivo.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(Dispositivo).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void UpdateDispositivoWithTransaction(Dispositivos Dispositivo, SGADbContext context)
        {
            var local = context.Set<Dispositivos>().Local.FirstOrDefault(x => x.Id == Dispositivo.Id);
            if (local != null)
                context.Entry(local).State = EntityState.Detached;

            context.Entry(Dispositivo).State = EntityState.Modified;
            context.SaveChanges();
        }

        public void DeleteDispositivo(int id)
        {
            using (var context = new SGADbContext())
            {
                Dispositivos Dispositivo = context.Dispositivos.Find(id);
                context.Dispositivos.Remove(Dispositivo);
                context.SaveChanges();
            }
        }

        public List<DispositivosNissan> GetDispositivosNissan()
        {
            using (var context = new SGADbContext())
            {
                var dispositivosNissan = context.DispositivosNissan.ToList();
                return dispositivosNissan;
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
