﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using System.Linq;

namespace Eraltech.SGA.Data.Repositories
{
    public class MarcaRepository : RepositoryBase<Marcas>, IMarcaRepository
    {
        public MarcaRepository(SGADbContext context)
            : base(context)
        { }

        public IEnumerable<Marcas> GetAllMarcas()
        {
            using (var context = new SGADbContext())
            {
                var marcas = context.Marcas.Include(p => p.Productos).ThenInclude(x => x.IdEspecialidadNavigation)
                                           .Include(p => p.Productos).ThenInclude(x => x.IdFamiliaNavigation)
                                           .Include(p => p.Productos).ThenInclude(x => x.IdMarcaNavigation)
                                           .ToList();
                return marcas;
            }
        }

        public Marcas GetMarca(int id)
        {
            using (var context = new SGADbContext())
            {
                return context.Marcas.FirstOrDefault(f => f.Id == id);
            }
        }

        public Marcas AddMarca(Marcas marca, bool identityOnOff = true)
        {
            using (var context = new SGADbContext())
            {
                var entity = context.Marcas.Add(marca);

                if (identityOnOff)
                { 
                    context.Database.OpenConnection();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Marcas ON");
                    context.SaveChanges();
                    context.Database.ExecuteSqlCommand("SET IDENTITY_INSERT dbo.Marcas OFF");
                    context.Database.CloseConnection();
                }
                else
                    context.SaveChanges();

                var id = entity.Entity.Id;
                return context.Marcas.FirstOrDefault(f => f.Id == id);
            }
        }

        public void UpdateMarca(Marcas marca)
        {
            using (var context = new SGADbContext())
            {
                var local = context.Set<Marcas>().Local.FirstOrDefault(x => x.Id == marca.Id);
                if (local != null)
                        context.Entry(local).State = EntityState.Detached;

                context.Entry(marca).State = EntityState.Modified;
                context.SaveChanges();
            }
        }

        public void DeleteMarca(int id)
        {
            using (var context = new SGADbContext())
            {
                Marcas marca = context.Marcas.Find(id);
                context.Marcas.Remove(marca);
                context.SaveChanges();
            }
        }

        private SGADbContext SGADbContext
        {
            get { return Context as SGADbContext; }
        }
    }
}
