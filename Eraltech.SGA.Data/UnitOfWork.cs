using System.Threading.Tasks;
using Eraltech.SGA.Core;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Core.Repositories;
using Eraltech.SGA.Data.Repositories;

namespace Eraltech.SGA.Data
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly SGADbContext _context;
        private FabricaRepository _fabricaRepository;
        private PlantaRepository _plantaRepository;
        private AlmacenRepository _almacenRepository;
        private UbicacionRepository _ubicacionRepository;
        private EspecialidadRepository _especialidadRepository;
        private FamiliaRepository _familiaRepository;
        private MarcaRepository _marcaRepository;
        private ProductoRepository _productoRepository;
        private StockRepository _stockRepository;
        private DispositivoRepository _dispositivoRepository;
        private ZonaRepository _zonaRepository;
        private MovimientoRepository _movimientoRepository;
        private MovementTemplateRepository _movementTemplateRepository;
        private AlmacenUsuarioRepository _almacenUsuarioRepository;       

        public UnitOfWork(SGADbContext context)
        {
            this._context = context;
        }

        public IFabricaRepository Fabricas => _fabricaRepository = _fabricaRepository ?? new FabricaRepository(_context);
        public IPlantaRepository Plantas => _plantaRepository = _plantaRepository ?? new PlantaRepository(_context);
        public IAlmacenRepository Almacenes => _almacenRepository = _almacenRepository ?? new AlmacenRepository(_context);
        public IUbicacionRepository Ubicaciones => _ubicacionRepository = _ubicacionRepository ?? new UbicacionRepository(_context);
        public IEspecialidadRepository Especialidades => _especialidadRepository = _especialidadRepository ?? new EspecialidadRepository(_context);
        public IFamiliaRepository Familias => _familiaRepository = _familiaRepository ?? new FamiliaRepository(_context);
        public IMarcaRepository Marcas => _marcaRepository = _marcaRepository ?? new MarcaRepository(_context);
        public IProductoRepository Productos => _productoRepository = _productoRepository ?? new ProductoRepository(_context);
        public IStockRepository Stocks => _stockRepository = _stockRepository ?? new StockRepository(_context);
        public IDispositivoRepository Dispositivos => _dispositivoRepository = _dispositivoRepository ?? new DispositivoRepository(_context);
        public IMovimientoRepository Movimientos => _movimientoRepository = _movimientoRepository ?? new MovimientoRepository(_context);
        public IZonaRepository Zonas => _zonaRepository = _zonaRepository ?? new ZonaRepository(_context);
        public IMovementTemplateRepository MovementTemplates => _movementTemplateRepository = _movementTemplateRepository ?? new MovementTemplateRepository(_context);
        public IAlmacenUsuarioRepository AlmacenesUsuarios => _almacenUsuarioRepository = _almacenUsuarioRepository ?? new AlmacenUsuarioRepository(_context);

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}