using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Eraltech.SGA.Core.Models;

namespace Eraltech.SGA.Data.Configurations
{
    public class FabricatConfiguration : IEntityTypeConfiguration<Fabricas>
    {
        public void Configure(EntityTypeBuilder<Fabricas> builder)
        {
            builder
                .HasKey(a => a.Id);

            builder
                .Property(m => m.Id)
                .UseIdentityColumn();
                
            builder
                .Property(m => m.Descripcion)
                .IsRequired()
                .HasMaxLength(100);

            builder
                .ToTable("Fabricas");
        }
    }
}