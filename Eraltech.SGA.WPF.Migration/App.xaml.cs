﻿using Eraltech.Login;
using Eraltech.Login.Models;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Migration;
using Eraltech.SGA.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using REMO.Almacen.Application.Service.Services.Especialidades.Contract;
using REMO.Almacen.Application.Service.Services.Especialidades.Implementation;
using REMO.Almacen.Application.Service.Services.Familias.Contract;
using REMO.Almacen.Application.Service.Services.Familias.Implementation;
using REMO.Almacen.Application.Service.Services.Images.Contract;
using REMO.Almacen.Application.Service.Services.Images.Implementation;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Log.Implementation;
using REMO.Almacen.Application.Service.Services.Marcas.Contract;
using REMO.Almacen.Application.Service.Services.Marcas.Implementation;
using REMO.Almacen.Application.Service.Services.Movimientos.Contract;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Contract;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Implementation;
using REMO.Almacen.Application.Service.Services.Ubicaciones.Contract;
using REMO.Almacen.Application.Service.Services.Ubicaciones.Implementation;
using REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Contrat;
using REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.MovimientosFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.MovimientosFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.UbicacionFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.UbicacionFactory.Implementation;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Implementation;
using REMO.Movimientos.Application.Service.Services.MovimientosSearch.Implementation;
using System;
using System.IO;
using System.Windows;

namespace Eraltech.SGA.WPF.Migration
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public IServiceProvider ServiceProvider { get; private set; }

        public IConfiguration Configuration { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            var builder = new ConfigurationBuilder()
             .SetBasePath(Directory.GetCurrentDirectory())
             .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true);

            Configuration = builder.Build();

            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            ServiceProvider = serviceCollection.BuildServiceProvider();

            var mainWindow = ServiceProvider.GetRequiredService<MainWindow>();
            mainWindow.Show();
        }

        private void ConfigureServices(IServiceCollection services)
        {
            Eraltech.SGA.Core.Helper.ConnectionString = Configuration.GetConnectionString("SGA");
            services.AddDbContext<SGADbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("SGA")));
            services.AddTransient(typeof(MainWindow));
            services.Configure<AppSettings>
                    (Configuration.GetSection(nameof(AppSettings)));

            services.Configure<ConnectionStrings>
                    (Configuration.GetSection(nameof(ConnectionStrings)));

            // REMO
            services.AddScoped<IUnitOfWork>(x => new UnitOfWork(Configuration.GetConnectionString("REMO")));
            services.AddScoped<ILogService, LogService>();
            services.AddScoped<IMigration, Eraltech.SGA.Migration.Migration>();
            services.AddScoped<DatabaseOrigen>();
            services.AddScoped<IUbicacionesFactory, UbicacionesFactory>();
            services.AddScoped<IUbicacionesService, UbicacionesService>();
            services.AddScoped<IEspecialidadesFactory, EspecialidadesFactory>();
            services.AddScoped<IEspecialidadesService, EspecialidadesService>();
            services.AddScoped<IFamiliasFactory, FamiliasFactory>();
            services.AddScoped<IFamiliasService, FamiliasService>();
            services.AddScoped<IMarcasFactory, MarcasFactory>();
            services.AddScoped<IMarcasService, MarcasService>();
            services.AddScoped<IImagesService, ImagesService>();
            services.AddScoped<IRecambiosAlmacenFactory, RecambiosAlmacenFactory>();
            services.AddScoped<IRecambiosAlmacenService, RecambiosAlmacenService>();
            services.AddScoped<IMovimientosFactory, MovimientosFactory>();
            services.AddScoped<IMovimientosService, MovimientosService>();

            Eraltech.Login.Models.Helper.ConnectionString = Configuration.GetConnectionString("REMO");
            services.AddDbContext<LoginDbContext>(options => options.UseSqlServer(Configuration.GetConnectionString("REMO")));
            services.AddScoped<IUserManagement, UserManagement>();

            // SGA
            services.AddScoped<Eraltech.SGA.Core.IUnitOfWork, Eraltech.SGA.Data.UnitOfWork>();
            services.AddScoped<UbicacionService>();
            services.AddScoped<EspecialidadService>();
            services.AddScoped<FamiliaService>();
            services.AddScoped<MarcaService>();
            services.AddScoped<StockService>();
            services.AddScoped<MovimientoService>();
            services.AddScoped<ProductoService>();
            services.AddScoped<DispositivoService>();
            services.AddScoped<DatabaseDestino>();
        }
    }
}
