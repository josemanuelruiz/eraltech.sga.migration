﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eraltech.SGA.WPF.Migration
{
    public class AppSettings
    {
        public string StringSetting { get; set; }

        public int IntegerSetting { get; set; }

        public bool BooleanSetting { get; set; }
    }
}
