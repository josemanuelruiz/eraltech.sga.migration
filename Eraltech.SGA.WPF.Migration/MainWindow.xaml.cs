﻿using Eraltech.SGA.Migration;
using Microsoft.Extensions.Options;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Threading;

namespace Eraltech.SGA.WPF.Migration
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IDisposable, INotifyPropertyChanged
    {
        private readonly IMigration _migration;
        private readonly AppSettings _settings;
        private readonly ConnectionStrings _connectionStrings;

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private string _message;
        public string Message
        {
            get { return _message; }
            set
            {
                if (value != _message)
                {
                    _message = value;
                    OnPropertyChanged("Message");
                }
            }
        }

        public MainWindow(IMigration migration,
                          IOptions<AppSettings> settings,
                          IOptions<ConnectionStrings> connectionStrings)
        {
            InitializeComponent();

            this._migration = migration;
            this._settings = settings.Value;
            this._connectionStrings = connectionStrings.Value;
            this._migration.SendMessageHandler += MigrationSendedMessage;
        }

        public void Dispose()
        {
            this._migration.SendMessageHandler -= MigrationSendedMessage;
        }

        protected void MigrationSendedMessage(object sender, OnSendMessageEventArgs e)
        {
            if (e.Count > 0)
            {
                this.Message += $"{DateTime.Now} [INFO] Se han procesado {e.Count} registros\n";
                if (this.pbarMigration.Value < 10)
                    this.pbarMigration.Value++;
            }

            this.Message += $"{DateTime.Now} {e.Message}\n";

            messageTextBox.Text = this.Message;
            DoEvents();
        }

        private void btnMigration_Click(object sender, RoutedEventArgs e)
        {
            this.pbarMigration.Maximum = 10;
            this.pbarMigration.Value = 0;
               
            this.Message += $"{DateTime.Now} [INFO] Iniciando la migración de REMO a SGA.\n";
            this.Message += $"{DateTime.Now} [INFO] Base de datos ORIGEN: '{_connectionStrings.REMO}'\n";
            this.Message += $"{DateTime.Now} [INFO] Base de datos DESTINO: '{_connectionStrings.SGA}'\n";
            messageTextBox.Text = this.Message;
            DoEvents();
            _migration.Start();

            if (this.Message.Contains("[ERROR]"))
                this.Message += $"{DateTime.Now} [ERROR] Migración finalizada con errores.\n";
            else
                this.Message += $"{DateTime.Now} [INFO] Migración finalizada satisfactoriamente.\n";

            messageTextBox.Text = this.Message;
            DoEvents();
        }

        public static void DoEvents()
        {
            Dispatcher.CurrentDispatcher.Invoke(DispatcherPriority.Background,
                    new EmptyDelegate(delegate { }));
        }

        private delegate void EmptyDelegate();

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
