﻿using Eraltech.Login;
using Eraltech.Login.Models;
using REMO.Almacen.Application.Service.Services.Especialidades.Contract;
using REMO.Almacen.Application.Service.Services.Familias.Contract;
using REMO.Almacen.Application.Service.Services.Marcas.Contract;
using REMO.Almacen.Application.Service.Services.Movimientos.Contract;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Contract;
using REMO.Almacen.Application.Service.Services.Ubicaciones.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Eraltech.SGA.Migration
{
    public class DatabaseOrigen
    {
        private static string ClassName => System.Reflection.MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly IUbicacionesService _UbicacionesService;
        private readonly IEspecialidadesService _EspecialidadesService;
        private readonly IFamiliasService _FamiliasService;
        private readonly IMarcasService _MarcasService;
        private readonly IRecambiosAlmacenService _ProductosService;
        private readonly IMovimientosService _MovimientosService;
        private readonly IUserManagement _UserManagement;

        public string ErrorMessage { get; private set; }

        public DatabaseOrigen(IUbicacionesService UbicacionesService,
                              IFamiliasService FamiliasService,
                              IEspecialidadesService EspecialidadesService,
                              IMarcasService MarcasService,
                              IRecambiosAlmacenService ProductosService,
                              IMovimientosService MovimientosService,
                              IUserManagement UserManagement)
        {
            _UbicacionesService = UbicacionesService;
            _FamiliasService = FamiliasService;
            _EspecialidadesService = EspecialidadesService;
            _MarcasService = MarcasService;
            _ProductosService = ProductosService;
            _MovimientosService = MovimientosService;
            _UserManagement = UserManagement;
        }

        public List<UbicacionDTO> GetUbicaciones()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                 var resultList = _UbicacionesService.GetUbicaciones();

                List<UbicacionDTO> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<EspecialidadesDTO> GetEspecialidades()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var resultList = _EspecialidadesService.GetEspecialidades();

                List<EspecialidadesDTO> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<FamiliasDTO> GetFamilias()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var resultList = _FamiliasService.GetFamilias();

                List<FamiliasDTO> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<MarcasDTO> GetMarcas()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var resultList = _MarcasService.GetMarcas();

                List<MarcasDTO> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<RecambiosAlmacenDTO> GetProductos()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var resultList = _ProductosService.GetRecambiosAlmacen();

                List<RecambiosAlmacenDTO> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<MovimientosDTO> GetMovimientos()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var resultList = _MovimientosService.GetMovimientos();

                List<MovimientosDTO> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<Roles> GetRoles()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var resultList = _UserManagement.CargarRoles();

                List<Roles> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<Usuarios> GetUsuarios()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var resultList = _UserManagement.CargarUsuarios(includeAlmacenes:false);

                List<Usuarios> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<Permisos> GetPermisos()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var resultList = _UserManagement.CargarPermisos();

                List<Permisos> resultado = resultList.ToList();

                return resultado;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        public List<PermisosRoles> GetPermisosRoles()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                List<PermisosRoles> permisosRoles = new List<PermisosRoles>();

                var resultList = _UserManagement.CargarRoles().ToList();
                if (resultList != null)
                {
                    resultList.ForEach(rol =>
                    {
                        if (rol.PermisosRoles != null)
                        {
                            rol.PermisosRoles.ToList().ForEach(p =>
                            {
                                permisosRoles.Add(new PermisosRoles
                                {
                                    IdRol = p.IdRol,
                                    IdPermiso = p.IdPermiso
                                });
                            });
                        }
                    });
                }

                return permisosRoles;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                return null;
            }
        }

        //public List<Accesos> GetAccesos()
        //{
        //    var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
        //    try
        //    {
        //        var resultList = _UserManagement.CargarAccesos();

        //        List<Accesos> resultado = resultList.ToList();

        //        return resultado;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
        //        return null;
        //    }
        //}
    }
}
