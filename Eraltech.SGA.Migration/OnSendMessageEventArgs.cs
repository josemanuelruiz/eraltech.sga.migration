﻿using System;

namespace Eraltech.SGA.Migration
{
    public class OnSendMessageEventArgs : EventArgs
    {
        public int Count { get; set; }
        public string Message { get; set; }
    }
}
