﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Eraltech.SGA.Migration
{
    public interface IMigration
    {
        event EventHandler<OnSendMessageEventArgs> SendMessageHandler;
        bool Start();
        bool Especialidades();
        bool Familias();
        bool Marcas();
    }
}
