﻿using Eraltech.Login;
using Eraltech.Login.Models;
using Eraltech.SGA.Core.Models;
using Eraltech.SGA.Services;
using Eraltech.SGA.Shared;
using Eraltech.SGA.Shared.Models.Dispositivo;
using Eraltech.SGA.Shared.Models.Especialidad;
using Eraltech.SGA.Shared.Models.Familia;
using Eraltech.SGA.Shared.Models.Marca;
using Eraltech.SGA.Shared.Models.Movimiento;
using Eraltech.SGA.Shared.Models.Producto;
using Eraltech.SGA.Shared.Models.Ubicacion;
using Microsoft.Extensions.Options;
using REMO.Almacen.Domain.Factory.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Eraltech.SGA.Migration
{
    public class DatabaseDestino
    {
        private const string username = "administrador";
        public string ErrorMessage { get; private set; }
        private static string ClassName => System.Reflection.MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly UbicacionService _UbicacionService;
        private readonly EspecialidadService _EspecialidadService;
        private readonly FamiliaService _FamiliaService;
        private readonly MarcaService _MarcaService;
        private readonly ProductoService _ProductoService;
        private readonly MovimientoService _MovimientoService;
        private readonly IUserManagement _UserManagement;
        private readonly ConnectionStrings _connectionStrings;
        private readonly DispositivoService _DispositivoService;

        public DatabaseDestino(UbicacionService UbicacionService,
                               EspecialidadService EspecialidadService,
                               FamiliaService FamiliaService,
                               MarcaService MarcaService,
                               ProductoService ProductoService,
                               MovimientoService MovimientoService,
                               IUserManagement UserManagement,
                               IOptions<ConnectionStrings> connectionStrings,
                               DispositivoService DispositivoService)
        {
            _UbicacionService = UbicacionService;
            _EspecialidadService = EspecialidadService;
            _FamiliaService = FamiliaService;
            _MarcaService = MarcaService;
            _ProductoService = ProductoService;
            _MovimientoService = MovimientoService;
            _UserManagement = UserManagement;
            _connectionStrings = connectionStrings.Value;
            _DispositivoService = DispositivoService;
        }

        public void CreateUbicacionDTO(UbicacionDTO u)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                UbicacionFormModel ubicacionFormModel = new UbicacionFormModel();
                ubicacionFormModel.Id = u.Id;
                ubicacionFormModel.IdAlmacen = u.IdAlmacen.ToString();
                ubicacionFormModel.CodigoUbicacion = "0000000";
                ubicacionFormModel.Descripcion = u.DescUbicacion;

                var responseData = _UbicacionService.CreateUbicacion(ubicacionFormModel);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        public void UpdateCodigoUbicacion()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var responseData = _UbicacionService.UpdateCodigoUbicacion();
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        public int CreateEspecialidad(EspecialidadesDTO e, bool identityOnOff = true)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                EspecialidadFormModel especialidadFormModel = new EspecialidadFormModel();
                especialidadFormModel.Id = e.Id;
                especialidadFormModel.Descripcion = e.Especialidad;

                var responseData = _EspecialidadService.CreateEspecialidad(especialidadFormModel, identityOnOff);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return 0;
                }
                else
                    return responseData.Object.Id;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }

            return 0;
        }

        public int CreateFamilia(FamiliasDTO f, bool identityOnOff = true)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                FamiliaFormModel familiaFormModel = new FamiliaFormModel();
                familiaFormModel.Id = f.Id;
                familiaFormModel.Descripcion = f.Familia;

                var responseData = _FamiliaService.CreateFamilia(familiaFormModel, identityOnOff);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return 0;
                }
                else
                    return responseData.Object.Id;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
            return 0;
        }

        public int CreateMarca(MarcasDTO f, bool identityOnOff = true)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                MarcaFormModel marcaFormModel = new MarcaFormModel();
                marcaFormModel.Id = f.Id;
                marcaFormModel.Descripcion = f.Marca;

                var responseData = _MarcaService.CreateMarca(marcaFormModel, identityOnOff);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return 0;
                }
                else
                    return responseData.Object.Id;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }

            return 0;
        }

        public void CreateProductoMigracion(RecambiosAlmacenDTO p)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                ProductoFormModel productoFormModel = new ProductoFormModel
                {
                    Id = p.Id,
                    CodigoProducto = p.CodigoAG,
                    Nombre = p.NombreRecambio,
                    Referencia = p.Referencia,
                    IdEspecialidad = p.Especialidad.Id.ToString(),
                    IdFamilia = p.Familia.Id.ToString(),
                    IdMarca = p.MArcaRecambio.Id.ToString(),
                    Observaciones = p.Observaciones,
                    Instalacion = p.Instalacion,
                    Precio = p.Precio,
                    Pedido = p.Pedido,
                    PerteneceKit = p.PerteneceKIT,
                    Seleccionado = p.Seleccionado,
                    SerialNumberRequired = false,
                    ImageUrl = !string.IsNullOrEmpty(p.ImageUrl) ? p.ImageUrl.Replace("/App_Images/", "") : string.Empty,
                    FechaAlta = DateTime.Now,
                };

                DateTime dateOut;
                if (IsDate(p.FechaPedido, out dateOut))
                    productoFormModel.FechaEntrega = dateOut;

                productoFormModel.Stock = new Shared.Models.Stock.StockFormModel
                {
                    Id = p.Id,
                    IdAlmacen = "1",
                    IdUbicacion = p.Ubicacion.Id.ToString(),
                    IdProducto = p.Id.ToString(),
                    CurrentStock = p.StockAlmacen,
                    StockMinimo = p.StockMinimo,
                    StockMaximo = p.StockMaximo,
                    BuenEstado = p.StockAlmacen,
                    MalEstado = 0,
                    PendienteClasificar = 0,
                    PendienteRecibir = 0,
                    DetalleUbicacion = p.Ubicacion.Description,
                };

                var responseData = _ProductoService.CreateProductoMigracion(productoFormModel, Shared.Enums.Zona.BuenEstado, username);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        private bool IsDate(string dateIn, out DateTime dateOut)
        {
            return DateTime.TryParse(dateIn, out dateOut);
        }

        /// <summary>
        /// Solo genera un movimiento de entrada o un movimientos de salida, NO de ajuste
        /// </summary>
        /// <param name="m"></param>
        public void CreateMovimientos(MovimientosDTO m)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                MovimientoFormModel movimientoFormModel = new MovimientoFormModel
                {
                    //Id = m.Id,
                    //Created = m.Created,
                    CreatedUser = username,
                    DetalleUbicacion = m.IdUbicacion.Description,
                    IdAlmacen = "1",
                    IdDispositivo = null,
                    IdProducto = m.IdRecambiosAlmacen.Id.ToString(),
                    IdTipoMovimiento = m.IdMovementTypes.Id.ToString(),
                    IdUbicacion = m.IdUbicacion.Id.ToString(),
                    IdZona = ((int)Enums.Zona.BuenEstado).ToString(),
                    //Stock = m.Stock,
                    Automatico = true
                };

                DateTime dateOut;
                switch (m.IdMovementTypes.Id)
                {
                    case (int)Enums.TipoMovimiento.Entrada:
                        if (IsDate(m.FechaRellenado, out dateOut))
                            movimientoFormModel.Created = dateOut;
                        movimientoFormModel.Stock = m.Repuestos;
                        break;

                    case (int)Enums.TipoMovimiento.Salida:
                        if (IsDate(m.FechaExtraccion, out dateOut))
                            movimientoFormModel.Created = dateOut;
                        movimientoFormModel.Stock = m.Extraidos;
                        break;

                    case 3: // Ajuste
                        ErrorMessage = $"[ERROR] {funcName}: Nunca debería de ser Ajuste '{m.IdMovementTypes.Id}'";
                        break;

                    default:
                        ErrorMessage = $"[ERROR] {funcName}: TipoMovimiento no existe '{m.IdMovementTypes.Id}'";
                        break;
                }

                var responseData = _MovimientoService.CreateMovimiento(movimientoFormModel, username);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return;
                }
                else
                {
                    var movimientoDTO = _MovimientoService.Create__Migracion_Movimientos_RMO_SGA(m, responseData.Object.Id);
                    if (!movimientoDTO.Ok)
                    {
                        ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        /// <summary>
        /// Genera un movimiento de entrada y otro movimientos de salida. Un movimiento de ajuste en REMO serán dos movimientos en SGA.
        /// </summary>
        /// <param name="m"></param>
        public void CreateMovimientosAjuste(MovimientosDTO m)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                // Se crear un movimento de entrada
                MovimientoFormModel movimientoFormModel = new MovimientoFormModel
                {
                    //Id = m.Id,
                    //Created = m.Created,
                    CreatedUser = username,
                    DetalleUbicacion = m.IdUbicacion.Description,
                    IdAlmacen = "1",
                    IdDispositivo = null,
                    IdProducto = m.IdRecambiosAlmacen.Id.ToString(),
                    IdTipoMovimiento = ((int)Enums.TipoMovimiento.Entrada).ToString(),
                    IdUbicacion = m.IdUbicacion.Id.ToString(),
                    IdZona = ((int)Enums.Zona.BuenEstado).ToString(),
                    //Stock = m.Stock,
                    Automatico = true
                };

                DateTime dateOut;
                if (IsDate(m.FechaRellenado, out dateOut))
                    movimientoFormModel.Created = dateOut;
                movimientoFormModel.Stock = m.Repuestos;

                var responseData = _MovimientoService.CreateMovimiento(movimientoFormModel, username);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return;
                }
                else
                {
                    var movimientoDTO = _MovimientoService.Create__Migracion_Movimientos_RMO_SGA(m, responseData.Object.Id);
                    if (!movimientoDTO.Ok)
                    {
                        ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                        return;
                    }
                }

                // Se crea un movimiento de salida
                movimientoFormModel = new MovimientoFormModel
                {
                    //Id = m.Id,
                    //Created = m.Created,
                    CreatedUser = username,
                    DetalleUbicacion = m.IdUbicacion.Description,
                    IdAlmacen = "1",
                    IdDispositivo = null,
                    IdProducto = m.IdRecambiosAlmacen.Id.ToString(),
                    IdTipoMovimiento = ((int)Enums.TipoMovimiento.Salida).ToString(),
                    IdUbicacion = m.IdUbicacion.Id.ToString(),
                    IdZona = ((int)Enums.Zona.BuenEstado).ToString(),
                    //Stock = m.Stock,
                    Automatico = true
                };

                if (IsDate(m.FechaExtraccion, out dateOut))
                    movimientoFormModel.Created = dateOut;
                movimientoFormModel.Stock = m.Extraidos;

                responseData = _MovimientoService.CreateMovimiento(movimientoFormModel, username);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return;
                }
                else
                {
                    var movimientoDTO = _MovimientoService.Create__Migracion_Movimientos_RMO_SGA(m, responseData.Object.Id);
                    if (!movimientoDTO.Ok)
                    {
                        ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        public void CreatePermiso(Login.Models.Permisos permiso)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                var temp = Helper.ConnectionString;
                Helper.ConnectionString = _connectionStrings.SGA;

                _UserManagement.CrearPermisoConId(permiso);

                Helper.ConnectionString = temp;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        public void CreateRol(Login.Models.Roles rol)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                var temp = Helper.ConnectionString;
                Helper.ConnectionString = _connectionStrings.SGA;

                rol.Usuarios = null;
                rol.PermisosRoles = null;
                _UserManagement.CrearRolConId(rol);

                Helper.ConnectionString = temp;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        public void CreatePermisoRol(Login.Models.PermisosRoles permisoRol)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                var temp = Helper.ConnectionString;
                Helper.ConnectionString = _connectionStrings.SGA;

                permisoRol.IdPermisoNavigation = null;
                permisoRol.IdRolNavigation = null;
                _UserManagement.CrearPermisoRolConId(permisoRol);

                Helper.ConnectionString = temp;

            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        public void CreateUsuario(Login.Models.Usuarios usuario)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            List<Login.Models.AlmacenesUsuarios> almacenesUsuarios = new List<Login.Models.AlmacenesUsuarios>();
            try
            {
                var temp = Helper.ConnectionString;
                Helper.ConnectionString = _connectionStrings.SGA;

                var user = usuario.IdRolNavigation = null;
                almacenesUsuarios.Add(new Login.Models.AlmacenesUsuarios { IdAlmacen = 1, Default = true });
                if (usuario.IdRol == 1)
                {
                    // Usuario con rol de Administrador
                    almacenesUsuarios.Add(new Login.Models.AlmacenesUsuarios { IdAlmacen = 2 });
                    almacenesUsuarios.Add(new Login.Models.AlmacenesUsuarios { IdAlmacen = 3 });
                    almacenesUsuarios.Add(new Login.Models.AlmacenesUsuarios { IdAlmacen = 4 });
                }
                usuario.AlmacenesUsuarios = almacenesUsuarios;

                _UserManagement.CrearUsuarioConId(usuario);

                Helper.ConnectionString = temp;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        #region Dispositivos Nissan

        internal List<DispositivosNissan> GetdDispositivosNissan()
        {
            var dispositivos = _DispositivoService.GetDispositivosNissan().ToList();
            dispositivos = dispositivos.Where(d => !string.IsNullOrEmpty(d.MODELO)).ToList();
            dispositivos = dispositivos.Where(d => !string.IsNullOrEmpty(d.N_SERIE?.TrimStart())).ToList();
            return dispositivos;
        }

        internal List<EspecialidadGridModel> GetEspecialidades()
        {
            var result = _EspecialidadService.GetAllEspecialidades();
            return result.ToList();
        }

        internal List<FamiliaGridModel> GetFamilias()
        {
            var result = _FamiliaService.GetAllFamilias();
            return result.ToList();
        }

        internal List<MarcaGridModel> GetMarcas()
        {
            var result = _MarcaService.GetAllMarcas();
            return result.ToList();
        }

        internal List<ProductoGridModel> GetProductos()
        {
            var result = _ProductoService.GetAllProductos();
            return result.ToList();

        }

        internal List<UbicacionGridModel> GetUbicaciones()
        {
            var result = _UbicacionService.GetAllUbicaciones();
            return result.ToList();
        }

        internal UbicacionFormModel GetUbicacionByCodigoUbicacion(string codigoUbicacion)
        {
            return _UbicacionService.GetUbicacionlByCodigoUbicacion(codigoUbicacion);
        }

        internal ProductoFormModel GetProductoByNombre(string nombre)
        {
            return _ProductoService.GetProductoByNombre(nombre);
        }

        public int CreateUbicacion(UbicacionFormModel ubicacionFormModel)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                var responseData = _UbicacionService.CreateUbicacion(ubicacionFormModel, false);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return 0;
                }
                else
                    return responseData.Object.Id;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }

            return 0;
        }

        //internal string CreateTramo(string tramo, ref int idAlmacen)
        //{
        //    throw new NotImplementedException();
        //}

        //internal string CreatePuestoMaquina(string puestoMaquina)
        //{
        //    throw new NotImplementedException();
        //}

        public int CreateProductoDispositivo(DispositivosNissan p, int idEspecialidad, int idFamilia, int idMarca, int idAlmacen, int idUbicacion)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                ProductoFormModel productoFormModel = new ProductoFormModel
                {
                    Nombre = p.MODELO,
                    Referencia = p.MODELO,
                    IdEspecialidad = idEspecialidad.ToString(),
                    IdFamilia = idFamilia.ToString(),
                    IdMarca = idMarca.ToString(),
                    Observaciones = "Producto creado automáticamente en la carga inicial de cotroladoras y herramientas",
                    Pedido = false,
                    PerteneceKit = false,
                    Seleccionado = false,
                    SerialNumberRequired = true,
                    FechaAlta = DateTime.Now,
                };

                productoFormModel.Stock = new Shared.Models.Stock.StockFormModel
                {
                    IdAlmacen = idAlmacen.ToString(),
                    IdUbicacion = idUbicacion.ToString(),
                    CurrentStock = 1,
                    StockMinimo = 0,
                    StockMaximo = 1,
                    BuenEstado = 1,
                    MalEstado = 0,
                    PendienteClasificar = 0,
                    PendienteRecibir = 0,
                    DetalleUbicacion = $"{p.TRAMO} - {p.PUESTO_MAQUINA}"
                };

                var responseData = _ProductoService.CreateProducto(productoFormModel, Shared.Enums.Zona.BuenEstado, username, false);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return 0;
                }
                else
                    return responseData.Object.Id;
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
            return 0;
        }

        public void CreateDispositivo(DispositivosNissan d, int idProducto, int idAlmacen, int idUbicacion)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                DispositivoFormModel dispositivoFormModel = new DispositivoFormModel
                {
                    SerialNumber = d.N_SERIE,
                    Descripcion = d.MODELO,
                    IdProducto = idProducto.ToString(),
                    FechaAlta = DateTime.Now,
                };

                dispositivoFormModel.Stock = new Shared.Models.Stock.StockFormModel
                {
                    IdAlmacen = idAlmacen.ToString(),
                    IdUbicacion = idUbicacion.ToString(),
                    CurrentStock = 1,
                    StockMinimo = 0,
                    StockMaximo = 1,
                    BuenEstado = 1,
                    MalEstado = 0,
                    PendienteClasificar = 0,
                    PendienteRecibir = 0,
                    DetalleUbicacion = $"{d.TRAMO} - {d.PUESTO_MAQUINA}"
                };

                var responseData = _DispositivoService.CreateDispositivo(dispositivoFormModel, Shared.Enums.Zona.BuenEstado, username, false);
                if (!responseData.Ok)
                {
                    ErrorMessage = $"[ERROR] {funcName}: {responseData.Message}";
                    return;
                }
            }
            catch (Exception ex)
            {
                ErrorMessage = $"[ERROR] {funcName}: { ex.Message}: { ex.InnerException?.Message}";
                Console.WriteLine(ErrorMessage);
            }
        }

        public string GetAlmacen(string tramo)
        {
            string codigoAlmacen = "CPE";
            switch(tramo)
            {
                case "":
                case "CHASIS L1":
                case "EPT L1":
                case "MULTIPLE RUEDAS L1":
                case "PLATFORM UF L1":
                case "PLATFORM UF1":
                case "PUERTAS L1":
                case "RING LINE UF L1":
                case "SUBCONJUNTOS L1":
                case "UF L1":
                case "VESTIDOS L1 ":
                    codigoAlmacen = "L600";
                    break;

                case "ABARCONES":
                case "ABARCONES PREPARADOS L2":
                case "BATTERY":
                case "BODY":
                case "CHASIS":
                case "LINEA":
                case "MOTORES":
                case "MÚLTIPLE RUEDAS L2":
                case "PREPARADOS L2":
                    codigoAlmacen = "L200";
                    break;

                default:
                    if (tramo?.Length > 2)
                    {
                        string linea = tramo.Substring(0, 2);
                        if (linea == "L2")
                            codigoAlmacen = "L200";
                        else if (linea == "L6")
                            codigoAlmacen = "L600";
                        else
                            codigoAlmacen = "CPE";
                    }
                    break;
            }
            return codigoAlmacen;
        }
        #endregion
    }
}
