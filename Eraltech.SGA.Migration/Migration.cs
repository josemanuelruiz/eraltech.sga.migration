﻿using Eraltech.SGA.Shared.Models.Especialidad;
using Eraltech.SGA.Shared.Models.Familia;
using Eraltech.SGA.Shared.Models.Marca;
using Eraltech.SGA.Shared.Models.Producto;
using Eraltech.SGA.Shared.Models.Ubicacion;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore.Internal;
using REMO.Almacen.Domain.Factory.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Eraltech.SGA.Migration
{
    public class Migration : IMigration
    {
        protected readonly DatabaseOrigen _databaseOrigen;
        protected readonly DatabaseDestino _databaseDestino;

        public Migration(DatabaseOrigen databaseOrigen,
                         DatabaseDestino databaseDestino)
        {
            _databaseOrigen = databaseOrigen;
            _databaseDestino = databaseDestino;
        }

        #region EventHandler

        protected virtual void OnSendMessage(OnSendMessageEventArgs e)
        {
            EventHandler<OnSendMessageEventArgs> handler = SendMessageHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        public event EventHandler<OnSendMessageEventArgs> SendMessageHandler;

        private void SendMessage(string message, int count = 0)
        {
            OnSendMessageEventArgs args = new OnSendMessageEventArgs();
            args.Message = message;
            args.Count = count;
            OnSendMessage(args);
        }

        #endregion

        public bool Start()
        {
            // Los datos de acceso no se migran, son accesos en REMO, para SGA se empieza de cero.
            // Accesos

            // En las siguientes tablas se aplica Script SET IDENTITY_INSERT MyTable ON para introducir el mismo Id que en REMO:
            // Familias
            // Especialidades
            // Marcas
            // Ubicaciones
            // Productos
            // Roles
            // Usuarios
            // Permisos
            // PermisosRoles

            // No migrar las siguientes tablas, se crean en SGA mediante script de carga inicial
            // Fabricas
            // Plantas
            // Almacenes

            if (!Ubicaciones())
                return false;

            if (!Especialidades())
                return false;

            if (!Familias())
                return false;

            if (!Marcas())
                return false;

            if (!Productos())
                return false;

            if (!Movimientos())
                return false;

            if (!Permisos())
                return false;

            if (!Roles())
                return false;

            if (!PermisosRoles())
                return false;

            if (!Usuarios())
                return false;

            // Los dispositivos se darán de alta en función de los datos del excel.
            if (!Dispositivos())
                return false;

            return true;
        }

        public bool Ubicaciones()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var ubicacionesREMO = _databaseOrigen.GetUbicaciones();

            if (ubicacionesREMO != null)
            {
                foreach (var u in ubicacionesREMO)
                {
                    _databaseDestino.CreateUbicacionDTO(u);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", ubicacionesREMO.Count);
                _databaseDestino.UpdateCodigoUbicacion();
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Especialidades()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var especialidadesREMO = _databaseOrigen.GetEspecialidades();

            if (especialidadesREMO != null)
            {
                foreach (var e in especialidadesREMO)
                {
                    _databaseDestino.CreateEspecialidad(e);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", especialidadesREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Familias()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var familiasREMO = _databaseOrigen.GetFamilias();

            if (familiasREMO != null)
            {
                foreach (var f in familiasREMO)
                {
                    _databaseDestino.CreateFamilia(f);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", familiasREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Marcas()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var marcasREMO = _databaseOrigen.GetMarcas();

            if (marcasREMO != null)
            {
                foreach (var m in marcasREMO)
                {
                    _databaseDestino.CreateMarca(m);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", marcasREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Productos()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var productosREMO = _databaseOrigen.GetProductos();

            if (productosREMO != null)
            {
                foreach (var p in productosREMO)
                {
                    _databaseDestino.CreateProductoMigracion(p);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", productosREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Movimientos()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var movimientosREMO = _databaseOrigen.GetMovimientos();

            if (movimientosREMO != null)
            {
                foreach (var m in movimientosREMO)
                {
                    if (m.IdMovementTypes.Id == 3)
                    {
                        _databaseDestino.CreateMovimientosAjuste(m);
                    }
                    else
                        _databaseDestino.CreateMovimientos(m);

                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", movimientosREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Usuarios()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var usuariosREMO = _databaseOrigen.GetUsuarios();

            if (usuariosREMO != null)
            {
                foreach (var u in usuariosREMO)
                {
                    _databaseDestino.CreateUsuario(u);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", usuariosREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Permisos()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var permisosREMO = _databaseOrigen.GetPermisos();

            if (permisosREMO != null)
            {
                foreach (var u in permisosREMO)
                {
                    _databaseDestino.CreatePermiso(u);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", permisosREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Roles()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var rolesREMO = _databaseOrigen.GetRoles();

            if (rolesREMO != null)
            {
                foreach (var rol in rolesREMO)
                {
                    _databaseDestino.CreateRol(rol);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", rolesREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool PermisosRoles()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            var permisosRolesREMO = _databaseOrigen.GetPermisosRoles();

            if (permisosRolesREMO != null)
            {
                foreach (var u in permisosRolesREMO)
                {
                    _databaseDestino.CreatePermisoRol(u);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", permisosRolesREMO.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        public bool Dispositivos()
        {
            bool result = true;

            SendMessage($"[INFO] Iniciando migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'");

            int idEspecialidad = GetIdEspecialidad();

            int idFamilia = GetIdFamilia();

            int idMarca = GetIdMarca();

            List<UbicacionGridModel> ubicaciones = _databaseDestino.GetUbicaciones();
            List<ProductoGridModel> productos = _databaseDestino.GetProductos();

            var dispositivos = _databaseDestino.GetdDispositivosNissan();

            if (dispositivos != null)
            {
                foreach (var dispositivo in dispositivos)
                {
                    // Crea una nueva marca, si el dispositivo la tiene informada
                    int idNuevaMarca = 0;
                    if (!string.IsNullOrEmpty(dispositivo.MARCA))
                    {
                        MarcasDTO marca = new MarcasDTO
                        {
                            Marca = dispositivo.MARCA,
                        };
                        idNuevaMarca = _databaseDestino.CreateMarca(marca, false);
                        if (idNuevaMarca > 0)
                            idMarca = idNuevaMarca;
                        if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                        {
                            result = false;
                            SendMessage($"{_databaseDestino.ErrorMessage}\n");
                            return false;
                        }
                    }

                    // Obtiene el código de almacén en SGA
                    string codigoAlmacen = _databaseDestino.GetAlmacen(dispositivo.TRAMO);
                    int idAlmacen = 0;
                    switch (codigoAlmacen)
                    {
                        case "CPE":
                            idAlmacen = 2;
                            break;
                        case "L600":
                            idAlmacen = 3;
                            break;
                        case "L200":
                            idAlmacen = 4;
                            break;
                    }

                    // Obtiene el código del tramo
                    string codigoTramo = dispositivo.TRAMO;
                    // Obtiene el código del puesto de máquina
                    string codigoPuestoMaquina = dispositivo.PUESTO_MAQUINA;

                    // Obtiene el código de ubicación y si no existe la ubicación en el almacén, se da de alta la nueva ubicación.
                    //string codigoUbicacion = idAlmacen.ToString().PadLeft(2,'0') + idTramo.ToString().PadLeft(2, '0') + idPuestoMaquina.ToString().PadLeft(3, '0');
                    string codigoUbicacion = $"{codigoAlmacen}-{codigoTramo}-{codigoPuestoMaquina}";

                    int idUbicacion = 0;
                    var ubicacion = _databaseDestino.GetUbicacionByCodigoUbicacion(codigoUbicacion);
                    if (ubicacion == null)
                    {
                        UbicacionFormModel ubicacionFormModel = new UbicacionFormModel
                        {
                            IdAlmacen = idAlmacen.ToString(),
                            CodigoUbicacion = codigoUbicacion,
                            Descripcion = $"{codigoTramo}-{codigoPuestoMaquina}",
                        };
                        idUbicacion = _databaseDestino.CreateUbicacion(ubicacionFormModel);
                        if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                        {
                            result = false;
                            SendMessage($"{_databaseDestino.ErrorMessage}\n");
                            return false;
                        }
                    }
                    else
                        idUbicacion = ubicacion.Id;

                    // Si no existe el producto por nombre, se crea el nuevo producto
                    var producto = _databaseDestino.GetProductoByNombre(dispositivo.MODELO);
                    int idProducto = 0;
                    if (producto == null)
                    {
                        idProducto = _databaseDestino.CreateProductoDispositivo(dispositivo, idEspecialidad, idFamilia, idMarca, idAlmacen, idUbicacion);
                        if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                        {
                            result = false;
                            SendMessage($"{_databaseDestino.ErrorMessage}\n");
                            return false;
                        }
                    }
                    else
                        idProducto = producto.Id;

                    _databaseDestino.CreateDispositivo(dispositivo, idProducto, idAlmacen, idUbicacion);
                    if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                    {
                        result = false;
                        SendMessage($"{_databaseDestino.ErrorMessage}\n");
                        return false;
                    }
                };
                SendMessage($"[INFO] Finalizada la migración de '{System.Reflection.MethodBase.GetCurrentMethod().Name}'", dispositivos.Count);
            }
            else
            {
                result = false;
                SendMessage($"{_databaseOrigen.ErrorMessage}\n");
            }

            return result;
        }

        private int GetIdEspecialidad()
        {
            int idEspecialidad = 0;

            List<EspecialidadGridModel> especialidades = _databaseDestino.GetEspecialidades();
            var especialidad = especialidades.FirstOrDefault(e => e.Descripcion == string.Empty);
            if (especialidad == null)
            {
                EspecialidadesDTO especialidadesDTO = new EspecialidadesDTO
                {
                    Especialidad = string.Empty
                };
                idEspecialidad = _databaseDestino.CreateEspecialidad(especialidadesDTO, false);
                if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                {
                    SendMessage($"{_databaseDestino.ErrorMessage}\n");
                    return -1;
                }
            }
            else
                idEspecialidad = especialidad.Id;

            return idEspecialidad;
        }

        private int GetIdFamilia()
        {
            int idFamilia = 0;

            List<FamiliaGridModel> familias = _databaseDestino.GetFamilias();
            var familia = familias.FirstOrDefault(f => f.Descripcion == string.Empty);
            if (familia == null)
            {
                FamiliasDTO familiaDTO = new FamiliasDTO
                {
                    Familia = string.Empty
                };
                idFamilia = _databaseDestino.CreateFamilia(familiaDTO, false);
                if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                {
                    SendMessage($"{_databaseDestino.ErrorMessage}\n");
                    return -1;
                }
            }
            else
                idFamilia = familia.Id;

            return idFamilia;
        }

        private int GetIdMarca()
        {
            int idMarca = 0;

            List <MarcaGridModel> marcas = _databaseDestino.GetMarcas();
            var marca = marcas.FirstOrDefault(f => f.Descripcion == string.Empty);
            if (marca == null)
            {
                MarcasDTO marcasDTO = new MarcasDTO
                {
                    Marca = string.Empty
                };
                idMarca = _databaseDestino.CreateMarca(marcasDTO, false);
                if (!string.IsNullOrEmpty(_databaseDestino.ErrorMessage))
                {
                    SendMessage($"{_databaseDestino.ErrorMessage}\n");
                    return -1;
                }
            }
            else
                idMarca = marca.Id;

            return idMarca;
        }
    }
}
