﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using REMO.Almacen.Application.Service.Services.Equipos.Contract;
using REMO.Almacen.Application.Service.Services.Equipos.Implementation;
using REMO.Almacen.Application.Service.Services.Lineas.Contract;
using REMO.Almacen.Application.Service.Services.Lineas.Implementation;
using REMO.Almacen.Domain.Factory.Factories.LineasFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.LineasFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.FasesFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.FasesFactory.Implementation;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Implementation;
using REMO.Almacen.Application.Service.Services.Fases.Implementation;
using REMO.Almacen.Application.Service.Services.Fases.Contract;
using REMO.Almacen.Application.Service.Services.Almacenes.Implementation;
using REMO.Almacen.Application.Service.Services.Almacenes.Contract;
using REMO.Almacen.Application.Service.Services.Especialidades.Implementation;
using REMO.Almacen.Application.Service.Services.Especialidades.Contract;
using REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Contrat;
using REMO.Almacen.Application.Service.Services.Marcas.Implementation;
using REMO.Almacen.Application.Service.Services.Marcas.Contract;
using REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Implementation;
using REMO.Almacen.Application.Service.Services.Operaciones.Contract;
using REMO.Almacen.Application.Service.Services.Operaciones.Implementation;
using REMO.Almacen.Domain.Factory.Factories.OperacionesFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.OperacionesFactory.Implementation;
using REMO.Almacen.Application.Service.Services.Familias.Implementation;
using REMO.Almacen.Application.Service.Services.Familias.Contract;
using REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Implementation;
using REMO.Almacen.Application.Service.Services.Layout.Implementation;
using REMO.Almacen.Application.Service.Services.Layout.Contract;
using REMO.Almacen.Domain.Factory.Factories.LayoutFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.LayoutFactory.Implementation;
using REMO.Almacen.Application.Service.Services.Planta.Implementation;
using REMO.Almacen.Application.Service.Services.Planta.Contract;
using REMO.Almacen.Domain.Factory.Factories.PlantaFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.PlantaFactory.Implementation;
using REMO.Movimientos.Application.Service.Services.MovimientosSearch.Implementation;
using REMO.Almacen.Application.Service.Services.Movimientos.Contract;
using REMO.Almacen.Domain.Factory.Factories.MovimientosFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.MovimientosFactory.Implementation;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Contract;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Implementation;
using REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Implementation;
using REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Contract;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacenSearch.Implementation;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacenSearch.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Log.Implementation;
using REMO.Almacen.Application.Service.Services.Ubicaciones.Implementation;
using REMO.Almacen.Application.Service.Services.Ubicaciones.Contract;
using REMO.Almacen.Domain.Factory.Factories.UbicacionFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.UbicacionFactory.Implementation;
using REMO.Almacen.Application.Service.Services.MovimientosSearch.Implementation;
using REMO.Almacen.Application.Service.Services.MovimientosSearch.Contract;
using REMO.Almacen.Application.Service.Services.Images.Contract;
using REMO.Almacen.Application.Service.Services.Images.Implementation;
using REMO.Almacen.Application.Service.Services.PrintLabel.Contract;
using REMO.Almacen.Application.Service.Services.PrintLabel.Implementation;

namespace REMO.Almacen.Application.Service.IoC
{

    public class NinjectDependencyResolver : IDependencyResolver
        {
            private readonly IKernel _kernel;

            public NinjectDependencyResolver()
            {
                _kernel = new StandardKernel();
                AddBindings();
            }

            public object GetService(Type serviceType)
            {
                return _kernel.TryGet(serviceType);
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                return _kernel.GetAll(serviceType);
            }

        private void AddBindings()
        {
            
            // Unit of work
             
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<LineasService>(); 
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<EquiposService>(); 
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<FasesService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<AlmacenesService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<EspecialidadesService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<MarcasService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<OperacionesService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<FamiliasService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<LayoutService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<PlantaService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<UbicacionesService>();

            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<MovimientosService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<RecambiosAlmacenService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<RecambiosAlmacenSearchService>(); 
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<MovimientosSearchService>(); 

            // Services
            _kernel.Bind<ILineasService>().To<LineasService>();
            _kernel.Bind<IEquiposService>().To<EquiposService>();
            _kernel.Bind<IFasesService>().To<FasesService>();
            _kernel.Bind<IAlmacenesService>().To<AlmacenesService>();
            _kernel.Bind<IEspecialidadesService>().To<EspecialidadesService>();
            _kernel.Bind<IMarcasService>().To<MarcasService>();
            _kernel.Bind<IOperacionesService>().To<OperacionesService>();
            _kernel.Bind<IFamiliasService>().To<FamiliasService>();
            _kernel.Bind<ILayoutService>().To<LayoutService>();
            _kernel.Bind<IPlantaService>().To<PlantaService>(); 
            _kernel.Bind<IUbicacionesService>().To<UbicacionesService>(); 

            _kernel.Bind<IMovimientosService>().To<MovimientosService>();
            _kernel.Bind<IRecambiosAlmacenService>().To<RecambiosAlmacenService>();
            _kernel.Bind<IRecambiosAlmacenSearchService>().To<RecambiosAlmacenSearchService>(); 
            _kernel.Bind<IMovimientosSearchService>().To<MovimientosSearchService>(); 

            // Factories
            _kernel.Bind<ILineasFactory>().To<LineasFactory>();
            _kernel.Bind<IEquiposFactory>().To<EquiposFactory>(); 
            _kernel.Bind<IFasesFactory>().To<FasesFactory>();
            _kernel.Bind<IAlmacenesFactory>().To<AlmacenesFactory>();
            _kernel.Bind<IMarcasFactory>().To<MarcasFactory>();
            _kernel.Bind<IOperacionesFactory>().To<OperacionesFactory>();
            _kernel.Bind<IFamiliasFactory>().To<FamiliasFactory>();
            _kernel.Bind<ILayoutFactory>().To<LayoutFactory>();
            _kernel.Bind<IPlantaFactory>().To<PlantaFactory>(); 
            _kernel.Bind<IUbicacionesFactory>().To<UbicacionesFactory>(); 

            _kernel.Bind<IMovimientosFactory>().To<MovimientosFactory>(); 
            _kernel.Bind<IRecambiosAlmacenFactory>().To<RecambiosAlmacenFactory>();

            //Helpers
            _kernel.Bind<ILogService>().To<LogService>();
            _kernel.Bind<IUnitOfWork>().To<UnitOfWork>().WhenInjectedInto<ILogService>();
            _kernel.Bind<IImagesService>().To<ImagesService>();
            _kernel.Bind<IPrintLabelService>().To<PrintLabelService>();



        }
    }
}

