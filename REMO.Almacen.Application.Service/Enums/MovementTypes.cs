﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REMO.Almacen.Application.Service.Enums
{
    public enum MovementTypes
    {
        Entrada = 1,
        Salida = 2,
        Ajuste = 3
    }
}
