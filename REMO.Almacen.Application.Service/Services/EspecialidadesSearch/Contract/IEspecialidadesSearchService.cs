﻿using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;


namespace REMO.Almacen.Application.Service.Services.EspecialidadesSearch.Contract
{
    public interface IEspecialidadesSearchService
    {
        IEnumerable<EspecialidadesDTO> FindEspecialidades(EspecialidadesDTO searchmodel);
    }
}
