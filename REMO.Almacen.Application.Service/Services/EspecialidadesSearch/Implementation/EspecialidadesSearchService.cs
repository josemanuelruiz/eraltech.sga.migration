﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Especialidades.Contract;
using REMO.Almacen.Application.Service.Services.EspecialidadesSearch.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.EspecialidadesSearch.Implementation
{
    public class EspecialidadesSearchService : IEspecialidadesSearchService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEspecialidadesService _EspecialidadesService;

        public EspecialidadesSearchService(IUnitOfWork unitOfWork, ILogService logService, IEspecialidadesService EspecialidadesService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _EspecialidadesService = EspecialidadesService;
        }

        public IEnumerable<EspecialidadesDTO> FindEspecialidades(EspecialidadesDTO searchmodel)
        {
            var funcName = ClassName + "." + MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);
          
                var res = (from t in (from g in (from l in _EspecialidadesService.GetEspecialidades().Where(i => i.DeletedReg.Equals(false)) select l).AsEnumerable()
                                      where
                                      !string.IsNullOrEmpty(searchmodel.Especialidad) && !string.IsNullOrEmpty(g.Especialidad) && g.Especialidad.ToUpper().Trim().Contains(searchmodel.Especialidad.ToUpper().Trim())
                                      select g)
                           group t by new { t.Id }
                             into mygroup
                           select mygroup.FirstOrDefault()).ToList().OrderBy(g => g.Especialidad);

                return res.AsQueryable();
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
