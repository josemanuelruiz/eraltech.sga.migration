﻿using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.Excel
{
    public interface IExcelHelper
    {
        string ExportToExcelRecAlma(string tablename, string filepath, List<RecambiosAlmacenDTO> recambiosalmacenlist);
        string ExportToExcelMovimientos(string tablename, string filepath, List<MovimientosDTO> movimientoslist);

    }
}
