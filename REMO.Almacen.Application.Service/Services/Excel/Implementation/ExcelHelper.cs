﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Log.Implementation;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.DTOs.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Excel.Implementation
{
    public class ExcelHelper : IExcelHelper
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        LogService _logService = new LogService();

         
        public string ExportToExcelRecAlma(string tablename, string filepath, List<RecambiosAlmacenDTO> recambiosalmacenlist)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);
             
         
            string fileExcel = string.Concat(tablename, "_", DateTime.Now.ToString("ddMMyyyy"), ".xlsx");
            string pathExcel = string.Concat(filepath, @"\", fileExcel);

            ExportToExcel(tablename, string.Concat(pathExcel), ToDataTable(recambiosalmacenlist));

            return fileExcel;

            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw;
            }
        }
        public string ExportToExcelMovimientos(string tablename, string filepath, List<MovimientosDTO> movimientoslist)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

                string fileExcel = string.Concat(tablename, "_", DateTime.Now.ToString("ddMMyyyy"), ".xlsx");
               string pathExcel = string.Concat(filepath, @"\", fileExcel);

            ExportToExcel(tablename, string.Concat(pathExcel), ToDataTable(movimientoslist));

            return fileExcel;
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw;
            }
        }

        private static DataTable ToDataTable(List<RecambiosAlmacenDTO> recambiosalmacenlist)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(RecambiosAlmacenExcelDTO));
                 DataTable table = new DataTable();
                    foreach (PropertyDescriptor prop in properties)
                        table.Columns.Add(string.Concat("[",prop.Name, "]"), Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

                    foreach (var item in recambiosalmacenlist)
                    {
                        var itemxls = new RecambiosAlmacenExcelDTO(item);
                        DataRow row = table.NewRow();
                        foreach (PropertyDescriptor prop in properties)
                            row[string.Concat("[", prop.Name, "]")] = prop.GetValue(itemxls) ?? DBNull.Value;
                        table.Rows.Add(row);
                    }
                    return table;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }

        }


        private static DataTable ToDataTable(List<MovimientosDTO> movimientoslist)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(MovimientosExcelDTO));
                DataTable table = new DataTable();
                foreach (PropertyDescriptor prop in properties)
                    table.Columns.Add(string.Concat("[", prop.Name, "]"), Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);

                foreach (var item in movimientoslist)
                {
                    var itemxls = new MovimientosExcelDTO(item);
                    DataRow row = table.NewRow();
                    foreach (PropertyDescriptor prop in properties)
                        row[string.Concat("[", prop.Name, "]")] = prop.GetValue(itemxls) ?? DBNull.Value;
                    table.Rows.Add(row);
                }
                return table;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message);
                }
        }

        private void ExportToExcel(string tablename,string filepath, DataTable table)
        {
          var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

          try
          {
              //_logService.InsertLogValid(
              //    "2",
              //    funcName,
              //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
              //    "",
              //    HttpContext.Current.User.Identity.Name);

                        using (SpreadsheetDocument document = SpreadsheetDocument.Create(filepath, SpreadsheetDocumentType.Workbook))
                        {
                                     WorkbookPart workbookPart = document.AddWorkbookPart();
                                     workbookPart.Workbook = new Workbook();

                                     WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                                     var sheetData = new SheetData();
                                     worksheetPart.Worksheet = new Worksheet(sheetData);

                                     Sheets sheets = workbookPart.Workbook.AppendChild(new Sheets());
                                     Sheet sheet = new Sheet() { Id = workbookPart.GetIdOfPart(worksheetPart), SheetId = 1, Name = tablename };

                                     sheets.Append(sheet);

                                     Row headerRow = new Row();

                                     List<String> columns = new List<string>();
                                      foreach (DataColumn column in table.Columns)
                                      {
                                          columns.Add(column.ColumnName);

                                          Cell cell = new Cell();
                                          cell.DataType = CellValues.String;
                                          cell.CellValue = new CellValue(column.ColumnName);
                                          headerRow.AppendChild(cell);
                         }

                          sheetData.AppendChild(headerRow);

                         foreach (DataRow dsrow in table.Rows)
                         {
                             Row newRow = new Row();
                             foreach (String col in columns)
                                  {
                                     Cell cell = new Cell();
                                     cell.DataType = CellValues.String;
                                     cell.CellValue = new CellValue(dsrow[col].ToString());
                                     newRow.AppendChild(cell);
                                 }

                                    sheetData.AppendChild(newRow);
                                 }

                               workbookPart.Workbook.Save();
                            }
            
             }
            catch (Exception ex)
            {
                   //_logService.InsertLogError("2",
                   //    funcName,
                   //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                   //    "",
                   //    ex,
                   //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }

           }
     }
}
 