﻿using REMO.Almacen.Application.Service.Services.Familias.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Familias.Implementation
{
    public class FamiliasService : IFamiliasService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFamiliasFactory _FamiliasFactory;

        public FamiliasService(IUnitOfWork unitOfWork, IFamiliasFactory FamiliasFactory, ILogService logService)
        {
           _logService = logService;   
           _unitOfWork = unitOfWork;
           _FamiliasFactory = FamiliasFactory;
        }


        IEnumerable<FamiliasDTO> IFamiliasService.GetFamilias()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                return from l in _unitOfWork.zSYS_M_FamiliasRepository.GetAll().OrderBy(g => g.Id).AsEnumerable()
                       select new FamiliasDTO(l);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
