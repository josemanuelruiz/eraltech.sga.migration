﻿using REMO.Almacen.Application.Service.Services.Layout.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.LayoutFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Layout.Implementation
{
    public class LayoutService : ILayoutService
    {

        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILayoutFactory _LayoutFactory;

        public LayoutService(IUnitOfWork unitOfWork, ILayoutFactory LayoutFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _LayoutFactory = LayoutFactory;
        }


        IEnumerable<LayoutDTO> ILayoutService.GetLayout()
        { var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

                return from l in _unitOfWork.zSYS_M_LayoutRepository.GetAll().OrderBy(g => g.Layout).AsEnumerable()
                   select new LayoutDTO(l);
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
