﻿using System;
using REMO.Almacen.Application.Service.Services.Marcas.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Marcas.Implementation
{
    public class MarcasService : IMarcasService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMarcasFactory _MarcasFactory;

        public MarcasService(IUnitOfWork unitOfWork, IMarcasFactory MarcasFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _MarcasFactory = MarcasFactory;
        }


        IEnumerable<MarcasDTO> IMarcasService.GetMarcas()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                return from l in _unitOfWork.zSYS_M_MarcasRepository.GetAll().OrderBy(g => g.Id).AsEnumerable()
                   select new MarcasDTO(l);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }
    }
}
