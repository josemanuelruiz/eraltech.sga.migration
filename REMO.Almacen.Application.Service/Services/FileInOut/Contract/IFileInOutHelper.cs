﻿using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.FileInOut
{
    public interface IFileInOutHelper
    {
          string CreateDir(string pathRoot);
          void DeleteDir(string pathRoot);
          string ExistsDir(string pathRoot);
          void Createfile(string pathRoot, string filename);
          void Deletefiles(string pathRoot);
          List<string> ReadfileData(string filename);
          void WritefileData(string filename,string Data);
          bool Existfile(string path, string filename);
      
    }
}
