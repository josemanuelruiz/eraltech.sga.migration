﻿using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.FileInOut
{
    public interface IFileInOutHelperExtended
    {
        void DeleteOlderfiles(string pathRoot, int NumDays);
    }
}
