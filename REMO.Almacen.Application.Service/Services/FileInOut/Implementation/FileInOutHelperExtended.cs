﻿using System;
using System.IO;

namespace REMO.Almacen.Application.Service.Services.FileInOut.Implementation
{
    public class FileInOutHelperExtended : IFileInOutHelperExtended
    {
        private static string ClassName => System.Reflection.MethodBase.GetCurrentMethod().DeclaringType?.Name;
        
        public FileInOutHelperExtended()
        {
        }

        public void DeleteOlderfiles(string pathRoot, int NumDays)
        {
            TimeSpan tsFile;

            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                if (!(string.IsNullOrEmpty(pathRoot)))
                {
                    var files = Directory.GetFiles(pathRoot);
                    foreach(string file in files)
                    {
                        tsFile = DateTime.Now.Subtract(File.GetCreationTime(file));
                        if (tsFile.Days * 24 + tsFile.Hours > NumDays * 24)
                        {
                            File.Delete(file);
                        } 
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }
        }
    }
}
