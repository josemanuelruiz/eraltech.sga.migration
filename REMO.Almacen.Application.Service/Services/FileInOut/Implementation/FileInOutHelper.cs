﻿using REMO.Almacen.Application.Service.Services.DirManager.Implementation;
using REMO.Almacen.Application.Service.Services.FileManager.Implementation;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using REMO.Almacen.Application.Service.Services.Log.Implementation;
using REMO.Almacen.Application.Service.Services.DirManager.Contract;
using REMO.Almacen.Application.Service.Services.FileManager.Contract;

namespace REMO.Almacen.Application.Service.Services.FileInOut.Implementation
{
    public class FileInOutHelper : IFileInOutHelper
    {
        private static string ClassName => System.Reflection.MethodBase.GetCurrentMethod().DeclaringType?.Name;

        public string _ext { set; get; }
        private DirManagerHelper _Dm = new DirManagerHelper();
        private FileManagerHelper _Fm = new FileManagerHelper();

        public FileInOutHelper()
        {         
        }
        
        public string CreateDir(string pathRoot)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {      

                if ((_Dm != null) && !(string.IsNullOrEmpty(pathRoot)))
                {
                    _Dm.CreateDir(pathRoot);
                }
                return pathRoot;
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }

        }

        public void DeleteDir(string pathRoot)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
            
                if ((_Dm != null) && !(string.IsNullOrEmpty(pathRoot)))
                {
                    _Dm.DeleteDir(pathRoot);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }

        }

        public string ExistsDir(string pathRoot)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
       
                if ((_Dm != null) && !(string.IsNullOrEmpty(pathRoot)))
                {
                    return _Dm.ExistsDir(pathRoot).ToString();
                }
                else return "Not Exists Directory";
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }

        }


        public void Createfile(string pathRoot, string filename)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
 
                if ((_Dm != null) && !(string.IsNullOrEmpty(pathRoot)))
                {
                    if (!Existfile(pathRoot, filename))
                    {
                        _Fm.Createfile(pathRoot, filename);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }
        }

        public void Deletefiles(string pathRoot)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
 
                if ((_Fm != null) && (_Dm != null) && !(string.IsNullOrEmpty(pathRoot)))
                {
                    _Fm.Deletefiles(pathRoot);
                    _Dm.DeleteDir(pathRoot);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }

        }

        public List<string> ReadfileData(string filename)
        {

            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
 
                if ((_Fm != null) && !(string.IsNullOrEmpty(filename)))
                {
                    return _Fm.ReadfileData(filename);
                }
                else
                {
                    return new List<string>();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }


        }

        public void WritefileData(string filename, string Data)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                 _Fm.WritefileData(filename, Data);
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }
        }

        public bool Existfile(string path, string filename)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                List<string> files = Directory.GetFiles(path, @"*.*", SearchOption.AllDirectories).ToList();
                bool res = false;

                foreach (string item in files)
                {
                    if (Path.GetFileNameWithoutExtension(item) == Path.GetFileNameWithoutExtension(filename))
                    {
                        res = true;
                    }

                }
                return res;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0} Error: {1} ", funcName, ex.Message));
            }

        }
    }
}
