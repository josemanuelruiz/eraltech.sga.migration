﻿using REMO.Almacen.Application.Service.Services.Almacenes.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Almacenes.Implementation
{
    public class AlmacenesService : IAlmacenesService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IAlmacenesFactory _AlmacenesFactory;

        public AlmacenesService(IUnitOfWork unitOfWork, IAlmacenesFactory AlmacenesFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _AlmacenesFactory = AlmacenesFactory;
        }


        IEnumerable<AlmacenDTO> IAlmacenesService.GetAlmacenes()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

                return from a in _unitOfWork.zSYS_M_AlmacenesRepository.GetAll().OrderBy(g => g.Descripcion).Where(i => i.DeletedReg.Equals(false)).AsEnumerable()
                       join p in _unitOfWork.zSYS_M_PlantaRepository.GetAll().AsEnumerable() on a.IdPlanta equals p.Id
                       select new AlmacenDTO()
                       {
                           Id = a.Id,
                           CodigoAlmacen = a.CodigoAlmacen,
                           Descripcion = a.Descripcion,
                           Planta = new IdAndDescClass() { Id = p.Id, Description = p.Descripcion }
                       };
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
