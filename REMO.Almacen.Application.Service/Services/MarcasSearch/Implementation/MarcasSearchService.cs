﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Marcas.Contract;
using REMO.Almacen.Application.Service.Services.MarcasSearch.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.MarcasSearch.Implementation
{
    public class MarcasSearchService : IMarcasSearchService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMarcasService _MarcasService;

        public MarcasSearchService(IUnitOfWork unitOfWork, ILogService logService, IMarcasService MarcasService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _MarcasService = MarcasService;
        }

        public IEnumerable<MarcasDTO> FindMarcas(MarcasDTO searchmodel)
        {
            var funcName = ClassName + "." + MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);
          
                var res = (from t in (from g in (from l in _MarcasService.GetMarcas().Where(i => i.DeletedReg.Equals(false)) select l).AsEnumerable()
                                      where
                                      (!string.IsNullOrEmpty(searchmodel.Marca) && !string.IsNullOrEmpty(g.Marca) && g.Marca.ToUpper().Trim().Contains(searchmodel.Marca.ToUpper().Trim()) ||
                                      (!string.IsNullOrEmpty(searchmodel.Proveedor) && !string.IsNullOrEmpty(g.Proveedor) && g.Proveedor.ToUpper().Trim().Contains(searchmodel.Proveedor.ToUpper().Trim())))
                                      select g)
                           group t by new { t.Id }
                             into mygroup
                           select mygroup.FirstOrDefault()).ToList().OrderBy(g => g.Marca);

                return res.AsQueryable();
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
