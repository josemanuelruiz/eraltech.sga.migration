﻿using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;


namespace REMO.Almacen.Application.Service.Services.MarcasSearch.Contract
{
    public interface IMarcasSearchService
    {
        IEnumerable<MarcasDTO> FindMarcas(MarcasDTO searchmodel);
    }
}
