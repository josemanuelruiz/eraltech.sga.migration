﻿using REMO.Almacen.Application.Service.Services.Equipos.Contract;
using REMO.Almacen.Application.Service.Services.Fases.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Domain.Factory.Factories.EquipoFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Equipos.Implementation
{
    public class EquiposService : IEquiposService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEquiposFactory _EquiposFactory;
 

        public EquiposService(IUnitOfWork unitOfWork
                            , IEquiposFactory EquiposFactory
                            , ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _EquiposFactory = EquiposFactory;

        }


        IEnumerable<EquiposDTO> IEquiposService.GetEquipos()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

                return from l in _unitOfWork.zSYS_M_EquiposRepository.GetAll().OrderBy(g => g.Equipo).Where(i => i.DeletedReg.Equals(false)).AsEnumerable()
                       join f in _unitOfWork.zSYS_M_FasesRepository.GetAll().AsEnumerable() on  l.IdFase equals f.Id
                     select new EquiposDTO() { Id = l.Id,
                                              CodigoEquipo = l.CodigoEquipo,
                                              Equipo = l.Equipo,
                                              Descripcion = l.Descripcion,
                                              Trafo_Subequipo = l.Trafo_Subequipo,
                                              Fase = new IdAndDescClass() { Id = f.Id, Description = f.Fase },
                                              ProductoTrabajo = l.ProductoTrabajo,
                                              DeletedReg = l.DeletedReg,
            };
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
