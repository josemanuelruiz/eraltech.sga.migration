﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Planta.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.PlantaFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Planta.Implementation
{
    public class PlantaService : IPlantaService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IPlantaFactory _PlantaFactory;

        public PlantaService(IUnitOfWork unitOfWork, IPlantaFactory PlantaFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _PlantaFactory = PlantaFactory;
        }


        IEnumerable<PlantaDTO> IPlantaService.GetPlanta()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

           return from l in _unitOfWork.zSYS_M_PlantaRepository.GetAll().OrderBy(g => g.Descripcion).Where(i => i.DeletedReg.Equals(false)).AsEnumerable()
                   select new PlantaDTO(l);

            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
