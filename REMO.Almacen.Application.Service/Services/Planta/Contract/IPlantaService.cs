﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.Planta.Contract
{
    public interface IPlantaService
    {
        IEnumerable<PlantaDTO> GetPlanta();
    }
}

