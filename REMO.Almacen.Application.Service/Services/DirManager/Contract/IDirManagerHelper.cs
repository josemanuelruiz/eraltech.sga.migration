﻿namespace REMO.Almacen.Application.Service.Services.DirManager.Contract
{
    public interface IDirManagerHelper
    {
        string CreateDir(string pathroot);
        void DeleteDir(string pathRoot);
        bool ExistsDir(string path);
    }
}
