﻿using REMO.Almacen.Application.Service.Services.DirManager.Contract;
using System;
using System.IO;

namespace REMO.Almacen.Application.Service.Services.DirManager.Implementation
{
   public class DirManagerHelper : IDirManagerHelper
    {

            public DirManagerHelper()
            {

            }

            public string CreateDir(string pathroot)
            {
                try
                {
                    Directory.CreateDirectory(pathroot);
                    return pathroot;
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("{0}", ex.Message));
                }

            }

            public void DeleteDir(string pathRoot)
            {
                try
                {
                    if (ExistsDir(pathRoot))
                    {
                        Directory.Delete(pathRoot, true);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("{0}", ex.Message));
                }

            }

            public bool ExistsDir(string path)
            {
                try
                {
                    if (Directory.Exists(path))
                    { return true; }
                    else { return false; }
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format("{0}", ex.Message));
                }

            }

        }
    }
