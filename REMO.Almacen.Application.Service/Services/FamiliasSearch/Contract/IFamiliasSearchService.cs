﻿using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;


namespace REMO.Almacen.Application.Service.Services.FamiliasSearch.Contract
{
    public interface IFamiliasSearchService
    {
        IEnumerable<FamiliasDTO> FindFamilias(FamiliasDTO searchmodel);
        bool AllFielsAreNull(FamiliasDTO searchmodel);
    }
}
