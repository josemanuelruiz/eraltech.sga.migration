﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Familias.Contract;
using REMO.Almacen.Application.Service.Services.FamiliasSearch.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.FamiliasSearch.Implementation
{
    public class FamiliasSearchService : IFamiliasSearchService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFamiliasService _FamiliasService;

        public FamiliasSearchService(IUnitOfWork unitOfWork, ILogService logService, IFamiliasService FamiliasService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _FamiliasService = FamiliasService;
        }

        public IEnumerable<FamiliasDTO> FindFamilias(FamiliasDTO searchmodel)
        {
            var funcName = ClassName + "." + MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);
          
                var res = (from t in (from g in (from l in _FamiliasService.GetFamilias().Where(i => i.DeletedReg.Equals(false)) select l).AsEnumerable()
                                      where
                                      !string.IsNullOrEmpty(searchmodel.Familia) && !string.IsNullOrEmpty(g.Familia) && g.Familia.ToUpper().Trim().Contains(searchmodel.Familia.ToUpper().Trim())
                                      select g)
                           group t by new { t.Id }
                             into mygroup
                           select mygroup.FirstOrDefault()).ToList().OrderBy(g => g.Familia);

                return res.AsQueryable();
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }

        public bool AllFielsAreNull(FamiliasDTO searchmodel)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);
                int i = 0;

                if (string.IsNullOrEmpty(searchmodel.Familia)) ++i;

                return i == 1 ? true : false;
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }

    }
}
