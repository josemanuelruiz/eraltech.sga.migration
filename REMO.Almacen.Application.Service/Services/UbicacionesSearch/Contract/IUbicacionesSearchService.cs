﻿using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;


namespace REMO.Almacen.Application.Service.Services.UbicacionesSearch.Contract
{
    public interface IUbicacionesSearchService
    {
        IEnumerable<UbicacionDTO> FindUbicaciones(UbicacionDTO searchmodel);
    }
}
