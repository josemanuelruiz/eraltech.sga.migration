﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Ubicaciones.Contract;
using REMO.Almacen.Application.Service.Services.UbicacionesSearch.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.UbicacionesSearch.Implementation
{
    public class UbicacionesSearchService : IUbicacionesSearchService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUbicacionesService _UbicacionesService;

        public UbicacionesSearchService(IUnitOfWork unitOfWork, ILogService logService, IUbicacionesService UbicacionesService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _UbicacionesService = UbicacionesService;
        }

        public IEnumerable<UbicacionDTO> FindUbicaciones(UbicacionDTO searchmodel)
        {
            var funcName = ClassName + "." + MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);
          
                var res = (from t in (from g in (from l in _UbicacionesService.GetUbicaciones().Where(i => i.DeletedReg.Equals(false)) select l).AsEnumerable()
                                      select g)
                           group t by new { t.Id }
                             into mygroup
                           select mygroup.FirstOrDefault()).ToList().OrderBy(g => g.DescUbicacion);

                return res.AsQueryable();
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
