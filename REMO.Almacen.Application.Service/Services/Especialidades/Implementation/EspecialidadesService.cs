﻿using REMO.Almacen.Application.Service.Services.Especialidades.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Contrat;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;
using System.Linq;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using System.Reflection;
using System.Web;
using System;

namespace REMO.Almacen.Application.Service.Services.Especialidades.Implementation
{
    public class EspecialidadesService : IEspecialidadesService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IEspecialidadesFactory _EspecialidadesFactory;

        public EspecialidadesService(IUnitOfWork unitOfWork, IEspecialidadesFactory EspecialidadesFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _EspecialidadesFactory = EspecialidadesFactory;
        }


        IEnumerable<EspecialidadesDTO> IEspecialidadesService.GetEspecialidades()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                var qry = from l in _unitOfWork.zSYS_M_EspecialidadesRepository.GetAll().OrderBy(g => g.Id).AsEnumerable()
                          select new EspecialidadesDTO(l);

                return qry;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
