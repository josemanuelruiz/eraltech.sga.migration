﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REMO.Almacen.Application.Service.Services.Ubicaciones.Contract
{
    public interface IUbicacionesService
    {
        IEnumerable<UbicacionDTO> GetUbicaciones();
    }
}
