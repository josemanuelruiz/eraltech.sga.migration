﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Ubicaciones.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Domain.Factory.Factories.UbicacionFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Ubicaciones.Implementation
{
    public class UbicacionesService : IUbicacionesService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IUbicacionesFactory _UbicacionesFactory;

        public UbicacionesService(IUnitOfWork unitOfWork, IUbicacionesFactory UbicacionesFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _UbicacionesFactory = UbicacionesFactory;
        }


        IEnumerable<UbicacionDTO> IUbicacionesService.GetUbicaciones()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                return from l in _unitOfWork.zSYS_M_UbicacionesRepository.GetAll().OrderBy(g => g.Id).AsEnumerable()
                       select new UbicacionDTO(l);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
