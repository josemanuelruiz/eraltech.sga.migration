﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.Lineas.Contract
{
    public  interface ILineasService
    {
        IEnumerable<LineasDTO> GetLineas();
    }
}
