﻿using REMO.Almacen.Application.Service.Services.Lineas.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.LineasFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Lineas.Implementation
{
    public class LineasService : ILineasService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILineasFactory _lineasFactory;

        public LineasService(IUnitOfWork unitOfWork, ILineasFactory lineasFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _lineasFactory = lineasFactory;
        }
  

        IEnumerable<LineasDTO> ILineasService.GetLineas()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

                return from l in _unitOfWork.zSYS_M_LineasRepository.GetAll().OrderBy(g => g.Linea).Where(i => i.DeletedReg.Equals(false)).AsEnumerable()
                   select new LineasDTO(l);
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
