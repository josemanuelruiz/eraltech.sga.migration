﻿using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;


namespace REMO.Almacen.Application.Service.Services.MovimientosSearch.Contract
{
    public interface IMovimientosSearchService
    {
        IEnumerable<MovimientosDTO> FindMovimientos(MovimientosExcelDTO searchmodel);
    }
}
