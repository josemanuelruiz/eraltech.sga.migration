﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Movimientos.Contract;
using REMO.Almacen.Application.Service.Services.MovimientosSearch.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.MovimientosSearch.Implementation
{
    public class MovimientosSearchService : IMovimientosSearchService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMovimientosService _MovimientosService;

        public MovimientosSearchService(IUnitOfWork unitOfWork, ILogService logService, IMovimientosService MovimientosService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _MovimientosService = MovimientosService;
        }

        public IEnumerable<MovimientosDTO> FindMovimientos(MovimientosExcelDTO searchmodel)
        {
            var funcName = ClassName + "." + MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

                var res = _MovimientosService.GetMovimientos().Where(m => m.DeletedReg.Equals(false));

                if (!string.IsNullOrEmpty(searchmodel.NombreRecambio))
                    res = res.Where(m => m.NombreRecambio.ToUpper().Trim().Contains(searchmodel.NombreRecambio.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.RecambiosAlmacen))
                    res = res.Where(m => m.IdRecambiosAlmacen.Description.ToUpper().Trim().Contains(searchmodel.RecambiosAlmacen.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.Ubicacion))
                    res = res.Where(m => m.IdUbicacion.Description.ToUpper().Trim().Contains(searchmodel.Ubicacion.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.MovementTypes))
                    res = res.Where(m => m.IdMovementTypes.Description.ToUpper().Trim().Contains(searchmodel.MovementTypes.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.FechaExtraccion))
                    res = res.Where(m => m.FechaExtraccion.ToUpper().Trim().Contains(searchmodel.FechaExtraccion.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.FechaRellenado))
                    res = res.Where(m => m.FechaRellenado.ToUpper().Trim().Contains(searchmodel.FechaRellenado.ToUpper()));

                if (searchmodel.Extraidos != 0)
                    res = res.Where(m => m.Extraidos == searchmodel.Extraidos);

                if (searchmodel.Repuestos != 0)
                    res = res.Where(m => m.Repuestos == searchmodel.Repuestos);

                if (searchmodel.Stock != 0)
                    res = res.Where(m => m.Stock == searchmodel.Stock);

                return res.OrderByDescending(m => DateTime.Parse(m.FechaExtraccion == "" ? "01/01/1900" : m.FechaExtraccion)).ToList().AsQueryable();
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
