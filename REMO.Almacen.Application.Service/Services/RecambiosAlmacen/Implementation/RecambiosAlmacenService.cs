﻿using REMO.Almacen.Application.Service.Enums;
using REMO.Almacen.Application.Service.Services.Images.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Domain.Factory.Factories.EspecialidadesFactory.Contrat;
using REMO.Almacen.Domain.Factory.Factories.FamiliasFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.MarcasFactory.Contract;
using REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Implementation
{
    public class RecambiosAlmacenService : IRecambiosAlmacenService
    {
        private static string ClassName => System.Reflection.MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private const int MAXCODIGAGVALUE = 100000000;

        private readonly IUnitOfWork _unitOfWork;
        private readonly ILogService _logService;
        private readonly IRecambiosAlmacenFactory _RecambiosAlmacenFactory;
        private readonly IEspecialidadesFactory _EspecialidadesFactory;
        private readonly IFamiliasFactory _FamiliasFactory;
        private readonly IMarcasFactory _MarcasFactory;
        private readonly IImagesService _imagesService;

        public RecambiosAlmacenService(IUnitOfWork unitOfWork
                                      ,ILogService logService
                                      ,IRecambiosAlmacenFactory recambiosAlmacenFactory
                                      ,IEspecialidadesFactory EspecialidadesFactory
                                      ,IFamiliasFactory FamiliasFactory
                                      ,IMarcasFactory MarcasFactory
                                      ,IImagesService imagesService)
        {
            _unitOfWork = unitOfWork;
            _logService = logService;
            _RecambiosAlmacenFactory = recambiosAlmacenFactory;
            _EspecialidadesFactory = EspecialidadesFactory;
            _FamiliasFactory = FamiliasFactory;
            _MarcasFactory = MarcasFactory;
            _imagesService = imagesService;
        }        


        public IEnumerable<RecambiosAlmacenDTO> GetRecambiosAlmacen()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {

                return from l in _unitOfWork.zSYS_T_RecambiosAlmacenRepository.GetAll().AsEnumerable()
                       join e in _unitOfWork.zSYS_M_EspecialidadesRepository.GetAll().AsEnumerable() on l.IdEspecialidad equals e.Id
                       join f in _unitOfWork.zSYS_M_FamiliasRepository.GetAll().AsEnumerable() on l.IdFamilia equals f.Id
                       join m in _unitOfWork.zSYS_M_MarcasRepository.GetAll().AsEnumerable() on l.IdMArcaRecambio equals m.Id
                       join u in _unitOfWork.zSYS_M_UbicacionesRepository.GetAll().AsEnumerable() on l.IdUbicacion equals u.Id
                            select new RecambiosAlmacenDTO(){  Id = l.Id,
                                                           CodigoAG = l.CodigoAG,
                                                           NumeroRecambio = l.NumeroRecambio,
                                                           Especialidad = new IdAndDescClass() {Id = e.Id, Description = e.Especialidad },
                                                           Familia = new IdAndDescClass() { Id = f.Id, Description = f.Familia },
                                                           NombreRecambio = l.NombreRecambio,
                                                           MArcaRecambio = new IdAndDescClass() { Id = m.Id, Description = m.Marca },                  
                                                           Ubicacion = new IdAndDescClass() { Id = u.Id, Description = u.DescUbicacion },
                                                           UbicacionDescription = u.DescUbicacion,
                                                           Referencia = l.Referencia,
                                                           StockAlmacen = l.StockAlmacen.HasValue ? l.StockAlmacen.Value : 0,
                                                           StockMinimo = l.StockMinimo.HasValue ? l.StockMinimo.Value : 0,
                                                           Precio = l.Precio.HasValue ? l.Precio.Value : 0,
                                                           FechaPedido = l.TiempoEntrega,
                                                           Pedido = l.Pedido,
                                                           Observaciones = l.Observaciones,
                                                           Cajon = l.Cajon,
                                                           Seleccionado = l.Seleccionado,
                                                           PerteneceKIT = l.PerteneceKIT,
                                                           Instalacion = l.Instalacion,
                                                           StockMaximo = l.StockMaximo.HasValue ? l.StockMaximo.Value : 0,
                                                           CodigoAntiguo = l.CodigoAntiguo,
                                                           Nuevos = l.Nuevos.HasValue ? l.Nuevos.Value : 0,
                                                           Usados = l.Usados.HasValue ? l.Usados.Value : 0,
                                                           Reparados = l.Reparados.HasValue ? l.Reparados.Value : 0,
                                                           ImageUrl = l.ImageUrl     

                   };
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
