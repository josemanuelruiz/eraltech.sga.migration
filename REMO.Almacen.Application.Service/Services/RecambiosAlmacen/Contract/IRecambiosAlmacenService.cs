﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;
using System.Drawing;
using System.IO;

namespace REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Contract
{
    public interface IRecambiosAlmacenService
    {
        IEnumerable<RecambiosAlmacenDTO> GetRecambiosAlmacen();
    }
}
