﻿using System.Collections.Generic;
using REMO.Almacen.Domain.Factory.DTOs;

namespace REMO.Almacen.Application.Service.Services.PrintLabel.Contract
{
    public interface IPrintLabelService
    {
        string PrintLabelFunction(IEnumerable<RecambiosAlmacenDTO> itemImage, string barcodeLabelLayoutPath, string labelpath);
    }
}
