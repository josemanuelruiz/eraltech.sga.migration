﻿using System;
using System.IO;
using iTextSharp.text;
using System.Reflection;
using REMO.Almacen.Print;
using iTextSharp.text.pdf;
using System.Collections.Generic;
using REMO.Almacen.Application.Service.Services.FileManager.Implementation;
using REMO.Almacen.Application.Service.Services.Log.Implementation;
using REMO.Almacen.Application.Service.Services.PrintLabel.Contract;
using REMO.Almacen.Domain.Factory.DTOs;

namespace REMO.Almacen.Application.Service.Services.PrintLabel.Implementation
{
    public class PrintLabelService : IPrintLabelService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly LogService _logService = new LogService();
        private readonly FileManagerHelper _FileManagerHelper = new FileManagerHelper();
        private readonly BarcodeGeneratorClass _BarcodeGeneratorClass = new BarcodeGeneratorClass();

        public PrintLabelService()
        {

        }

        public string PrintLabelFunction(IEnumerable<RecambiosAlmacenDTO> itemImage, string barcodeLabelLayoutPath, string labelpath)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            var imageFullPath = "";
            var pdfFile = "";
            var ext = ".bmp";
            try
            {
                List<string> barCodes = new List<string>();

                foreach(RecambiosAlmacenDTO recItem in itemImage)
                {
                    var image = _BarcodeGeneratorClass.BarcodeGeneratorFunction(recItem.CodigoAG);
                    labelpath = labelpath.Replace(@"\", "");
                    recItem.ImageUrl = string.Concat(AppContext.BaseDirectory, labelpath, @"\BRCD_", recItem.CodigoAG, ext);
                    image.Save(recItem.ImageUrl);
                    imageFullPath = _BarcodeGeneratorClass.GenerateImage(recItem, barcodeLabelLayoutPath, labelpath);
                    barCodes.Add(imageFullPath);
                }

                //Creación PDF tantas veces como se quiera
                pdfFile = PrintLabelFunction(barCodes, labelpath);

                return pdfFile;
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}", ex.Message));
            }

        }

        private string PrintLabelFunction(List<string> labels, string labelpath)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            var pdfFileName = "";
            try
            {
                //Generar Documento
                iTextSharp.text.Document document = new iTextSharp.text.Document();
                //Definimos la ruta
                var rutaPdf = string.Concat(AppContext.BaseDirectory, labelpath);
                pdfFileName = "PrintLabels_" + DateTime.Now.ToString().Replace(":","").Replace("/","") + ".pdf";
                var fullPath = string.Concat(AppContext.BaseDirectory, labelpath, @"\", pdfFileName);

                if (_FileManagerHelper.Existfile(rutaPdf, pdfFileName))
                {
                    File.Delete(fullPath);
                }

                PdfWriter.GetInstance(document, new FileStream(fullPath, FileMode.Create));
                //Abrir el Documento
                document.Open();

                //Fuerza tamaño de celda para poner 10 imágenes por hoja
                PdfPTable t = new PdfPTable(2);
                t.DefaultCell.Border = Rectangle.NO_BORDER;
                t.DefaultCell.FixedHeight = 140f;
                
                foreach(string item in labels)
                {
                    iTextSharp.text.Image imagen = iTextSharp.text.Image.GetInstance(item);
                    imagen.BorderWidth = 0;
                    float percentage = 0.0f;
                    percentage = 120 / imagen.Width;
                    t.AddCell(imagen);
                }

                if(labels.Count % 2 == 1)
                {
                    t.AddCell("");
                }

                document.Add(t);
                document.Close();
            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}", ex.Message));
            }

            return pdfFileName;
        }
               
    }
}
