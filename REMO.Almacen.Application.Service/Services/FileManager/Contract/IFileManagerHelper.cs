﻿using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.FileManager.Contract
{
    public interface IFileManagerHelper
    {
        void Createfile(string path, string filename);
        void Deletefiles(string pathRoot);
        List<string> ReadfileData(string filename);
        void WritefileData(string filename, string Data);
        bool Existfile(string path, string filename);

    }
}
