﻿using REMO.Almacen.Application.Service.Services.FileManager.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace REMO.Almacen.Application.Service.Services.FileManager.Implementation
{
    public class FileManagerHelper : IFileManagerHelper
    {
        private static string ClassName => System.Reflection.MethodBase.GetCurrentMethod().DeclaringType?.Name;
        public string Extension; 

        public FileManagerHelper()
        {
        }

        public void Createfile(string path, string filename)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                filename = string.Concat(filename, Extension);

                if (!Existfile(path, filename))
                {
                    var myFile = File.Create(string.Concat(path, @"\", filename));
                    myFile.Close();
                }
            }
            catch (Exception ex)
            {
  
                throw new Exception(string.Format("{0}", ex.Message));
            }
        }

        public void Deletefiles(string pathRoot)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                List<string> files = Directory.GetFiles(pathRoot, "*.*", SearchOption.AllDirectories).ToList();
                foreach (string item in files)
                {
                    System.IO.File.Delete(item);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}", ex.Message));
            }

        }

        public List<string> ReadfileData(string filename)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                var listLines = new List<string>();
                string line;

                using (StreamReader fileStream = new StreamReader(string.Concat(filename, Extension)))
                {
                    while ((line = fileStream.ReadLine()) != null)
                    {
                        listLines.Add(line);
                    }
                    if (fileStream != null) fileStream.Close();
                    return listLines;
                }

            }
            catch (Exception ex)
            {
                  throw new Exception(string.Format("{0}", ex.Message));
            }

        }

        public void WritefileData(string filename, string Data)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                using (StreamWriter fileStream = new StreamWriter(filename, append: true))
                {
 
                    fileStream.WriteLine(Data);
                    if (fileStream != null)
                        fileStream.Flush();
                    fileStream.Close();
                }
            }
            catch (Exception ex)
            {
                 throw new Exception(string.Format("{0}", ex.Message));
            }
        }

        public bool Existfile(string path, string filename)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                List<string> files = Directory.GetFiles(path, @"*.*", SearchOption.AllDirectories).ToList();
                bool res = false;

                foreach (string item in files)
                {
                    if (Path.GetFileName(item) == filename)
                    {
                        res = true;
                        break;
                    }

                }
                return res;

            }
            catch (Exception ex)
            {
                throw new Exception(string.Format("{0}", ex.Message));
            }
        }
    }
}
