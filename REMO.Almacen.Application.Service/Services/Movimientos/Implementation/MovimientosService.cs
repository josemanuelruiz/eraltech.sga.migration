﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.Movimientos.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Domain.Factory.Factories.MovimientosFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Movimientos.Application.Service.Services.MovimientosSearch.Implementation
{
    public class MovimientosService : IMovimientosService
    {

        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMovimientosFactory _MovimientosFactory;

        public MovimientosService(IUnitOfWork unitOfWork, IMovimientosFactory MovimientosFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _MovimientosFactory = MovimientosFactory;
        }

        IEnumerable<MovimientosDTO> IMovimientosService.GetMovimientos()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                      return (from l in _unitOfWork.zSYS_T_MovimientosRepository.GetAll().Where(i => i.DeletedReg.Equals(false)).AsEnumerable()
                             join r in _unitOfWork.zSYS_T_RecambiosAlmacenRepository.GetAll().AsEnumerable() on l.IdRecambiosAlmacen equals r.Id
                             join u in _unitOfWork.zSYS_M_UbicacionesRepository.GetAll().AsEnumerable() on l.IdUbicacion equals u.Id
                             join m in _unitOfWork.zSYS_M_MovementTypesRepository.GetAll().AsEnumerable() on l.IdMovementTypes equals m.Id
                                   select new MovimientosDTO() {   Id = l.Id,
                                                                   NumeroMovimiento = l.NumeroMovimiento.HasValue ? l.NumeroMovimiento.Value : 0,
                                                                   IdMovementTypes = new IdAndDescClass() { Id = m.Id, Description = m.Description },
                                                                   IdUbicacion = new IdAndDescClass() { Id = u.Id, Description = u.DescUbicacion },
                                                                   IdRecambiosAlmacen = new IdAndDescClass() { Id = r.Id, Description = r.CodigoAG },
                                                                   NombreRecambio = r.NombreRecambio,
                                                                   FechaExtraccion = string.Format(l.FechaExtraccion.ToString(), "DD/MM/YYYY", CultureInfo.InvariantCulture),
                                                                   Extraidos = l.Extraidos.HasValue ? l.Extraidos.Value : 0,
                                                                   Vaciado = l.Vaciado,
                                                                   FechaRellenado = string.Format(l.FechaRellenado.ToString(), "DD/MM/YYYY", CultureInfo.InvariantCulture),
                                                                   Repuestos = l.Repuestos.HasValue ? l.Repuestos.Value : 0,
                                                                   Rellenado = l.Rellenado,
                                                                   Stock = l.Stock.HasValue ? l.Stock.Value : 0,
                                                                   Nuevos = l.Nuevos,
                                                                   Usados = l.Usados,
                                                                   Reparados = l.Reparados,
                                                                   DeletedReg = l.DeletedReg,
                                                            }).OrderBy(m => m.Id);
           
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
