﻿using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.Movimientos.Contract
{
    public interface IMovimientosService
    {
        IEnumerable<MovimientosDTO> GetMovimientos();
    }
}
