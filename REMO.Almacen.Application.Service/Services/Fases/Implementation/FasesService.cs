﻿using REMO.Almacen.Application.Service.Services.Fases.Contract;
using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Domain.Factory.Factories.FasesFactory.Contract;
using REMO.Almacen.Infrastructure.Data.EntityModel;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.Fases.Implementation
{
    public class FasesService : IFasesService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IFasesFactory _FasesFactory;

        public FasesService(IUnitOfWork unitOfWork, IFasesFactory FasesFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _FasesFactory = FasesFactory;
        }


        IEnumerable<FasesDTO> IFasesService.GetFases()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

                return from f in _unitOfWork.zSYS_M_FasesRepository.GetAll().OrderBy(g => g.Fase).Where(i => i.DeletedReg.Equals(false)).AsEnumerable()
                       join l in _unitOfWork.zSYS_M_LineasRepository.GetAll().AsEnumerable() on f.IdLinea equals l.Id
                       join la in _unitOfWork.zSYS_M_LayoutRepository.GetAll().AsEnumerable() on f.IdLayout equals la.Id
                       select new FasesDTO()
                       {
                           Id = f.Id,
                           Fase = f.Fase,
                           Linea = new IdAndDescClass() { Id = l.Id, Description = l.Linea },
                           Layout = new IdAndDescClass() { Id = la.Id, Description = la.Layout },
                           NombreArchivo = f.NombreArchivo
                       };
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
