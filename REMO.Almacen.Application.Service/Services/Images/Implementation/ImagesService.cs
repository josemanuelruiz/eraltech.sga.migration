﻿using REMO.Almacen.Application.Service.Services.FileManager.Implementation;
using REMO.Almacen.Application.Service.Services.Images.Contract;
using REMO.Almacen.Application.Service.Services.Log.Implementation;
using System;
using System.Configuration;
using System.Reflection;

namespace REMO.Almacen.Application.Service.Services.Images.Implementation
{
    public class ImagesService : IImagesService
    {
     
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly  LogService _logService = new LogService();
        private readonly  FileManagerHelper _FileManagerHelper = new FileManagerHelper();
        private readonly string _imagepath = ConfigurationManager.AppSettings["ImagesPath"];

        public ImagesService( )
        {

        }

        public string SaveImageOnFolder(byte[] imageSource, string fileName)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;
            try
            {
                string path = string.Format(@"{0}{1}", AppContext.BaseDirectory, _imagepath.Replace(@"\", ""));
                string fullPath = string.Format(@"{0}{1}\{2}", AppContext.BaseDirectory, _imagepath.Replace(@"\", ""), fileName);

                if (_FileManagerHelper.Existfile(path, fileName))
                {
                    System.IO.File.Delete(fullPath);
                }

                System.IO.File.WriteAllBytes(fullPath, imageSource);
                string webPath = string.Format(@"{0}/{1}", _imagepath.Replace(@"\", "/"), fileName);

                return webPath;
            }
            catch (Exception ex)
            {

                throw new Exception(string.Format("{0}", ex.Message));
            }
        }
    }
}
