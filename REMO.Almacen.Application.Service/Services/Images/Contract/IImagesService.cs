﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace REMO.Almacen.Application.Service.Services.Images.Contract
{
    public interface IImagesService
    {
        string SaveImageOnFolder(byte[] imageSource, string filename);
    }
}
