﻿using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.MovementTypesService.Contract
{
    public  interface IMovementTypesService
    {
        IEnumerable<MovementTypesDTO> GetMovementTypes();
    }
}
