﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.MovementTypesService.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.Factories.LineasFactory.Contract;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.MovementTypesService.Implementation
{
    public class MovementTypesService : IMovementTypesService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;

        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILineasFactory _lineasFactory;

        public MovementTypesService(IUnitOfWork unitOfWork, ILineasFactory lineasFactory, ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _lineasFactory = lineasFactory;
        }
  

        IEnumerable<MovementTypesDTO> IMovementTypesService.GetMovementTypes()
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);

                return from t in _unitOfWork.zSYS_M_MovementTypesRepository.GetAll().Where(i => i.DeletedReg.Equals(false)).AsEnumerable()
                   select new MovementTypesDTO(t);
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
