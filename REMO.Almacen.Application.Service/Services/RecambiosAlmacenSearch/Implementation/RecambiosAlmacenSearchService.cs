﻿using REMO.Almacen.Application.Service.Services.Log.Contract;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacen.Contract;
using REMO.Almacen.Application.Service.Services.RecambiosAlmacenSearch.Contract;
using REMO.Almacen.Domain.Factory.DTOs;
using REMO.Almacen.Domain.Factory.DTOs.Common;
using REMO.Almacen.Domain.Factory.Factories.RecambiosAlmacenFactory.Contract;
using REMO.Almacenes.Infrastructure.Data.UnitOfWork.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Web;

namespace REMO.Almacen.Application.Service.Services.RecambiosAlmacenSearch.Implementation
{
    public class RecambiosAlmacenSearchService : IRecambiosAlmacenSearchService
    {
        public static string ClassName => MethodBase.GetCurrentMethod().DeclaringType?.Name;


        private readonly ILogService _logService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRecambiosAlmacenFactory _RecambiosAlmacenFactory;
        private readonly IRecambiosAlmacenService _RecambiosAlmacensService;
        private bool _filterStockAlmacen = false;
        private bool _filterStockMinimo = false;
        private bool _filterPrecio = false;
        private bool _filterStockMaximo = false;
        private bool _filterNuevos = false;
        private bool _filterUsados = false;
        private bool _filterReparados = false;

        public RecambiosAlmacenSearchService(IUnitOfWork unitOfWork
                                            , IRecambiosAlmacenFactory RecambiosAlmacenFactory
                                            , IRecambiosAlmacenService RecambiosAlmacensService
                                            , ILogService logService)
        {
            _logService = logService;
            _unitOfWork = unitOfWork;
            _RecambiosAlmacenFactory = RecambiosAlmacenFactory;
            _RecambiosAlmacensService = RecambiosAlmacensService;
        }

        IEnumerable<RecambiosAlmacenDTO> IRecambiosAlmacenSearchService.FindRecambiosAlmacen(RecambiosAlmacenExcelDTO searchmodel)
        {
            var funcName = ClassName + "." + System.Reflection.MethodBase.GetCurrentMethod().Name;

            try
            {
                //_logService.InsertLogValid(
                //    "2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    HttpContext.Current.User.Identity.Name);
                //generamos lista con todos los elementos 
                //y luego filtramos
                RegexOptions options = RegexOptions.None;
                Regex regex = new Regex("[ ]{2,}", options); //Para reemplazar más de un espacio en blanco. 
                
                var res = _RecambiosAlmacensService.GetRecambiosAlmacen();

                if (!string.IsNullOrEmpty(searchmodel.CodigoAG))
                    res = res.Where(r => r.CodigoAG != null && r.CodigoAG.ToUpper().Contains(searchmodel.CodigoAG.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.Especialidad))
                    res = res.Where(r => r.Especialidad != null && r.Especialidad.Description.ToUpper().Contains(searchmodel.Especialidad.ToUpper()));

                if(!string.IsNullOrEmpty(searchmodel.Familia) && int.Parse(searchmodel.Familia) != 0)
                    res = res.Where(r => r.Familia != null && r.Familia.Id == int.Parse(searchmodel.Familia));

                if (!string.IsNullOrEmpty(searchmodel.NombreRecambio))
                    res = res.Where(r => r.NombreRecambio != null && r.NombreRecambio.ToUpper().Contains(searchmodel.NombreRecambio.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.MArcaRecambio) && int.Parse(searchmodel.MArcaRecambio) != 0)
                    res = res.Where(r => r.MArcaRecambio != null && r.MArcaRecambio.Id == int.Parse(searchmodel.MArcaRecambio));

                if (!string.IsNullOrEmpty(searchmodel.Referencia))
                    res = res.Where(r => r.Referencia != null && r.Referencia.ToUpper().Contains(searchmodel.Referencia.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.Ubicacion))
                    res = res.Where(r => r.Ubicacion != null && r.Ubicacion.Description.ToUpper().Contains(searchmodel.Ubicacion.ToUpper()));

                if (searchmodel.StockAlmacen > 0)
                    res = res.Where(r => r.StockAlmacen == searchmodel.StockAlmacen);
                else if (_filterStockAlmacen && searchmodel.StockAlmacen == -1)
                    res = res.Where(r => r.StockAlmacen == 0);

                if (searchmodel.StockMinimo > 0)
                    res = res.Where(r => r.StockMinimo == searchmodel.StockMinimo);
                else if (_filterStockMinimo && searchmodel.StockMinimo == -1)
                    res = res.Where(r => r.StockMinimo == 0);

                if (searchmodel.StockMaximo > 0)
                    res = res.Where(r => r.StockMaximo == searchmodel.StockMaximo);
                else if (_filterStockMaximo && searchmodel.StockMaximo == -1)
                    res = res.Where(r => r.StockMaximo == 0);

                if (searchmodel.Precio > 0)
                    res = res.Where(r => r.Precio == searchmodel.Precio);
                else if (_filterPrecio && searchmodel.Precio == -1)
                    res = res.Where(r => r.Precio == 0);

                if (!string.IsNullOrEmpty(searchmodel.FechaPedido))
                    res = res.Where(r => r.FechaPedido != null && r.FechaPedido.ToUpper().Contains(searchmodel.FechaPedido.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.Observaciones))
                    res = res.Where(r => r.Observaciones != null && r.Observaciones.ToUpper().Contains(searchmodel.Observaciones.ToUpper()));
                
                if (!string.IsNullOrEmpty(searchmodel.Cajon))
                    res = res.Where(r => r.Cajon != null && r.Cajon.ToUpper().Contains(searchmodel.Cajon.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.Instalacion))
                    res = res.Where(r => r.Instalacion != null && regex.Replace(r.Instalacion, " ").ToUpper().Contains(searchmodel.Instalacion.ToUpper()));

                if (!string.IsNullOrEmpty(searchmodel.CodigoAntiguo))
                    res = res.Where(r => r.CodigoAntiguo != null && r.CodigoAntiguo.ToUpper().Contains(searchmodel.CodigoAntiguo.ToUpper()));

                if (searchmodel.Nuevos > 0)
                    res = res.Where(r => r.Nuevos == searchmodel.Nuevos);
                else if (_filterNuevos && searchmodel.Nuevos == -1)
                    res = res.Where(r => r.Nuevos == 0);

                if (searchmodel.Usados > 0)
                    res = res.Where(r => r.Usados == searchmodel.Usados);
                else if (_filterUsados && searchmodel.Usados == -1)
                    res = res.Where(r => r.Usados == 0);

                if (searchmodel.Reparados > 0)
                    res = res.Where(r => r.Reparados == searchmodel.Reparados);
                else if (_filterReparados && searchmodel.Reparados == -1)
                    res = res.Where(r => r.Reparados == 0);

                if( searchmodel.Seleccionado)
                    res = res.Where(r => r.Seleccionado == searchmodel.Seleccionado);

                if (searchmodel.PerteneceKIT)
                    res = res.Where(r => r.PerteneceKIT == searchmodel.PerteneceKIT);

                if (searchmodel.Pedido)
                    res = res.Where(r => r.Pedido == searchmodel.Pedido);

                if (!string.IsNullOrEmpty(searchmodel.ImageUrl))
                    res = res.Where(r => r.ImageUrl.ToUpper().Contains(searchmodel.ImageUrl.ToUpper()));

                return res.AsQueryable();
            }
            catch (Exception ex)
            {
                //_logService.InsertLogError("2",
                //    funcName,
                //    string.Concat("Usuari='", HttpContext.Current.User.Identity.Name, "'"),
                //    "",
                //    ex,
                //    HttpContext.Current.User.Identity.Name);

                throw new Exception(ex.Message);
            }
        }
    }
}
