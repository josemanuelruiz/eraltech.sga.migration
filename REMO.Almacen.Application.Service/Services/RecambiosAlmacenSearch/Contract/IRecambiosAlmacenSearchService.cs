﻿using REMO.Almacen.Domain.Factory.DTOs;
using System.Collections.Generic;

namespace REMO.Almacen.Application.Service.Services.RecambiosAlmacenSearch.Contract
{
    public interface IRecambiosAlmacenSearchService
    {
        IEnumerable<RecambiosAlmacenDTO> FindRecambiosAlmacen(RecambiosAlmacenExcelDTO searchmodel);
    }
}


